﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="default.aspx.vb" Inherits="_default" %>

<%@ Register src="includes/header.ascx" tagname="header" tagprefix="uc1" %>
<%@ Register src="includes/footer.ascx" tagname="footer" tagprefix="uc2" %>
<%@ Register src="includes/tasks.ascx" tagname="tasks" tagprefix="uc3" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link href="style.css" rel="stylesheet" type="text/css" />
</head>
<body>
    <form id="form1" runat="server">
 <uc1:header ID="header1" runat="server" />
    <div class="top">&nbsp;&nbsp;&nbsp;</div>
    <div id="wrapper">
        <div class="left_col">
            <uc3:tasks ID="tasks1" runat="server" />
        </div>
        <div class="right_main">
            <iframe runat="server" id="myHome" width="730" height="1800" style="border-style: none; border-width: 0px; border-color: inherit; margin: 0px;" src="/home.aspx" scrolling="auto" frameborder="0"></iframe>
        </div>
    </div>
    <uc2:footer ID="footer1" runat="server" />
    </form>
</body>
</html>
