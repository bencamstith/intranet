﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="default.aspx.vb" Inherits="webaccounts_default" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Web Accounts</title>
    <link href="../style.css" rel="stylesheet" type="text/css" />
    <link href="../table_style.css" rel="stylesheet" type="text/css" />
</head>
<body>
    <form id="form1" runat="server">
    <h1><asp:Label ID="lblHeader" runat="server" Text=""></asp:Label></h1>
    <asp:Table ID="tWebAccounts" runat="server" CellPadding="2" CellSpacing="0" Width="689">
    </asp:Table>
    </form>
</body>
</html>
