﻿
Partial Class webaccounts_webaccount
    Inherits System.Web.UI.Page

    Protected Sub Page_PreInit(sender As Object, e As System.EventArgs) Handles Me.PreInit
        If Not IsPostBack Then
            If Not Request.QueryString("webaccountkey") Is Nothing AndAlso IsNumeric(Request.QueryString("webaccountkey").ToString) Then
                Me.txtWebAccountKey.Text = Request.QueryString("webaccountkey").ToString
            Else
                Me.txtWebAccountKey.Text = "0"
            End If
        End If

        Dim strHTML As String = "<object id=""webforms"" classid=""http://intranet.counselmanauto.com/carwebforms.dll#carwebforms.webaccountstoolbar"" height=""25"" width=""800"" style=""background-color: #ffffff; font-family: sans-serif; font-size: 8.25pt;"" archive=""true"" >" & _
                        "   <param name=""primarykey"" value=""" & Me.txtWebAccountKey.Text & """>" & _
                        "   <param name=""foreignkey"" value=""0"">" & _
                        "   <param name=""activeuserkey"" value=""1"">" & _
                        "   <param name=""HTTPPath"" value="""">" & _
                        "</object>"

        Me.litToolbar.Text = strHTML
    End Sub

   Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
      If Not IsPostBack And Not Request.QueryString("webaccountkey") Is Nothing Then
         If Not Session("approve") Is Nothing Then
            If CType(Session("approve").ToString, Boolean) Then
                    '  Me.lblMessage.Text = "Click Submit to save and activate account."
            End If
         End If

         BindWebAccount()
      End If
   End Sub

   Private Sub BindWebAccount()
        Using objWebAccounts As New cardb.Database.WebAccounts("WebAccountKey = " & Me.txtWebAccountKey.Text)
            If objWebAccounts.Count > 0 Then
                With objWebAccounts(0)
                    Me.lblEmail.Text = .Email
                    Me.lblPassword.Text = .Password
                    Me.lblEmailInvoices.Text = .EmailInvoices.ToString
                    Me.lblEmailStatements.Text = .EmailStatements.ToString
                    Me.lblPasswordExpired.Text = .ExpiredPassword.ToString
                    'Me.lblStatus.Text = .AccountStatus

                    Me.lblCustomerNumbers.Text = ""
                    Using objCustomerNumbers As New cardb.Database.WebAccountCustomers("WebAccountKey = " & Me.txtWebAccountKey.Text, "CustomerNumber")
                        For Each objCustomerNumber As cardb.Database.WebAccountCustomer In objCustomerNumbers
                            If Me.lblCustomerNumbers.Text.Trim.Length > 0 Then Me.lblCustomerNumbers.Text &= vbCrLf
                            Me.lblCustomerNumbers.Text &= objCustomerNumber.CustomerNumber.Trim
                        Next
                    End Using
                End With
            End If
        End Using
   End Sub

End Class
