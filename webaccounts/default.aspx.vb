﻿
Partial Class webaccounts_default
    Inherits System.Web.UI.Page

   Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
      If Not Request.QueryString("status") Is Nothing Then BindWebAccounts()
   End Sub

   Private Sub BindWebAccounts()
      Select Case CType(Request.QueryString("status"), Integer)
         Case Is = 1
            Me.lblHeader.Text = "Pending Web Accounts"
         Case Is = 2
            Me.lblHeader.Text = "Active Web Accounts"
         Case Is = 3
            Me.lblHeader.Text = "In-active Web Accounts"
      End Select

      Dim tr As TableRow
      Dim td As TableCell

      tr = New TableRow
      tWebAccounts.Rows.Add(tr)

      td = New TableCell
      tr.Cells.Add(td)
      td.Width = "175"
      td.Text = "User Id/Email"

      td = New TableCell
      tr.Cells.Add(td)
      td.Text = "Customer #"

      td = New TableCell
      tr.Cells.Add(td)
      td.Width = "100"
      td.Text = "Email Invoices"

      td = New TableCell
      tr.Cells.Add(td)
      td.Width = "100"
      td.Text = "Email Statements"

      tr = New TableRow
      tWebAccounts.Rows.Add(tr)

      td = New TableCell
      tr.Cells.Add(td)
      td.ColumnSpan = 4
      td.Text = "<div class=""spacer""></div>"

      Using objWebAccounts As New cardb.Database.WebAccounts("AccountStatus = " & Request.QueryString("status"))
         For Each objWebAccount As cardb.Database.WebAccount In objWebAccounts
            tr = New TableRow
            tWebAccounts.Rows.Add(tr)

            tr.ID = "wa_" & objWebAccount.WebAccountKey
            tr.Attributes("onmouseout") = "this.className='trdataOut'"
            tr.Attributes("onmouseover") = "this.className='trdataOver'"
            tr.Attributes("onclick") = "window.navigate('webaccount.aspx?status=" & Request.QueryString("status").ToString & "&webaccountkey=" & objWebAccount.WebAccountKey & "')"

            td = New TableCell
            tr.Cells.Add(td)
            td.VerticalAlign = VerticalAlign.Top
            td.Text = objWebAccount.Email

            td = New TableCell
            tr.Cells.Add(td)
            td.VerticalAlign = VerticalAlign.Top
            td.Text = ""
            Using objWebAccountCustomers As New cardb.Database.WebAccountCustomers("WebAccountKey = " & objWebAccount.WebAccountKey, "CustomerNumber")
               For Each objWebAccountCustomer As cardb.Database.WebAccountCustomer In objWebAccountCustomers
                  If td.Text.Length > 0 Then td.Text &= ""
                  td.Text &= objWebAccountCustomer.CustomerNumber & "<br />"
               Next
            End Using

            td = New TableCell
            tr.Cells.Add(td)
            td.VerticalAlign = VerticalAlign.Top
            td.HorizontalAlign = HorizontalAlign.Center
            If objWebAccount.EmailInvoices Then
               td.Text = "Yes"
            Else
               td.Text = "No"
            End If

            td = New TableCell
            tr.Cells.Add(td)
            td.VerticalAlign = VerticalAlign.Top
            td.HorizontalAlign = HorizontalAlign.Center
            If objWebAccount.EmailStatements Then
               td.Text = "Yes"
            Else
               td.Text = "No"
            End If
         Next
      End Using

      tr = Nothing
      td = Nothing
   End Sub
End Class
