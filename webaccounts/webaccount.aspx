﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="webaccount.aspx.vb" Inherits="webaccounts_webaccount" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Web Accounts</title>
    <link href="../style.css" rel="stylesheet" type="text/css" />
    <link href="../table_style.css" rel="stylesheet" type="text/css" />
</head>
<body>
    <form id="form1" runat="server">
    <asp:Literal ID="litToolbar" runat="server"></asp:Literal>
<br /><br />
<object id="devforms" classid="http://services.counselmanauto.com/carwebforms.dll#carwebforms.webaccountstoolbar" height="25" width="800" archive="true" >
   <param name="primarykey" value="54">
   <param name="foreignkey" value="0">
   <param name="activeuserkey" value="1">
   <param name="HTTPPath" value="">
</object><br /><br />
    <h1>Web Account</h1>
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <asp:UpdatePanel ID="uPanel1" runat="server">
        <ContentTemplate>
            <asp:TextBox ID="txtWebAccountKey" style="display:none;" runat="server"></asp:TextBox>
            <asp:Table ID="tWebAccount" runat="server" Width="350">
                <asp:TableRow>
                    <asp:TableCell CssClass="tdhead">UserId/Email</asp:TableCell>
                    <asp:TableCell CssClass="tdhead">:</asp:TableCell>
                    <asp:TableCell><asp:Label ID="lblEmail" runat="server" Text=""></asp:Label></asp:TableCell>
                    <asp:TableCell Width="100%"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell CssClass="tdhead">Password</asp:TableCell>
                    <asp:TableCell CssClass="tdhead">:</asp:TableCell>
                    <asp:TableCell><asp:Label ID="lblPassword" runat="server" Text=""></asp:Label></asp:TableCell>
                    <asp:TableCell Width="100%"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell CssClass="tdhead">Password Expired</asp:TableCell>
                    <asp:TableCell CssClass="tdhead">:</asp:TableCell>
                    <asp:TableCell><asp:Label ID="lblPasswordExpired" runat="server" Text=""></asp:Label></asp:TableCell>
                    <asp:TableCell Width="100%"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell CssClass="tdhead" VerticalAlign="Top">Customer #'s</asp:TableCell>
                    <asp:TableCell CssClass="tdhead" VerticalAlign="Top">:</asp:TableCell>
                    <asp:TableCell><asp:Label ID="lblCustomerNumbers" runat="server" Text=""></asp:Label></asp:TableCell>
                    <asp:TableCell Width="100%"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell CssClass="tdhead">Email Invoices</asp:TableCell>
                    <asp:TableCell CssClass="tdhead">:</asp:TableCell>
                    <asp:TableCell><asp:Label ID="lblEmailInvoices" runat="server" Text=""></asp:Label></asp:TableCell>
                    <asp:TableCell Width="100%"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell CssClass="tdhead">Email&nbsp;Statements</asp:TableCell>
                    <asp:TableCell CssClass="tdhead">:</asp:TableCell>
                    <asp:TableCell><asp:Label ID="lblEmailStatements" runat="server" Text=""></asp:Label></asp:TableCell>
                    <asp:TableCell Width="100%"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell CssClass="tdhead">Status</asp:TableCell>
                    <asp:TableCell CssClass="tdhead">:</asp:TableCell>
                    <asp:TableCell><asp:Label ID="lblStatus" runat="server" Text=""></asp:Label></asp:TableCell>
                    <asp:TableCell Width="100%"></asp:TableCell>
                </asp:TableRow>
            </asp:Table>
        </ContentTemplate>
    </asp:UpdatePanel>
    </form>
</body>
</html>
