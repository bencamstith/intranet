﻿
Partial Class _default
    Inherits System.Web.UI.Page

   Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
      If Not IsPostBack Then
         If Not Request.QueryString("deliverytickets") Is Nothing Then
            myHome.Attributes("src") = "/deliverytickets/dtlabels.aspx"
         End If
         If Not Request.QueryString("approve") Is Nothing And Not Request.QueryString("webaccountkey") Is Nothing Then
            Session("webaccountkey") = Request.QueryString("webaccountkey").ToString
            Session("approve") = True
            myHome.Attributes("src") = "/webaccounts/webaccount.aspx?status=2&webaccountkey=" & Request.QueryString("webaccountkey").ToString
         Else
            Session("approve") = False
         End If
      End If
   End Sub
End Class
