﻿
Partial Class includes_tasks
    Inherits System.Web.UI.UserControl

   Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
      If Not Request.QueryString("deliverytickets") Is Nothing Then
         Me.iFrame_Tasks.Attributes("src") = "/tasks/deliverytickets.aspx"
      End If
      If Not Session("approve") Is Nothing AndAlso CType(Session("approve").ToString, Boolean) = True Then
         Me.iFrame_Tasks.Attributes("src") = "/tasks/webaccounts.aspx"
      End If
   End Sub
End Class
