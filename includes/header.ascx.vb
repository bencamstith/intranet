﻿
Partial Class includes_header
    Inherits System.Web.UI.UserControl

   Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
      Dim bolStop As Boolean = True
      Dim bolSuperAccess As Boolean = False
      Dim strIPAddress As String = Request.ServerVariables("remote_host").ToString

      If strIPAddress.StartsWith("192.168") Or _
         strIPAddress.StartsWith("173.21.128.198") Or _
         strIPAddress.StartsWith("12.171.200") Or _
         strIPAddress.StartsWith("12.171.200") Or _
         strIPAddress.StartsWith("70.147.114") Or _
         strIPAddress.StartsWith("69.85.207.99") Then bolStop = False

      ' 70.147.114.218 = Spanish Fort

      If bolStop Then
         Response.Redirect("http://www.counselmanauto.com")
         Response.End()
      End If

      Select Case True
         Case strIPAddress = "192.168.111.125" ' Local server
            bolSuperAccess = True
         Case strIPAddress = "192.168.111.11" ' Chad
            bolSuperAccess = True
         Case strIPAddress = "192.168.114.32" ' James
            bolSuperAccess = True
         Case strIPAddress = "192.168.111.138" ' James
            bolSuperAccess = True
         Case strIPAddress = "69.85.207.99" ' James
            bolSuperAccess = True
         Case strIPAddress = "192.168.114.31" ' Mary
            bolSuperAccess = True
         Case strIPAddress = "192.168.111.12" ' Veronica
            bolSuperAccess = True
         Case strIPAddress = "192.168.111.100" ' Rick
            bolSuperAccess = True
         Case strIPAddress = "12.171.200.226" ' Rick
            bolSuperAccess = True
         Case strIPAddress = "173.21.128.198" ' Rick
            bolSuperAccess = True
         Case strIPAddress = "192.168.111.77" ' Rick
            bolSuperAccess = True
         Case Else
            bolSuperAccess = True
      End Select

      Session("SuperAccess") = True

      If bolSuperAccess Then
         lnkWebAccounts.Visible = True
         lblWebAccounts.Visible = True

         lnkJunkVehicles.Visible = True
         lblJunkVehicles.Visible = True
      Else
         lnkWebAccounts.Visible = False
         lblWebAccounts.Visible = False

         lnkJunkVehicles.Visible = False
         lblJunkVehicles.Visible = False
      End If

      Me.lblInfo.Text = System.Security.Principal.WindowsIdentity.GetCurrent.Name() & "/" & strIPAddress
      Me.lblInfo.Text = strIPAddress
   End Sub
End Class
