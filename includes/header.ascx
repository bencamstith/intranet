<%@ Control Language="VB" AutoEventWireup="false" CodeFile="header.ascx.vb" Inherits="includes_header" %>
<div class="header">
   <div class="logo"></div>
   <div class="logo_right">Counselman Automotive Recycling<br />3019 St. Stephens Rd.<br />Mobile, Al. 36612<br /><br />
   <asp:Label ID="lblInfo" runat="server" Text=""></asp:Label>
   </div>
</div>
<br /><br /><br />
<asp:Panel ID="pnlNav" CssClass="nav" runat="server">
    <asp:HyperLink ID="lnkHome" runat="server" NavigateUrl="/default.aspx" CssClass="nav">HOME</asp:HyperLink><asp:Label ID="lblHome" runat="server" Text="&nbsp;|&nbsp;"></asp:Label>
    <asp:HyperLink ID="lnkWebAccounts" runat="server" onclick="tasks1_iFrame_Tasks.src='/tasks/webaccounts.aspx'" CssClass="nav">Web Accounts</asp:HyperLink><asp:Label ID="lblWebAccounts" runat="server" Text="&nbsp;|&nbsp;"></asp:Label>
    <asp:HyperLink ID="lnkJunkVehicles" runat="server" onclick="tasks1_iFrame_Tasks.src='/tasks/junkvehicles.aspx'" CssClass="nav">Vehicles To Junk</asp:HyperLink><asp:Label ID="lblJunkVehicles" runat="server" Text="&nbsp;|&nbsp;"></asp:Label>
    <asp:HyperLink ID="lnkReports" runat="server" onclick="tasks1_iFrame_Tasks.src='/tasks/reports.aspx'" CssClass="nav">Reports</asp:HyperLink><asp:Label ID="lblReports" runat="server" Text="&nbsp;|&nbsp;"></asp:Label>
    <asp:HyperLink ID="lnkDeliveryTickets" runat="server" onclick="tasks1_iFrame_Tasks.src='/tasks/deliverytickets.aspx'" CssClass="nav">Delivery Tickets</asp:HyperLink>
</asp:Panel>
<asp:Panel ID="pnlShadow" CssClass="shadow" runat="server">
</asp:Panel>
<!--<a href="#" onclick="tasks1_iFrame_Tasks.location.href='/tasks/configuration.aspx'" class="nav">Configuration</a>&nbsp;|&nbsp;<a href="#" onclick="tasks1_iFrame_Tasks.location.href='/tasks/vehiclestojunk.aspx'" class="nav">Vehicles to Junk</a>&nbsp;|&nbsp; -->
		