﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="dailyreturns.aspx.vb" Inherits="reports_dailyreturns" Debug="true" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Daily Returns</title>
    <link href="../style.css" rel="stylesheet" type="text/css" />
    <link href="../table_style.css" rel="stylesheet" type="text/css" />
</head>
<body>
    <form id="form1" runat="server">
    <div style="width: 50%; float: left;"><h1><asp:Label ID="lblHeader" runat="server" Text=""></asp:Label></h1></div>
    <div style="width: 50%; float: right; text-align: right;"><h1><asp:Label ID="lblDate" runat="server" Text=""></asp:Label></h1></div>
    <div class="clear">
    <asp:Label ID="lblMessage" runat="server" Text=""></asp:Label>
    <asp:Table ID="tDailyReturns" runat="server" CellPadding="2" CellSpacing="0">
    </asp:Table>
    </div>
    </form>
</body>
</html>
