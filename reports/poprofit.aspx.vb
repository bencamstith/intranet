﻿Imports cardb

Partial Class reports_poprofit
   Inherits System.Web.UI.Page
   Private pintTableWidth As Integer = 800
   Private pstrStartDate As String = ""
   Private pstrEndDate As String = ""

   Private pobjSuperAccess As Boolean = False

   Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        'pobjSuperAccess = CType(Session("SuperAccess"), Boolean)

        'If Not pobjSuperAccess Then Response.End()

      If Not IsPostBack Then
         Me.ddlMonth.SelectedValue = Date.Now.Month
         For x As Integer = Date.Now.Year - 8 To Date.Now.Year
            Me.ddlYear.Items.Add(New ListItem(x, x))
         Next
         Me.ddlYear.SelectedValue = Date.Now.Year
      End If
   End Sub

   Private Sub BindReport(ByVal tblMain As Table, Optional ByVal strVendor As String = "")
      Me.tblQuery.Visible = False
      tblMain.Visible = True
      Dim tbl As Table
      Dim tr As TableRow
      Dim td As TableCell
      Dim strPreviousPO As String = ""
      Dim strPreviousVendorName As String = ""
      Dim decTotalCost As Decimal = 0

      Dim strWhere As String = ""

      'If Me.chkHideMargin.Checked Then
      ' strWhere = "(Customer = '" & strVendor.Replace("'", "''") & "') AND (LineItemType IS NOT NULL) AND (PODate BETWEEN CONVERT(DATETIME, '" & pstrStartDate & "', 102) AND CONVERT(DATETIME, '" & pstrEndDate & "', 102))"
      'Else
      strWhere = "(LineItemType IS NOT NULL) AND (PODate BETWEEN CONVERT(DATETIME, '" & pstrStartDate & "', 102) AND CONVERT(DATETIME, '" & pstrEndDate & "', 102))"
      'End If

      'With (New DevNet.Tools.Settings.Connections("CARPODB"))
      '   If .ErrorMsg.Length > 0 Then
      '      Me.lblErrors.Visible = True
      '      Me.lblErrors.Text = .ErrorMsg
      '      Exit Sub
      '   End If
      'End With

      Using objPOReport As New Database.POReconcileUNIONs(strWhere, "VendorName, PONumber, PODate")
         'Using objPOReport As New Database.POReconcileUNIONs
         For Each objError As DevNet.Tools.Common.ValidationError In objPOReport.ValidationErrors
            Me.lblErrors.Visible = True
            Me.lblErrors.Text = Me.lblErrors.Text & objError.Message & "<br>"
         Next
         tblMain.Width = New Unit(pintTableWidth, UnitType.Pixel)
         tblMain.Rows.Add(New TableRow)
         td = New TableCell
         tblMain.Rows(0).Cells.Add(td)

         tbl = New Table
         tbl.Width = New Unit(pintTableWidth, UnitType.Pixel)

         tbl.BorderStyle = BorderStyle.Solid
         tbl.BorderWidth = New Unit(2, UnitType.Pixel)
         td.Controls.Add(tbl)

         tr = New TableRow
         td = New TableCell
         tbl.Rows.Add(tr)
         tr.Cells.Add(td)
         td.ColumnSpan = 18
         'If Me.chkHideMargin.Checked Then
         ' td.Text = strVendor
         ' Else
         td.Text = "Purchase Order Profitability"
         'End If
         td.HorizontalAlign = HorizontalAlign.Center
         td.CssClass = "reporttitle"

         tr = New TableRow
         td = New TableCell
         tbl.Rows.Add(tr)
         tr.Cells.Add(td)
         td.ColumnSpan = 18
         td.Text = CType(pstrStartDate, Date).ToString("MM/dd/yyyy") & " - " & CType(pstrEndDate, Date).ToString("MM/dd/yyyy")
         td.HorizontalAlign = HorizontalAlign.Center
         td.CssClass = "tabledata"

         tbl.Rows.Add(FillHeader())

         For Each objRow As Database.POReconcileUNION In objPOReport
            If strPreviousPO <> objRow.PONumber.ToString Then
               tr = New TableRow
               td = New TableCell
               tbl.Rows.Add(tr)
               tr.Cells.Add(td)
               td.ColumnSpan = 18
               td.Text = "<hr noshade>"
               strPreviousPO = objRow.PONumber.ToString
            End If

            If strPreviousVendorName <> objRow.VendorName Then
               tr = New TableRow
               td = New TableCell
               tbl.Rows.Add(tr)
               tr.Cells.Add(td)
               td.ColumnSpan = 18
               tbl.Rows.Add(FillFooter(decTotalCost))
               strPreviousVendorName = objRow.VendorName

               decTotalCost = 0
               tr = New TableRow
               td = New TableCell
               tbl.Rows.Add(tr)
               tr.Cells.Add(td)
               td.ColumnSpan = 18
               td.Text = "<hr noshade>"
            End If

            tbl.Rows.Add(FillRow(objRow))

            If objRow.Recommendation.ToUpper <> "DON'T PAY" Then
               decTotalCost += objRow.Cost
            End If
         Next
      End Using
   End Sub

   Private Function FillHeader() As TableRow
      Dim tr As New TableRow
      Dim td As TableCell

      tr.Width = New Unit(pintTableWidth, UnitType.Pixel)
      td = New TableCell
      tr.Cells.Add(td)
      td.CssClass = "tableheader"
      td.Text = "PO#"
      ' td.Width = New Unit(pintColumnWidth1, UnitType.Pixel)

      td = New TableCell
      tr.Cells.Add(td)
      td.CssClass = "tableheader"
      td.Text = "PO&nbsp;Status"
      ' td.Width = New Unit(pintColumnWidth2, UnitType.Pixel)

      td = New TableCell
      tr.Cells.Add(td)
      td.CssClass = "tableheader"
      td.Text = "Recommend"
      'td.Width = New Unit(pintColumnWidth3, UnitType.Pixel)

      td = New TableCell
      tr.Cells.Add(td)
      td.CssClass = "tableheader"
      td.Text = "PO&nbsp;Date"
      td.HorizontalAlign = HorizontalAlign.Center
      'td.Width = New Unit(pintColumnWidth4, UnitType.Pixel)

      td = New TableCell
      tr.Cells.Add(td)
      td.CssClass = "tableheader"
      td.Text = "PO&nbsp;SLS"
      td.HorizontalAlign = HorizontalAlign.Center
      'td.Width = New Unit(pintColumnWidth4, UnitType.Pixel)

      'If Me.chkHideMargin.Checked = False Then
      td = New TableCell
      tr.Cells.Add(td)
      td.CssClass = "tableheader"
      td.Text = "Vendor"
      td.HorizontalAlign = HorizontalAlign.Left
      'td.Width = New Unit(pintColumnWidth4, UnitType.Pixel)
      'End If

      td = New TableCell
      tr.Cells.Add(td)
      td.CssClass = "tableheader"
      td.Text = "Inventory&nbsp;#"
      ' td.Width = New Unit(pintColumnWidth5, UnitType.Pixel)

      td = New TableCell
      tr.Cells.Add(td)
      td.CssClass = "tableheader"
      td.Text = "Year"
      ' td.Width = New Unit(pintColumnWidth6, UnitType.Pixel)

      td = New TableCell
      tr.Cells.Add(td)
      td.CssClass = "tableheader"
      td.Text = "Model"
      ' td.Width = New Unit(pintColumnWidth7, UnitType.Pixel)

      td = New TableCell
      tr.Cells.Add(td)
      td.CssClass = "tableheader"
      td.Text = "Notes"
      ' td.Width = New Unit(pintColumnWidth8, UnitType.Pixel)

      td = New TableCell
      tr.Cells.Add(td)
      td.CssClass = "tableheader"
      td.Text = "Customer"
      'td.Width = New Unit(pintColumnWidth9, UnitType.Pixel)

      td = New TableCell
      tr.Cells.Add(td)
      td.CssClass = "tableheader"
      td.Text = "SLS"
      'td.Width = New Unit(pintColumnWidth10, UnitType.Pixel)

      td = New TableCell
      tr.Cells.Add(td)
      td.CssClass = "tableheader"
      td.Text = "Invoice&nbsp;Date"
      'td.Width = New Unit(pintColumnWidth11, UnitType.Pixel)

      td = New TableCell
      tr.Cells.Add(td)
      td.CssClass = "tableheader"
      td.Text = "Invoice&nbsp;#"
      ' td.Width = New Unit(pintColumnWidth12, UnitType.Pixel)


      td = New TableCell
      tr.Cells.Add(td)
      td.CssClass = "tableheader"
      td.Text = "Cost&nbsp;$"
      td.HorizontalAlign = HorizontalAlign.Center
      ' td.Width = New Unit(pintColumnWidth13, UnitType.Pixel)

      'If chkHideMargin.Checked = False Then
      td = New TableCell
      tr.Cells.Add(td)
      td.CssClass = "tableheader"
      td.Text = "Sold&nbsp;$"
      td.HorizontalAlign = HorizontalAlign.Center
      ' td.Width = New Unit(pintColumnWidth14, UnitType.Pixel)
      'End If

      'If chkHideMargin.Checked = False Then
      td = New TableCell
      tr.Cells.Add(td)
      td.CssClass = "tableheader"
      td.Text = "Profit&nbsp;$"
      td.HorizontalAlign = HorizontalAlign.Center
      ' td.Width = New Unit(pintColumnWidth15, UnitType.Pixel)
      'End If

      'If chkHideMargin.Checked = False Then
      td = New TableCell
      tr.Cells.Add(td)
      td.CssClass = "tableheader"
      td.Text = "Markup&nbsp;%"
      td.HorizontalAlign = HorizontalAlign.Center
      'td.Width = New Unit(pintColumnWidth16, UnitType.Pixel)
      'End If

      Return tr
   End Function

   Private Function FillFooter(ByVal decTotalCost As Decimal) As TableRow
      Dim tr As New TableRow
      Dim td As TableCell

      tr.Width = New Unit(pintTableWidth, UnitType.Pixel)
      td = New TableCell
      tr.Cells.Add(td)
      td.CssClass = "tabledata"
      td.Text = ""
      'td.Width = New Unit(pintColumnWidth1, UnitType.Pixel)

      td = New TableCell
      tr.Cells.Add(td)
      td.CssClass = "tabledata"
      td.Text = ""
      ' td.Width = New Unit(pintColumnWidth2, UnitType.Pixel)

      'If Me.chkHideMargin.Checked = False Then
      td = New TableCell
      tr.Cells.Add(td)
      td.CssClass = "tabledata"
      td.Text = ""
      ' td.Width = New Unit(pintColumnWidth3, UnitType.Pixel)
      'End If

      td = New TableCell
      tr.Cells.Add(td)
      td.CssClass = "tabledata"
      td.Text = ""
      ' td.Width = New Unit(pintColumnWidth3, UnitType.Pixel)

      td = New TableCell
      tr.Cells.Add(td)
      td.CssClass = "tabledata"
      td.Text = ""
      td.HorizontalAlign = HorizontalAlign.Center
      'td.Width = New Unit(pintColumnWidth4, UnitType.Pixel)

      td = New TableCell
      tr.Cells.Add(td)
      td.CssClass = "tabledata"
      td.Text = ""
      td.HorizontalAlign = HorizontalAlign.Center
      'td.Width = New Unit(pintColumnWidth4, UnitType.Pixel)

      td = New TableCell
      tr.Cells.Add(td)
      td.CssClass = "tabledata"
      td.Text = ""
      'td.Width = New Unit(pintColumnWidth5, UnitType.Pixel)

      td = New TableCell
      tr.Cells.Add(td)
      td.CssClass = "tabledata"
      td.Text = ""
      ' td.Width = New Unit(pintColumnWidth6, UnitType.Pixel)

      td = New TableCell
      tr.Cells.Add(td)
      td.CssClass = "tabledata"
      td.Text = ""
      'td.Width = New Unit(pintColumnWidth7, UnitType.Pixel)

      td = New TableCell
      tr.Cells.Add(td)
      td.CssClass = "tabledata"
      td.Text = ""
      'td.Width = New Unit(pintColumnWidth8, UnitType.Pixel)

      td = New TableCell
      tr.Cells.Add(td)
      td.CssClass = "tabledata"
      td.Text = ""
      'td.Width = New Unit(pintColumnWidth9, UnitType.Pixel)

      td = New TableCell
      tr.Cells.Add(td)
      td.CssClass = "tabledata"
      td.Text = ""
      'td.Width = New Unit(pintColumnWidth10, UnitType.Pixel)

      td = New TableCell
      tr.Cells.Add(td)
      td.CssClass = "tabledata"
      td.Text = ""
      'td.Width = New Unit(pintColumnWidth11, UnitType.Pixel)

      td = New TableCell
      tr.Cells.Add(td)
      td.CssClass = "tabledata"
      td.Text = "Total"
      'td.Width = New Unit(pintColumnWidth12, UnitType.Pixel)
      td.HorizontalAlign = HorizontalAlign.Right


      td = New TableCell
      tr.Cells.Add(td)
      td.CssClass = "tabledata"
      td.Text = FormatCurrency(decTotalCost, 2)
      td.HorizontalAlign = HorizontalAlign.Right
      'td.Width = New Unit(pintColumnWidth13, UnitType.Pixel)

      'If chkHideMargin.Checked = False Then
      td = New TableCell
      tr.Cells.Add(td)
      td.CssClass = "tabledata"
      td.Text = ""
      td.HorizontalAlign = HorizontalAlign.Center
      'td.Width = New Unit(pintColumnWidth14, UnitType.Pixel)
      ' End If

      'If chkHideMargin.Checked = False Then
      td = New TableCell
      tr.Cells.Add(td)
      td.CssClass = "tabledata"
      td.Text = ""
      td.HorizontalAlign = HorizontalAlign.Center
      'td.Width = New Unit(pintColumnWidth15, UnitType.Pixel)
      'End If

      'If chkHideMargin.Checked = False Then
      td = New TableCell
      tr.Cells.Add(td)
      td.CssClass = "tabledata"
      td.Text = ""
      td.HorizontalAlign = HorizontalAlign.Center
      'td.Width = New Unit(pintColumnWidth16, UnitType.Pixel)
      'End If

      Return tr
   End Function

   Private Function FillRow(ByVal objPORow As Database.POReconcileUNION) As TableRow
      Dim tr As New TableRow
      Dim td As TableCell

      td = New TableCell
      tr.Cells.Add(td)
      td.CssClass = "tabledata"
      td.Text = objPORow.PONumber
      'td.Width = New Unit(pintColumnWidth1, UnitType.Pixel)
      td.VerticalAlign = VerticalAlign.Top

      td = New TableCell
      tr.Cells.Add(td)
      td.CssClass = "tabledata"
      td.Text = objPORow.Status.Replace(" ", "&nbsp;")
      If objPORow.Status.ToUpper = "VOID" Then td.CssClass = "tabledatared"
      ' td.Width = New Unit(pintColumnWidth2, UnitType.Pixel)
      td.VerticalAlign = VerticalAlign.Top

      td = New TableCell
      tr.Cells.Add(td)
      td.CssClass = "tabledata"
      td.Text = objPORow.Recommendation.Replace(" ", "&nbsp;")
      Select Case objPORow.Recommendation.ToUpper
         Case Is = "PAY"
            td.CssClass = "tabledatagreen"
         Case Is = "DON'T PAY"
            td.CssClass = "tabledatared"
         Case Is = "PARTIAL PAY"
            td.CssClass = "tabledatayellow"
      End Select
      'td.Width = New Unit(pintColumnWidth3, UnitType.Pixel)
      td.VerticalAlign = VerticalAlign.Top

      td = New TableCell
      tr.Cells.Add(td)
      td.CssClass = "tabledata"
      If IsDate(objPORow.PODate) Then
         td.Text = CType(objPORow.PODate, Date).ToString("MM/dd/yyyy").Replace(" ", "&nbsp;")
      Else
         td.Text = ""
      End If

      td = New TableCell
      tr.Cells.Add(td)
      td.CssClass = "tabledata"
      td.Text = objPORow.POCreatedBy

      ' td.Width = New Unit(pintColumnWidth4, UnitType.Pixel)
      td.VerticalAlign = VerticalAlign.Top

      'If Me.chkHideMargin.Checked = False Then
      td = New TableCell
      tr.Cells.Add(td)
      td.CssClass = "tabledata"
      td.Text = objPORow.VendorName.Replace(" ", "&nbsp;")
      ' td.Width = New Unit(pintColumnWidth5, UnitType.Pixel)
      td.VerticalAlign = VerticalAlign.Top
      'End If

      td = New TableCell
      tr.Cells.Add(td)
      td.CssClass = "tabledata"
      td.Text = objPORow.InventoryNumber.Replace(" ", "&nbsp;")
      ' td.Width = New Unit(pintColumnWidth5, UnitType.Pixel)
      td.VerticalAlign = VerticalAlign.Top

      td = New TableCell
      tr.Cells.Add(td)
      td.CssClass = "tabledata"
      td.Text = objPORow.ModelYear.ToString.Replace(" ", "&nbsp;")
      '  td.Width = New Unit(pintColumnWidth6, UnitType.Pixel)
      td.VerticalAlign = VerticalAlign.Top

      td = New TableCell
      tr.Cells.Add(td)
      td.CssClass = "tabledata"
      td.Text = objPORow.ModelName.Replace(" ", "&nbsp;")
      ' td.Width = New Unit(pintColumnWidth7, UnitType.Pixel)
      td.VerticalAlign = VerticalAlign.Top

      td = New TableCell
      tr.Cells.Add(td)
      td.CssClass = "tabledata"
      td.Text = objPORow.InvoiceNotes
      If objPORow.InvoiceNotes.Length > 40 Then
         td.Text = objPORow.InvoiceNotes.Substring(0, 40).Replace(" ", "&nbsp;")
         td.Text = td.Text & objPORow.InvoiceNotes.Substring(41, objPORow.InvoiceNotes.Length - 41)
      Else
         td.Text = objPORow.InvoiceNotes.Replace(" ", "&nbsp;")
      End If

      ' td.Width = New Unit(pintColumnWidth8, UnitType.Pixel)
      td.VerticalAlign = VerticalAlign.Top

      td = New TableCell
      tr.Cells.Add(td)
      td.CssClass = "tabledata"
      td.Text = objPORow.BillToBusinessName.ToString.Replace(" ", "&nbsp;")
      'td.Width = New Unit(pintColumnWidth9, UnitType.Pixel)
      td.VerticalAlign = VerticalAlign.Top

      td = New TableCell
      tr.Cells.Add(td)
      td.CssClass = "tabledata"
      td.Text = objPORow.EmployeeInitials.ToString.Replace(" ", "&nbsp;")
      ' td.Width = New Unit(pintColumnWidth10, UnitType.Pixel)
      td.VerticalAlign = VerticalAlign.Top

      td = New TableCell
      tr.Cells.Add(td)
      td.HorizontalAlign = HorizontalAlign.Right
      td.CssClass = "tabledata"
      If IsDate(objPORow.InvoiceDate) Then
         td.Text = CType(objPORow.InvoiceDate, Date).ToString("MM/dd/yyyy").ToString.Replace(" ", "&nbsp;")
      Else
         td.Text = "&nbsp;"
      End If

      ' td.Width = New Unit(pintColumnWidth11, UnitType.Pixel)
      td.VerticalAlign = VerticalAlign.Top

      td = New TableCell
      tr.Cells.Add(td)
      td.CssClass = "tabledata"
      td.Text = objPORow.InvoiceNumber.ToString.Replace(" ", "&nbsp;")
      ' td.Width = New Unit(pintColumnWidth12, UnitType.Pixel)
      td.VerticalAlign = VerticalAlign.Top


      td = New TableCell
      tr.Cells.Add(td)
      td.HorizontalAlign = HorizontalAlign.Right
      td.CssClass = "tabledata"
      td.Text = FormatCurrency(objPORow.Cost, 2).ToString.Replace(" ", "&nbsp;")
      If objPORow.Cost < 0 Then td.CssClass = "tabledatared"
      'td.Width = New Unit(pintColumnWidth13, UnitType.Pixel)
      td.VerticalAlign = VerticalAlign.Top

      'If chkHideMargin.Checked = False Then
      td = New TableCell
      tr.Cells.Add(td)
      td.HorizontalAlign = HorizontalAlign.Right
      td.CssClass = "tabledata"
      td.Text = FormatCurrency(objPORow.SoldFor, 2).ToString.Replace(" ", "&nbsp;")
      If objPORow.SoldFor < 0 Then td.CssClass = "tabledatared"
      ' td.Width = New Unit(pintColumnWidth14, UnitType.Pixel)
      td.VerticalAlign = VerticalAlign.Top
      ' End If

      'If chkHideMargin.Checked = False Then
      td = New TableCell
      tr.Cells.Add(td)
      td.HorizontalAlign = HorizontalAlign.Right
      td.CssClass = "tabledata"
      td.Text = FormatCurrency(objPORow.Profit, 2).ToString.Replace(" ", "&nbsp;")
      If objPORow.Profit < 0 Then td.CssClass = "tabledatared"
      ' td.Width = New Unit(pintColumnWidth15, UnitType.Pixel)
      td.VerticalAlign = VerticalAlign.Top
      'End If

      'If chkHideMargin.Checked = False Then
      td = New TableCell
      tr.Cells.Add(td)
      td.HorizontalAlign = HorizontalAlign.Right
      td.CssClass = "tabledata"
      td.Text = (FormatNumber(objPORow.Profit / objPORow.Cost, 2) * 100).ToString & "%".ToString.Replace(" ", "&nbsp;")
      ' td.Width = New Unit(pintColumnWidth16, UnitType.Pixel)
      td.VerticalAlign = VerticalAlign.Top
      ' End If

      Return tr
   End Function

   Protected Sub btnSubmit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSubmit.Click
      If IsDate(Me.ddlMonth.SelectedValue & "/01/" & Me.ddlYear.SelectedValue) Then
         pstrStartDate = Me.ddlMonth.SelectedValue & "/01/" & Me.ddlYear.SelectedValue

         If Me.ddlMonth.SelectedValue = "12" Then
            pstrEndDate = CType("01/01/" & CType(Me.ddlYear.SelectedValue, Integer) + 1, Date).AddDays(-1)
         Else
            pstrEndDate = CType((CType(Me.ddlMonth.SelectedValue, Integer) + 1).ToString & "/01/" & Me.ddlYear.SelectedValue, Date).AddDays(-1)
         End If

         'If Me.chkHideMargin.Checked Then
         '    Me.tblReport.Visible = True
         '    Dim strWhere As String = "(LineItemType IS NOT NULL) AND (PODate BETWEEN CONVERT(DATETIME, '" & pstrStartDate & "', 102) AND CONVERT(DATETIME, '" & pstrEndDate & "', 102))"
         '    Dim tbl As Table
         '    Dim tr As TableRow
         '    Dim td As TableCell
         '    Dim strCustomer As String = ""

         '    Using objPOReport As New Database.POReconcileUNIONs(strWhere, "Customer, PONumber, PODate")
         '        For Each objError As DevNet.Tools.Common.ValidationError In objPOReport.ValidationErrors
         '            Me.lblErrors.Visible = True
         '            Me.lblErrors.Text = Me.lblErrors.Text & objError.Message & "<br>"
         '        Next
         '        For Each objRow As Database.POReconcileUNION In objPOReport
         '            If strCustomer <> objRow.VendorName Then
         '                tbl = New Table
         '                tbl.Width = New Unit(pintTableWidth, UnitType.Pixel)
         '                tr = New TableRow
         '                td = New TableCell
         '                Me.tblReport.Rows.Add(tr)
         '                tr.Cells.Add(td)
         '                td.Controls.Add(tbl)
         '                BindReport(tbl, objRow.VendorName)

         '                tr = New TableRow
         '                td = New TableCell
         '                Me.tblReport.Rows.Add(tr)
         '                tr.Cells.Add(td)
         '                td.Text = "<br class=page>"
         '                strCustomer = objRow.VendorName
         '            End If
         '        Next
         '    End Using
         'Else
         BindReport(Me.tblReport)
         'End If
      End If
   End Sub
End Class
