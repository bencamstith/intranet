﻿
Partial Class reports_mmdc_detail
    Inherits System.Web.UI.Page
    Private pintMMDCDGroupKey As Integer = 0

    Protected Sub Page_Load(sender As Object, e As System.EventArgs) Handles Me.Load
        Me.lblMessage.Text = ""
        If Not Request.QueryString("groupkey") Is Nothing Then
            pintMMDCDGroupKey = CType(Request.QueryString("groupkey"), Integer)
        End If

        BindHeader()
        BindMMDCDetails()
    End Sub

    Private Sub BindHeader()
        Using objMMDCDGroups As New cardb.Database.MMDCDGroups("MMDCDGroupKey = " & pintMMDCDGroupKey.ToString)
            If objMMDCDGroups.Count > 0 Then
                With objMMDCDGroups(0)
                    Me.lblYear.Text = .YearRange
                    Me.lblModel.Text = .Model
                    Me.lblAvgCost.Text = String.Format("{0:c}", .AvgCost)
                    Me.lblAvgIncome.Text = String.Format("{0:c}", .AvgIncome)
                    Me.lblAvgProfit.Text = String.Format("{0:c}", .AvgIncome - .AvgCost)
                    Me.lblAvgProfit.Text = String.Format("{0:c}", .AvgProfit)
                    If .AvgCost > 0 Then
                        Me.lblAvgROI.Text = FormatNumber(.AvgIncome / .AvgCost, 2)
                    Else
                        Me.lblAvgROI.Text = "0.00"
                    End If
                    Me.lblAvgROI.Text = FormatNumber(.AvgROI, 2)
                    Me.lblBED.Text = .BED
                    Me.lblTotalActivity.Text = .TotalActivity
                    Me.lblNeed.Text = FormatNumber(.Need, 2)
                End With
            End If
        End Using
    End Sub

    Private Sub BindMMDCDetails()
        Dim tr As TableRow
        Dim td As TableCell

        tr = New TableRow

        Me.tMMDCDetails.Rows.Add(tr)
        td = New TableCell
        tr.Cells.Add(td)
        td.CssClass = "tdhead"
        td.Text = "Part Type"

        td = New TableCell
        tr.Cells.Add(td)
        td.CssClass = "tdhead"
        td.HorizontalAlign = HorizontalAlign.Center
        td.Text = "90 Day<br />Out of Stock Activity"

        td = New TableCell
        tr.Cells.Add(td)
        td.CssClass = "tdhead"
        td.HorizontalAlign = HorizontalAlign.Center
        td.Text = "90 Day<br />Total Activity"

        td = New TableCell
        tr.Cells.Add(td)
        td.CssClass = "tdhead"
        td.HorizontalAlign = HorizontalAlign.Center
        td.Text = "90 Day<br />Sales"

        td = New TableCell
        tr.Cells.Add(td)
        td.CssClass = "tdhead"
        td.HorizontalAlign = HorizontalAlign.Center
        td.Text = "QOH"

        td = New TableCell
        tr.Cells.Add(td)
        td.CssClass = "tdhead"
        td.HorizontalAlign = HorizontalAlign.Center
        td.Text = "Need"

        td = New TableCell
        tr.Cells.Add(td)
        td.CssClass = "tdhead"
        td.HorizontalAlign = HorizontalAlign.Center
        td.Text = "Avg Price"

        td = New TableCell
        tr.Cells.Add(td)
        td.CssClass = "tdhead"
        td.HorizontalAlign = HorizontalAlign.Center
        td.Text = "Avg<br />Exchange Price"

        'Using objMMDCDReports As New cardb.Database.MMDCDReports("MMDCDGroupKey = " & Request.QueryString("groupkey"), "PartType")
        Using objMMDCDReports As New cardb.Database.MMDCDReports("MMDCDGroupKey = " & pintMMDCDGroupKey.ToString, "PartType")
            'MMDCDGroupKey = 2110
            If objMMDCDReports.Count = 0 Then
                Me.lblMessage.Text = "(0) Part Type Records."
            End If
            For Each objMMDCDReport As cardb.Database.MMDCDReport In objMMDCDReports
                tr = New TableRow
                Me.tMMDCDetails.Rows.Add(tr)
                td = New TableCell
                tr.Cells.Add(td)
                td.Text = objMMDCDReport.PartType

                td = New TableCell
                tr.Cells.Add(td)
                td.Text = objMMDCDReport.OutOfStockActivity

                td = New TableCell
                tr.Cells.Add(td)
                td.Text = objMMDCDReport.TotalActivity

                td = New TableCell
                tr.Cells.Add(td)
                td.Text = objMMDCDReport.Sales

                td = New TableCell
                tr.Cells.Add(td)
                td.Text = objMMDCDReport.QOH

                td = New TableCell
                tr.Cells.Add(td)
                td.Text = FormatNumber(objMMDCDReport.Need, 2)

                td = New TableCell
                tr.Cells.Add(td)
                td.Text = FormatCurrency(objMMDCDReport.AvgPrice, 2)

                td = New TableCell
                tr.Cells.Add(td)
                td.Text = FormatCurrency(objMMDCDReport.AvgExchangePrice, 2)
            Next

            For Each objErr As DevNet.Tools.Common.ValidationError In objMMDCDReports.ValidationErrors
                Me.lblMessage.Text &= objErr.Message
            Next
        End Using
    End Sub

    Protected Sub lnkPrint_Click(sender As Object, e As System.EventArgs) Handles lnkPrint.Click
        If Not Request.QueryString("groupkey") Is Nothing Then
            cardb.Tools.TaskQueues.AddTaskToQueue(cardb.Tools.TaskQueues.eTaskTypes.MMDCReport, Request.QueryString("groupkey").ToString, "")
        End If
    End Sub
End Class
