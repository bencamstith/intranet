﻿
Partial Class reports_print
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(sender As Object, e As System.EventArgs) Handles Me.Load
        Me.lblMessage.Text = ""
        If Not Request.QueryString("mmdcdgroupkey") Is Nothing Then
            cardb.Tools.TaskQueues.AddTaskToQueue(cardb.Tools.TaskQueues.eTaskTypes.MMDCReport, Request.QueryString("mmdcdgroupkey").ToString, "")
            Me.lblMessage.Text = "Document queued to print. Clear <a href=""mmdc_index.aspx"">here</a> to return."
        End If
    End Sub
End Class
