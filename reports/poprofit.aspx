﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="poprofit.aspx.vb" Inherits="reports_poprofit" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Purchase Order Profitability</title>
    <link href="stylesheet_poreport.css" rel="stylesheet" type="text/css" />
    <style>
        br.page { page-break-after: always }
    </style>
</head>
<body>
    <form id="form1" runat="server">
    <asp:Label ID="lblErrors" runat="server" Text="Label" Visible="false"></asp:Label>
    <asp:Table ID="tblQuery" runat="server" CssClass="tabledata" CellPadding="2" 
        CellSpacing="0" Width="260px">
        <asp:TableRow>
            <asp:TableCell ColumnSpan="3" HorizontalAlign="Center" CssClass="reporttitle">P.O. Profitability</asp:TableCell>
        </asp:TableRow>
        <asp:TableRow>
            <asp:TableCell>Month</asp:TableCell>
            <asp:TableCell>:</asp:TableCell>
            <asp:TableCell>
                <asp:DropDownList ID="ddlMonth" runat="server" Width="150px">
                <asp:ListItem Value="1">January</asp:ListItem>
                <asp:ListItem Value="2">February</asp:ListItem>
                <asp:ListItem Value="3">March</asp:ListItem>
                <asp:ListItem Value="4">April</asp:ListItem>
                <asp:ListItem Value="5">May</asp:ListItem>
                <asp:ListItem Value="6">June</asp:ListItem>
                <asp:ListItem Value="7">July</asp:ListItem>
                <asp:ListItem Value="8">August</asp:ListItem>
                <asp:ListItem Value="9">September</asp:ListItem>
                <asp:ListItem Value="10">October</asp:ListItem>
                <asp:ListItem Value="11">November</asp:ListItem>
                <asp:ListItem Value="12">December</asp:ListItem>
                </asp:DropDownList>
            </asp:TableCell>
        </asp:TableRow>
        <asp:TableRow>
            <asp:TableCell>Year</asp:TableCell>
            <asp:TableCell>:</asp:TableCell>
            <asp:TableCell>
                <asp:DropDownList ID="ddlYear" runat="server" Width="150px">
                </asp:DropDownList>
            </asp:TableCell>
        </asp:TableRow>
        <asp:TableRow>
            <asp:TableCell HorizontalAlign="Right" ColumnSpan="3">
                <asp:Button ID="btnSubmit" runat="server" Text="Submit" /></asp:TableCell>
        </asp:TableRow>
    </asp:Table>
    
    <asp:Table ID="tblReport" runat="server" Visible="false"></asp:Table>
    </form>
</body>
</html>
