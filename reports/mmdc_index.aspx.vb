﻿
Partial Class reports_mmdc_index
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(sender As Object, e As System.EventArgs) Handles Me.Load
        BindMMDCIndex()
    End Sub

    Private Sub BindMMDCIndex()
        Dim tr As TableRow
        Dim td As TableCell
        Dim lnk As HyperLink
        Dim strSortOrder As String = ""

        Dim strURL_Year As String = "mmdc_index.aspx?sort=1"
        Dim strURL_Model As String = "mmdc_index.aspx?sort=3"
        Dim strURL_AvgCost As String = "mmdc_index.aspx?sort=5"
        Dim strURL_AvgIncome As String = "mmdc_index.aspx?sort=7"
        Dim strURL_AvgProfit As String = "mmdc_index.aspx?sort=9"
        Dim strURL_AvgROI As String = "mmdc_index.aspx?sort=11"
        Dim strURL_BED As String = "mmdc_index.aspx?sort=13"
        Dim strURL_TotalActvity As String = "mmdc_index.aspx?sort=15"
        Dim strURL_Need As String = "mmdc_index.aspx?sort=17"

        If Not Request.QueryString("sort") Is Nothing Then
            Select Case True
                Case Request.QueryString("sort").ToString = "1"
                    strSortOrder = "Year DESC"
                    strURL_Year = "mmdc_index.aspx?sort=2"
                Case Request.QueryString("sort").ToString = "2"
                    strSortOrder = "Year"
                    strURL_Year = "mmdc_index.aspx?sort=1"
                Case Request.QueryString("sort").ToString = "3"
                    strSortOrder = "Model DESC"
                    strURL_Model = "mmdc_index.aspx?sort=4"
                Case Request.QueryString("sort").ToString = "4"
                    strSortOrder = "Model"
                    strURL_Model = "mmdc_index.aspx?sort=3"
                Case Request.QueryString("sort").ToString = "5"
                    strSortOrder = "AvgCost DESC"
                    strURL_AvgCost = "mmdc_index.aspx?sort=6"
                Case Request.QueryString("sort").ToString = "6"
                    strSortOrder = "AvgCost"
                    strURL_AvgCost = "mmdc_index.aspx?sort=5"
                Case Request.QueryString("sort").ToString = "7"
                    strSortOrder = "AvgIncome DESC"
                    strURL_AvgIncome = "mmdc_index.aspx?sort=8"
                Case Request.QueryString("sort").ToString = "8"
                    strSortOrder = "AvgIncome"
                    strURL_AvgIncome = "mmdc_index.aspx?sort=7"
                Case Request.QueryString("sort").ToString = "9"
                    strSortOrder = "AvgProfit DESC"
                    strURL_AvgProfit = "mmdc_index.aspx?sort=10"
                Case Request.QueryString("sort").ToString = "10"
                    strSortOrder = "AvgProfit"
                    strURL_AvgProfit = "mmdc_index.aspx?sort=9"
                Case Request.QueryString("sort").ToString = "11"
                    strSortOrder = "AvgROI DESC"
                    strURL_AvgROI = "mmdc_index.aspx?sort=12"
                Case Request.QueryString("sort").ToString = "12"
                    strSortOrder = "AvgROI"
                    strURL_AvgROI = "mmdc_index.aspx?sort=11"
                Case Request.QueryString("sort").ToString = "13"
                    strSortOrder = "BED DESC"
                    strURL_BED = "mmdc_index.aspx?sort=14"
                Case Request.QueryString("sort").ToString = "14"
                    strSortOrder = "BED"
                    strURL_BED = "mmdc_index.aspx?sort=13"
                Case Request.QueryString("sort").ToString = "15"
                    strSortOrder = "TotalActivity DESC"
                    strURL_TotalActvity = "mmdc_index.aspx?sort=16"
                Case Request.QueryString("sort").ToString = "16"
                    strSortOrder = "TotalActivity"
                    strURL_TotalActvity = "mmdc_index.aspx?sort=15"
                Case Request.QueryString("sort").ToString = "17"
                    strSortOrder = "Need DESC"
                    strURL_Need = "mmdc_index.aspx?sort=18"
                Case Request.QueryString("sort").ToString = "18"
                    strSortOrder = "Need"
                    strURL_Need = "mmdc_index.aspx?sort=17"
            End Select
        End If

        tr = New TableRow

        Me.tMMDCIndex.Rows.Add(tr)
        td = New TableCell
        tr.Cells.Add(td)
        td.CssClass = "tdHead"
        lnk = New HyperLink
        td.Controls.Add(lnk)
        lnk.ID = "lnkYear"
        lnk.Text = "Year"
        lnk.NavigateUrl = strURL_Year

        td = New TableCell
        tr.Cells.Add(td)
        td.CssClass = "tdHead"
        lnk = New HyperLink
        td.Controls.Add(lnk)
        lnk.ID = "lnkModel"
        lnk.Text = "Model"
        lnk.NavigateUrl = strURL_Model

        td = New TableCell
        tr.Cells.Add(td)
        td.CssClass = "tdHead"
        lnk = New HyperLink
        td.Controls.Add(lnk)
        lnk.ID = "lnkAvgCost"
        lnk.Text = "Avg. Cost"
        lnk.NavigateUrl = strURL_AvgCost

        td = New TableCell
        tr.Cells.Add(td)
        td.CssClass = "tdHead"
        lnk = New HyperLink
        td.Controls.Add(lnk)
        lnk.ID = "lnkAvgIncome"
        lnk.Text = "Avg. Income"
        lnk.NavigateUrl = strURL_AvgIncome

        td = New TableCell
        tr.Cells.Add(td)
        td.CssClass = "tdHead"
        lnk = New HyperLink
        td.Controls.Add(lnk)
        lnk.ID = "lnkAvgProfit"
        lnk.Text = "Avg. Profit"
        lnk.NavigateUrl = strURL_AvgProfit

        td = New TableCell
        tr.Cells.Add(td)
        td.CssClass = "tdHead"
        lnk = New HyperLink
        td.Controls.Add(lnk)
        lnk.ID = "lnkAvgROI"
        lnk.Text = "Avg. ROI"
        td.HorizontalAlign = HorizontalAlign.Center
        lnk.NavigateUrl = strURL_AvgROI

        td = New TableCell
        tr.Cells.Add(td)
        td.CssClass = "tdHead"
        lnk = New HyperLink
        td.Controls.Add(lnk)
        td.HorizontalAlign = HorizontalAlign.Center
        lnk.ID = "lnkBED"
        lnk.Text = "BED"
        lnk.NavigateUrl = strURL_BED

        td = New TableCell
        tr.Cells.Add(td)
        td.CssClass = "tdHead"
        lnk = New HyperLink
        td.Controls.Add(lnk)
        td.HorizontalAlign = HorizontalAlign.Center
        lnk.ID = "lnkTotalActivity"
        lnk.Text = "90 Day<br />Total Activity"
        lnk.NavigateUrl = strURL_TotalActvity

        td = New TableCell
        tr.Cells.Add(td)
        td.CssClass = "tdHead"
        lnk = New HyperLink
        td.Controls.Add(lnk)
        lnk.ID = "lnkNeed"
        lnk.Text = "Need"
        lnk.NavigateUrl = strURL_Need

        Dim strColor As String = ""

        Dim lnkPrint As HyperLink

        Using objMMDCDGroups As New cardb.Database.MMDCDGroups("Model Is Not Null", strSortOrder)
            For Each objMMDCDGroup As cardb.Database.MMDCDGroup In objMMDCDGroups
                tr = New TableRow
                If strColor = "" Then
                    tr.BackColor = Drawing.Color.LightGray
                    strColor = "lightGrey"
                Else
                    tr.BackColor = Drawing.Color.White
                    strColor = ""
                End If

                tr.Attributes("onmouseout") = "this.className='trdataOut'"
                tr.Attributes("onmouseover") = "this.className='trdataOver'"
                tr.Attributes("onclick") = "window.navigate('mmdc_detail.aspx?groupkey=" & objMMDCDGroup.MMDCDGroupKey & "')"

                Me.tMMDCIndex.Rows.Add(tr)
                td = New TableCell
                tr.Cells.Add(td)
                td.Text = objMMDCDGroup.YearRange

                td = New TableCell
                tr.Cells.Add(td)
                td.Text = objMMDCDGroup.Model

                td = New TableCell
                tr.Cells.Add(td)
                td.HorizontalAlign = HorizontalAlign.Right
                td.Text = FormatCurrency(objMMDCDGroup.AvgCost)

                td = New TableCell
                tr.Cells.Add(td)
                td.HorizontalAlign = HorizontalAlign.Right
                td.Text = FormatCurrency(objMMDCDGroup.AvgIncome)

                td = New TableCell
                tr.Cells.Add(td)
                td.HorizontalAlign = HorizontalAlign.Right
                td.Text = FormatCurrency(objMMDCDGroup.AvgIncome - objMMDCDGroup.AvgCost)

                td = New TableCell
                tr.Cells.Add(td)
                td.HorizontalAlign = HorizontalAlign.Right
                If objMMDCDGroup.AvgCost > 0 Then
                    td.Text = FormatNumber(objMMDCDGroup.AvgIncome / objMMDCDGroup.AvgCost, 2)
                Else
                    td.Text = "0.00"
                End If


                td = New TableCell
                tr.Cells.Add(td)
                td.HorizontalAlign = HorizontalAlign.Center
                td.Text = objMMDCDGroup.BED

                td = New TableCell
                tr.Cells.Add(td)
                td.HorizontalAlign = HorizontalAlign.Right
                td.Text = FormatNumber(objMMDCDGroup.TotalActivity, 2)

                td = New TableCell
                tr.Cells.Add(td)
                td.HorizontalAlign = HorizontalAlign.Right
                td.Text = FormatNumber(objMMDCDGroup.Need, 2)
            Next
        End Using
    End Sub
End Class
