﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="mmdc_detail.aspx.vb" Inherits="reports_mmdc_detail" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>MMDC Detail</title>
    <link href="../style.css" rel="stylesheet" type="text/css" />
    <link href="../table_style.css" rel="stylesheet" type="text/css" />
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <asp:LinkButton ID="lnkPrint" runat="server">Print</asp:LinkButton><br /><br />
        <asp:Table ID="tHeader" runat="server">
            <asp:TableRow>
                <asp:TableCell CssClass="tdhead">Year</asp:TableCell>
                <asp:TableCell CssClass="tdhead">:</asp:TableCell>
                <asp:TableCell><asp:Label ID="lblYear" runat="server" Text=""></asp:Label></asp:TableCell>
            </asp:TableRow>
            <asp:TableRow>
                <asp:TableCell CssClass="tdhead">Model</asp:TableCell>
                <asp:TableCell CssClass="tdhead">:</asp:TableCell>
                <asp:TableCell><asp:Label ID="lblModel" runat="server" Text=""></asp:Label></asp:TableCell>
            </asp:TableRow>
            <asp:TableRow>
                <asp:TableCell CssClass="tdhead">Average Cost</asp:TableCell>
                <asp:TableCell CssClass="tdhead">:</asp:TableCell>
                <asp:TableCell><asp:Label ID="lblAvgCost" runat="server" Text=""></asp:Label></asp:TableCell>
            </asp:TableRow>
            <asp:TableRow>
                <asp:TableCell CssClass="tdhead">Average Income</asp:TableCell>
                <asp:TableCell CssClass="tdhead">:</asp:TableCell>
                <asp:TableCell><asp:Label ID="lblAvgIncome" runat="server" Text=""></asp:Label></asp:TableCell>
            </asp:TableRow>
            <asp:TableRow>
                <asp:TableCell CssClass="tdhead">Average Profit</asp:TableCell>
                <asp:TableCell CssClass="tdhead">:</asp:TableCell>
                <asp:TableCell><asp:Label ID="lblAvgProfit" runat="server" Text=""></asp:Label></asp:TableCell>
            </asp:TableRow>
            <asp:TableRow>
                <asp:TableCell CssClass="tdhead">Avg. ROI</asp:TableCell>
                <asp:TableCell CssClass="tdhead">:</asp:TableCell>
                <asp:TableCell><asp:Label ID="lblAvgROI" runat="server" Text=""></asp:Label></asp:TableCell>
            </asp:TableRow>
            <asp:TableRow>
                <asp:TableCell CssClass="tdhead">Break Even Days</asp:TableCell>
                <asp:TableCell CssClass="tdhead">:</asp:TableCell>
                <asp:TableCell><asp:Label ID="lblBED" runat="server" Text=""></asp:Label></asp:TableCell>
            </asp:TableRow>
            <asp:TableRow>
                <asp:TableCell CssClass="tdhead">Total Activity</asp:TableCell>
                <asp:TableCell CssClass="tdhead">:</asp:TableCell>
                <asp:TableCell><asp:Label ID="lblTotalActivity" runat="server" Text=""></asp:Label></asp:TableCell>
            </asp:TableRow>
            <asp:TableRow>
                <asp:TableCell CssClass="tdhead">Need</asp:TableCell>
                <asp:TableCell CssClass="tdhead">:</asp:TableCell>
                <asp:TableCell><asp:Label ID="lblNeed" runat="server" Text=""></asp:Label></asp:TableCell>
            </asp:TableRow>
        </asp:Table>
        <asp:Table ID="tMMDCDetails" runat="server" CellPadding="2" CellSpacing="0" Border="1">
        </asp:Table><br />
        <asp:Label ID="lblMessage" runat="server" Text=""></asp:Label>
    </div>
    </form>
</body>
</html>
