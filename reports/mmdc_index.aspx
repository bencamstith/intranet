﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="mmdc_index.aspx.vb" Inherits="reports_mmdc_index" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>MMDC Index</title>
    <link href="../style.css" rel="stylesheet" type="text/css" />
    <link href="mmdc_index.css" rel="stylesheet" type="text/css" />
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <asp:Table ID="tMMDCIndex" runat="server" CellPadding="2" CellSpacing="0" Border="1">
        </asp:Table>
    </div>
    </form>
</body>
</html>
