﻿
Partial Class reports_dailyreturns
    Inherits System.Web.UI.Page

   Private pstrDate As String = ""

   Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
      If Not IsPostBack Then
         Me.lblMessage.Text = ""
         BindDailyReturns()
      End If
   End Sub

   Private Sub BindDailyReturns()
      If Request.QueryString("date") Is Nothing Then
         pstrDate = cardb.BL.NonWorkingDay.PreviousWorkingDay(Date.Now).ToString("MM/dd/yyyy")
      Else
         pstrDate = CType(Request.QueryString("date"), Date).ToString("MM/dd/yyyy")
      End If

      Dim tr As TableRow
      Dim td As TableCell

      tr = New TableRow
      tDailyReturns.Rows.Add(tr)

      td = New TableCell
      tr.Cells.Add(td)
      td.CssClass = "tdhead"
      td.Width = "65"
      td.Text = "Trans #"

      td = New TableCell
      tr.Cells.Add(td)
      td.CssClass = "tdhead"
      td.HorizontalAlign = HorizontalAlign.Center
      td.Width = "50"
      td.Text = "QOH"

      td = New TableCell
      tr.Cells.Add(td)
      td.CssClass = "tdhead"
      td.Width = "80"
      td.Text = "Location"

      td = New TableCell
      tr.Cells.Add(td)
      td.CssClass = "tdhead"
      td.Width = "100"
      td.Text = "Inventory #"

      td = New TableCell
      tr.Cells.Add(td)
      td.CssClass = "tdhead"
      td.Width = "50"
      td.Text = "Stk #"

      td = New TableCell
      tr.Cells.Add(td)
      td.CssClass = "tdhead"
      td.Width = "125"
      td.Text = "Year/Make"

      td = New TableCell
      tr.Cells.Add(td)
      td.CssClass = "tdhead"
      td.Width = "150"
      td.Text = "Condition/Options"

      td = New TableCell
      tr.Cells.Add(td)
      td.CssClass = "tdhead"
      td.Width = "50"
      td.Text = "CC"

      td = New TableCell
      tr.Cells.Add(td)
      td.CssClass = "tdhead"
      td.Text = "SLS"

      td = New TableCell
      tr.Cells.Add(td)
      td.CssClass = "tdhead"
      td.Text = "USR"

      tr = New TableRow
      tDailyReturns.Rows.Add(tr)

      td = New TableCell
      tr.Cells.Add(td)
      td.ColumnSpan = 10
      td.Text = "<div class=""spacer""></div>"
      Dim bolToggle As Boolean = False

      Using objReport_ReturnedParts As New carpldb.Database.Report_ReturnedParts("(DateCreated = '" & pstrDate & "') " & StoreNumber(), "LocationCode")
         For Each objReport_ReturnedPart As carpldb.Database.Report_ReturnedPart In objReport_ReturnedParts
            tr = New TableRow
            If bolToggle Then
               'tr.Attributes.Add("backcolor", "#E8ECFF")
               tr.BackColor = Drawing.Color.LightGray
               bolToggle = False
            Else
               tr.BackColor = Drawing.Color.White
               bolToggle = True
            End If
            tDailyReturns.Rows.Add(tr)
            With objReport_ReturnedPart
               td = New TableCell
               tr.Cells.Add(td)
               td.Text = .TransactionNumber

               td = New TableCell
               tr.Cells.Add(td)
               td.HorizontalAlign = HorizontalAlign.Center
               td.Text = .QuantityAvailable

               td = New TableCell
               tr.Cells.Add(td)
               td.Text = .LocationCode

               td = New TableCell
               tr.Cells.Add(td)
               td.Text = .InventoryNumber

               td = New TableCell
               tr.Cells.Add(td)
               td.Text = .StockTicketNumber

               td = New TableCell
               tr.Cells.Add(td)
               td.Text = .ModelYear & "/" & .ModelName

               td = New TableCell
               tr.Cells.Add(td)
               td.Text = .ConditionsAndOptions

               td = New TableCell
               tr.Cells.Add(td)
               td.Text = .ConditionCode

               td = New TableCell
               tr.Cells.Add(td)
               td.Text = .CreatedBy_EmployeeInitials

               td = New TableCell
               tr.Cells.Add(td)
               td.Text = .CurrentUserId_EmployeeInitials
            End With
         Next
         For Each objErr As DevNet.Tools.Common.ValidationError In objReport_ReturnedParts.ValidationErrors
            If Me.lblMessage.Text.Trim.Length > 0 Then Me.lblMessage.Text &= ""
            Me.lblMessage.Text &= objErr.Message
         Next
      End Using
   End Sub

   Private Function StoreNumber() As String
      Me.lblDate.Text = pstrDate
      If Request.QueryString("store") Is Nothing Then
         Me.lblHeader.Text = "Daily Returns"
         Return ""
      Else
         If Request.QueryString("store").ToString = "0" Then
            Me.lblHeader.Text = "Daily Returns"
            Return ""
         Else
            Select Case Request.QueryString("store").ToString
               Case Is = "1"
                  Me.lblHeader.Text = "Daily Returns Store 1"
               Case Is = "3"
                  Me.lblHeader.Text = "Daily Returns Store 3"
            End Select
            Return "AND (CategorizingStoreNumber = " & Request.QueryString("store").ToString & ")"
         End If
      End If
   End Function
End Class
