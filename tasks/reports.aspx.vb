﻿
Partial Class tasks_reports
    Inherits System.Web.UI.Page

   Private pobjSuperAccess As Boolean = False

   Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
      pobjSuperAccess = CType(Session("SuperAccess"), Boolean)

      BindAccess()

      If Not IsPostBack Then
         Me.txtDate_Store1.Text = cardb.BL.NonWorkingDay.PreviousWorkingDay(Date.Now).ToString("MM/dd/yyyy")
         Me.txtPrevious_Store1.Text = cardb.BL.NonWorkingDay.PreviousWorkingDay(CType(Me.txtDate_Store1.Text, Date)).ToString("MM/dd/yyyy")
         Me.txtNext_Store1.Text = cardb.BL.NonWorkingDay.NextWorkingDay(CType(Me.txtDate_Store1.Text, Date)).ToString("MM/dd/yyyy")
         SetAttributes_Store1()

         Me.txtDate_Store3.Text = cardb.BL.NonWorkingDay.PreviousWorkingDay(Date.Now).ToString("MM/dd/yyyy")
         Me.txtPrevious_Store3.Text = cardb.BL.NonWorkingDay.PreviousWorkingDay(CType(Me.txtDate_Store3.Text, Date)).ToString("MM/dd/yyyy")
         Me.txtNext_Store3.Text = cardb.BL.NonWorkingDay.NextWorkingDay(CType(Me.txtDate_Store3.Text, Date)).ToString("MM/dd/yyyy")
         SetAttributes_Store3()
      End If
   End Sub

   Private Sub BindAccess()
      If pobjSuperAccess Then
         Me.pnlAccounting.Visible = True
         Me.pnlAccounting_Links.Visible = True
      Else
         Me.pnlAccounting.Visible = False
         Me.pnlAccounting_Links.Visible = False
      End If
   End Sub

   Private Sub SetAttributes_Store1()
      Dim datDate As Date = CType(Me.txtDate_Store1.Text, Date)
      lnkPrevious_Store1.Attributes("onclick") = "parent.myHome.location.href='/reports/dailyreturns.aspx?store=1&date=" & cardb.BL.NonWorkingDay.PreviousWorkingDay(datDate) & "'"
      Me.txtPrevious_Store1.Text = cardb.BL.NonWorkingDay.PreviousWorkingDay(datDate).ToString
      lnkNext_Store1.Attributes("onclick") = "parent.myHome.location.href='/reports/dailyreturns.aspx?store=1&date=" & cardb.BL.NonWorkingDay.NextWorkingDay(datDate) & "'"
      Me.txtNext_Store1.Text = cardb.BL.NonWorkingDay.NextWorkingDay(datDate).ToString
   End Sub

   Protected Sub lnkNext_Store1_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkNext_Store1.Click
      If IsDate(Me.txtDate_Store1.Text) Then
         Me.txtDate_Store1.Text = CType(Me.txtNext_Store1.Text, Date).ToString("MM/dd/yyyy")
      Else
         Me.txtDate_Store1.Text = Date.Now.ToString("MM/dd/yyyy")
      End If
      SetAttributes_Store1()
   End Sub

   Protected Sub lnkPrevious_Store1_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkPrevious_Store1.Click
      If IsDate(Me.txtDate_Store1.Text) Then
         Me.txtDate_Store1.Text = CType(Me.txtPrevious_Store1.Text, Date).ToString("MM/dd/yyyy")
      Else
         Me.txtDate_Store1.Text = Date.Now.ToString("MM/dd/yyyy")
      End If
      SetAttributes_Store1()
   End Sub

   Private Sub SetAttributes_Store3()
      Dim datDate As Date = CType(Me.txtDate_Store3.Text, Date)
      lnkPrevious_Store3.Attributes("onclick") = "parent.myHome.location.href='/reports/dailyreturns.aspx?store=3&date=" & cardb.BL.NonWorkingDay.PreviousWorkingDay(datDate) & "'"
      Me.txtPrevious_Store3.Text = cardb.BL.NonWorkingDay.PreviousWorkingDay(datDate).ToString
      lnkNext_Store3.Attributes("onclick") = "parent.myHome.location.href='/reports/dailyreturns.aspx?store=3&date=" & cardb.BL.NonWorkingDay.PreviousWorkingDay(datDate) & "'"
      Me.txtNext_Store3.Text = cardb.BL.NonWorkingDay.NextWorkingDay(datDate).ToString
   End Sub

   Protected Sub lnkNext_Store3_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkNext_Store3.Click
      If IsDate(Me.txtDate_Store3.Text) Then
         Me.txtDate_Store3.Text = CType(Me.txtNext_Store3.Text, Date).ToString("MM/dd/yyyy")
      Else
         Me.txtDate_Store3.Text = Date.Now.ToString("MM/dd/yyyy")
      End If
      SetAttributes_Store3()
   End Sub

   Protected Sub lnkPrevious_Store3_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkPrevious_Store3.Click
      If IsDate(Me.txtDate_Store3.Text) Then
         Me.txtDate_Store3.Text = CType(Me.txtPrevious_Store3.Text, Date).ToString("MM/dd/yyyy")
      Else
         Me.txtDate_Store3.Text = Date.Now.ToString("MM/dd/yyyy")
      End If
      SetAttributes_Store3()
   End Sub
End Class
