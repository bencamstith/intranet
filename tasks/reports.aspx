<%@ Page Language="VB" AutoEventWireup="false" CodeFile="reports.aspx.vb" Inherits="tasks_reports" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link href="../style.css" rel="stylesheet" type="text/css" />
</head>
<body>
    <form id="form1" runat="server">
<!--    <div class="left_heads">&nbsp;&nbsp;Sales Reports</div><div class="lefts"><a href="/home/accounting/poprofit.aspx">P.O. Profitability</a><br /><a href="/car/junk_report.aspx">Junk Report Store 1</a><br /><a href="/car/junk_report3.aspx">Junk Report Store 3</a><br /><a href="/car/bust_report.aspx" target="_blank">Bust Report Store 1</a><br /><a href="/car/bust_report.aspx?store=3" target="_blank">Bust Report Store 3</a><br /><a href="/car/pull_report.aspx" target="_blank">Pull Report Store 1</a><br /><a href="/car/pull_report.aspx?store=3" target="_blank">Pull Report Store 3</a><br /></div><br /><br /> -->
    <asp:Panel ID="pnlAccounting" CssClass="left_heads" runat="server">&nbsp;&nbsp;Accounting</asp:Panel>
    <asp:Panel ID="pnlAccounting_Links" CssClass="lefts" runat="server">
        <a href="/reports/poprofit.aspx" target="_new">P.O. Profit Report</a><br /><br />
        <a href="#" onclick="parent.myHome.src='/reports/mmdc_index.aspx'">MMDC Report</a>
        <br /><br />
    </asp:Panel>
    <asp:Panel ID="pnlInventory" CssClass="left_heads" runat="server">&nbsp;&nbsp;Inventory Reports</asp:Panel>
    <asp:Panel ID="pnlInventory_Links" CssClass="lefts" runat="server">
        <a href="#" onclick="parent.myHome.src='/reports/dailyreturns.aspx?store=1'">Daily Returns Store 1</a>
        <div style="text-align: center"><asp:LinkButton ID="lnkPrevious_Store1" runat="server">Previous</asp:LinkButton>&nbsp;&nbsp;<asp:LinkButton ID="lnkNext_Store1" runat="server">Next</asp:LinkButton></div>
        <br />
        <a href="#" onclick="parent.myHome.src='/reports/dailyreturns.aspx?store=3'">Daily Returns Store 3</a>
        <div style="text-align: center"><asp:LinkButton ID="lnkPrevious_Store3" runat="server">Previous</asp:LinkButton>&nbsp;&nbsp;<asp:LinkButton ID="lnkNext_Store3" runat="server">Next</asp:LinkButton></div>
        <asp:TextBox ID="txtPrevious_Store1" Visible="false" runat="server"></asp:TextBox><br />
        <asp:TextBox ID="txtNext_Store1" Visible="false"  runat="server"></asp:TextBox><br />
        <asp:TextBox ID="txtDate_Store1" Visible="false" runat="server"></asp:TextBox><br />
        <asp:TextBox ID="txtPrevious_Store3" Visible="false" runat="server"></asp:TextBox><br />
        <asp:TextBox ID="txtNext_Store3" Visible="false"  runat="server"></asp:TextBox><br />
        <asp:TextBox ID="txtDate_Store3" Visible="false" runat="server"></asp:TextBox>
        <br /><br />
    </asp:Panel>
    </form>
</body>
</html>
