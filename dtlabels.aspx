﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="dtlabels.aspx.vb" Inherits="dtlabels" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Delivery Ticket Labels</title>
    <link href="../style.css" rel="stylesheet" type="text/css" />
    <link href="../table_style.css" rel="stylesheet" type="text/css" />
</head>
<body>
    <form id="form1" runat="server" DefaultButton="btnSearch" >
    <div style="padding-left: 20px; padding-top: 20px;">
        <h1>Re-Print Delivery Ticket Label</h1>
        Delivery Ticket #:<asp:TextBox ID="txtDeliveryTicketNumber" runat="server"></asp:TextBox>
        <asp:Button ID="btnSearch" runat="server" Text="Search" Width="80" /><br /><br />
        <asp:Label ID="lblMessage" runat="server" Text=""></asp:Label>
        <table cellpadding="2" cellspacing="0" border="0" style="border-top-width: thin; border-top-style: solid; border-top-color: #ABADB3; width: 600px; ">
            <tr>
                <td style="border-left: thin solid #E3E9EF; border-bottom: thin solid #E3E9EF;width: 50px;">Revision</td>
                <td style="border-bottom: thin solid #E3E9EF;width: 35px;text-align: center;">Qty</td>
                <td style="border-bottom: thin solid #E3E9EF;width: 90px;">Inventory #</td>
                <td style="border-bottom-style: solid; border-bottom-width: thin; border-bottom-color: #E3E9EF; width: 190px;">Description</td>
                <td style="border-bottom-style: solid; border-bottom-width: thin; border-bottom-color: #E3E9EF; width: 60px;text-align: center;">Store</td>
                <td style="border-bottom: thin solid #E3E9EF; width: 75px;">Status</td>
                <td style="border-right-color: #ABADB3; border-right-width: thin; border-right-style: solid; border-bottom-style: solid; 
                    border-bottom-width: thin; border-bottom-color: #E3E9EF; width: 100px;">&nbsp;</td>
            </tr>
            <tr>
                <td style="border-right-color: #ABADB3; border-left-color: #E3E9EF; border-left-width: thin; border-right-width: thin; border-right-style: solid; border-left-style: solid;" colspan="7">
                    <iframe runat="server" id="iframeDeliveryTicketLineItems" width="600" height="150" style="border-style: none; border-width: 0px; border-color: inherit; margin: 0px;" scrolling="auto" frameborder="0" src=""></iframe>
                </td>
            </tr>
            <tr>
                <td style="border-top-style: solid; border-top-width: thin; border-top-color: #E3E9EF; " colspan="7">&nbsp;</td>
            </tr>
        </table>
    </div>
    </form>
</body>
</html>