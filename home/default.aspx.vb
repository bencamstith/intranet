﻿
Partial Class home_default
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(sender As Object, e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            BindPayPeriods()
            BindEmployees()
        End If
        Me.lblPayPeriodCommission.Text = FormatCurrency(0, 2)
        Me.lblPayPeriodSales.Text = FormatCurrency(0, 2)
    End Sub

    Private Sub BindEmployees()
        Using objEmployees As New cardb.Database.Employees("EmployeeKey In (SELECT EmployeeKey FROM tblLedgerItems GROUP BY EmployeeKey)")
            For Each objEmployee As cardb.Database.Employee In objEmployees
                Me.ddlEmployeeKey.Items.Add(New ListItem(objEmployee.EmployeeName, objEmployee.EmployeeKey))
            Next
        End Using
    End Sub

    Private Sub BindCommissions()
        Dim tr As TableRow
        Dim td As TableCell
        Dim strToDate As String = Me.ddlPayPeriod.SelectedValue
        Dim strFromDate As String = CType(Me.ddlPayPeriod.SelectedValue, Date).AddDays(-6)

        Dim decTotalSales As Decimal = 0
        Dim decTotalCommission As Decimal = 0

        Dim strCommissionDetails As String = ""
        Dim strDistributionDetails As String = ""
        Dim strSharedDetails As String = ""

        Dim strPaymentDetails As String = ""

        Dim objFile As System.IO.StreamReader

        With New System.IO.FileInfo(Server.MapPath("./templates/commissiondetails.txt"))
            If .Exists Then
                objFile = New System.IO.StreamReader(.FullName)
                strCommissionDetails = objFile.ReadToEnd
            End If
        End With

        With New System.IO.FileInfo(Server.MapPath("./templates/distributiondetails.txt"))
            If .Exists Then
                objFile = New System.IO.StreamReader(.FullName)
                strDistributionDetails = objFile.ReadToEnd
            End If
        End With

        With New System.IO.FileInfo(Server.MapPath("./templates/shareddetails.txt"))
            If .Exists Then
                objFile = New System.IO.StreamReader(.FullName)
                strSharedDetails = objFile.ReadToEnd
            End If
        End With

        Dim objLit As Literal
        'vwPL_InvoiceLineItems.InvoiceID, PaymentNumber", "INNER JOIN tblLedgerItemTypes ON tblLedgerItems.LedgerItemTypeKey = tblLedgerItemTypes.LedgerItemTypeKey INNER JOIN vwPL_InvoiceLineItems ON tblLedgerItems.InvoiceLineItemId = vwPL_InvoiceLineItems.LineItemID"
        Using objLedgerItems As New cardb.Database.LedgerItems("EmployeeKey = " & Me.ddlEmployeeKey.SelectedValue & " AND (LedgerDate BETWEEN CONVERT(DATETIME, '" & strFromDate & "', 102) AND CONVERT(DATETIME, '" & strToDate & "', 102))", "LedgerDate")
            For Each objLedgerItem As cardb.Database.LedgerItem In objLedgerItems
                tr = Me.tCommissions.Rows(Me.tCommissions.Rows.Add(New TableRow))

                td = tr.Cells(tr.Cells.Add(New TableCell))
                td.Text = objLedgerItem.LedgerDate

                td = tr.Cells(tr.Cells.Add(New TableCell))
                td.Text = objLedgerItem.LedgerItemTypeKey_Object.LedgerItemType

                td = tr.Cells(tr.Cells.Add(New TableCell))
                objLit = New Literal
                objLit.Text = "<a href=""#"">" & objLedgerItem.BL.InvoiceLineItem.BL.Invoice.InvoiceNumber & strCommissionDetails & "</a>"
                objLit.Text = objLit.Text.Replace("[DESCRIPTION]", objLedgerItem.LedgerItemTypeKey_Object.LedgerItemType.Replace(" ", "&nbsp;"))
                objLit.Text = objLit.Text.Replace("[SALE]", FormatCurrency(objLedgerItem.CollectedAmount, 2))
                objLit.Text = objLit.Text.Replace("[COST]", FormatCurrency(objLedgerItem.Cost, 2))
                objLit.Text = objLit.Text.Replace("[PROFIT]", FormatCurrency(objLedgerItem.CollectedAmount - objLedgerItem.Cost, 2))
                objLit.Text = objLit.Text.Replace("[PERCENT]", Math.Round(objLedgerItem.Commission * 100, 2).ToString & "%")
                objLit.Text = objLit.Text.Replace("[COMMISSION]", FormatCurrency(objLedgerItem.CommissionAmount, 2))
                If objLedgerItem.LedgerItemKey_Shared > 0 Then
                    objLit.Text = objLit.Text.Replace("[SHARED]", strSharedDetails)
                    objLit.Text = objLit.Text.Replace("[SHAREDWITH]", objLedgerItem.LedgerItemKey_Shared_Object.EmployeeKey_Object.EmployeeName)
                    objLit.Text = objLit.Text.Replace("[SHAREDAMOUNT]", FormatCurrency(objLedgerItem.CommissionAmount, 2))
                    objLit.Text = objLit.Text.Replace("[NETCOMMISSION]", FormatCurrency(objLedgerItem.CommissionAmount, 2))
                Else
                    objLit.Text = objLit.Text.Replace("[SHARED]", "")
                End If
                objLit.Text = objLit.Text.Replace("[PAYMENTS]", objLedgerItem.Payments)

                strPaymentDetails = ""
                If objLedgerItem.Payments = 1 Then
                    strPaymentDetails = strDistributionDetails
                    strPaymentDetails = strPaymentDetails.Replace("[PAYMENTNUM]", "1")
                    strPaymentDetails = strPaymentDetails.Replace("[PAYMENTDATE]", objLedgerItem.LedgerDate)
                    strPaymentDetails = strPaymentDetails.Replace("[PAYMENTAMOUNT]", FormatCurrency(objLedgerItem.DistributionAmount, 2))
                Else
                    Using objPayments As New cardb.Database.LedgerItems(" InvoiceLineItemId = " & objLedgerItem.InvoiceLineItemId, "PaymentNumber")
                        For Each objPayment As cardb.Database.LedgerItem In objPayments
                            strPaymentDetails &= strDistributionDetails
                            strPaymentDetails = strPaymentDetails.Replace("[PAYMENTNUM]", objPayment.PaymentNumber)
                            strPaymentDetails = strPaymentDetails.Replace("[PAYMENTDATE]", objPayment.LedgerDate)
                            strPaymentDetails = strPaymentDetails.Replace("[PAYMENTAMOUNT]", FormatCurrency(objPayment.DistributionAmount, 2))
                        Next
                    End Using
                End If
                objLit.Text = objLit.Text.Replace("[PAYMENTDETAIL]", strPaymentDetails)

                td.Controls.Add(objLit)
                objLit = Nothing

                td = tr.Cells(tr.Cells.Add(New TableCell))
                td.Text = objLedgerItem.InventoryNumber & "<br />" & objLedgerItem.Description

                td = tr.Cells(tr.Cells.Add(New TableCell))
                td.HorizontalAlign = HorizontalAlign.Right
                If objLedgerItem.CollectedAmount < 0 Then
                    td.ForeColor = Drawing.Color.Red
                Else
                    td.ForeColor = Drawing.Color.Black
                End If
                td.Text = FormatCurrency(objLedgerItem.CollectedAmount, 2)
                decTotalSales += objLedgerItem.CollectedAmount

                td = tr.Cells(tr.Cells.Add(New TableCell))
                td.HorizontalAlign = HorizontalAlign.Right
                If objLedgerItem.CollectedAmount - objLedgerItem.Cost < 0 Then
                    td.ForeColor = Drawing.Color.Red
                Else
                    td.ForeColor = Drawing.Color.Black
                End If
                If objLedgerItem.PONumber > 0 Then
                    td.Text = FormatCurrency(objLedgerItem.CollectedAmount - objLedgerItem.Cost, 2)
                Else
                    td.Text = "-"
                End If

                td = tr.Cells(tr.Cells.Add(New TableCell))
                td.HorizontalAlign = HorizontalAlign.Right
                If objLedgerItem.CommissionAmount < 0 Then
                    td.ForeColor = Drawing.Color.Red
                Else
                    td.ForeColor = Drawing.Color.Black
                End If
                td.Text = FormatCurrency(objLedgerItem.DistributionAmount, 2)

                td = tr.Cells(tr.Cells.Add(New TableCell))
                td.HorizontalAlign = HorizontalAlign.Right
                td.Text = objLedgerItem.PaymentNumber & " of " & objLedgerItem.Payments
                decTotalCommission += objLedgerItem.CommissionAmount
            Next

            Me.lbl52WeekSales.Text = ""
            For Each objErr As DevNet.Tools.Common.ValidationError In objLedgerItems.ValidationErrors
                Me.lbl52WeekSales.Text &= objErr.Message
            Next
        End Using

        If decTotalCommission < 0 Then
            Me.lblPayPeriodCommission.ForeColor = Drawing.Color.Red
        Else
            Me.lblPayPeriodCommission.ForeColor = Drawing.Color.Black
        End If
        If decTotalSales < 0 Then
            Me.lblPayPeriodSales.ForeColor = Drawing.Color.Red
        Else
            Me.lblPayPeriodSales.ForeColor = Drawing.Color.Black
        End If
        Me.lblPayPeriodCommission.Text = FormatCurrency(decTotalCommission, 2)
        Me.lblPayPeriodSales.Text = FormatCurrency(decTotalSales, 2)
    End Sub

    Private Sub BindPayPeriods()
        For x As Integer = 4 To 1 Step -1
            Me.ddlPayPeriod.Items.Add(New ListItem(Date.Now.AddDays(x * 7).ToString("MM/dd/yyyy")))
        Next
        For x As Integer = 0 To 52
            Me.ddlPayPeriod.Items.Add(New ListItem(Date.Now.AddDays(x * -7).ToString("MM/dd/yyyy")))
        Next
        Me.ddlPayPeriod.SelectedValue = Date.Now.ToString("MM/dd/yyyy")
    End Sub

    Protected Sub ddlPayPeriod_SelectedIndexChanged(sender As Object, e As System.EventArgs) Handles ddlPayPeriod.SelectedIndexChanged
        BindCommissions()
    End Sub
End Class
