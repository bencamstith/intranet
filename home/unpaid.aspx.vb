﻿
Partial Class home_unpaid
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(sender As Object, e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            BindEmployees()
        End If
    End Sub

    Private Sub BindEmployees()
        Using objEmployees As New cardb.Database.Employees("EmployeeKey In (SELECT EmployeeKey FROM tblLedgerItems GROUP BY EmployeeKey)")
            For Each objEmployee As cardb.Database.Employee In objEmployees
                Me.ddlEmployeeKey.Items.Add(New ListItem(objEmployee.EmployeeName, objEmployee.EmployeeKey))
            Next
        End Using
    End Sub

    Private Sub BindInvoices()
        Dim tr As TableRow
        Dim td As TableCell

        Dim decTotalSales As Decimal = 0
        Dim decTotalCommission As Decimal = 0

        Using objInvoices As New carpldb.Database.Invoices("(dbo.fnPaymentDueDate(DateCreated) <= GETDATE()) AND (NotPaid = 1) AND (CreatedBy = " & Me.ddlEmployeeKey.SelectedValue & ")", "DateCreated")
            For Each objInvoice As carpldb.Database.Invoice In objInvoices
                tr = Me.tUnpaid.Rows(Me.tUnpaid.Rows.Add(New TableRow))
                td = tr.Cells(tr.Cells.Add(New TableCell))
                td.Text = objInvoice.InvoiceNumber

                td = tr.Cells(tr.Cells.Add(New TableCell))
                td.Text = CType(objInvoice.DateCreated, Date).ToString("MM/dd/yyyy")

                td = tr.Cells(tr.Cells.Add(New TableCell))
                td.Text = objInvoice.BillToBusinessName

                td = tr.Cells(tr.Cells.Add(New TableCell))
                td.Text = objInvoice.CustomerNumber

                td = tr.Cells(tr.Cells.Add(New TableCell))
                td.Text = FormatCurrency(objInvoice.InvoiceAmount, 2)

                td = tr.Cells(tr.Cells.Add(New TableCell))
                td.Text = FormatCurrency(0, 2)

                td = tr.Cells(tr.Cells.Add(New TableCell))
                td.Text = FormatCurrency(0, 2)
            Next

            For Each objErr As DevNet.Tools.Common.ValidationError In objInvoices.ValidationErrors
                Me.lbl52WeekSales.Text &= objErr.Message
            Next
        End Using
    End Sub

    Protected Sub ddlEmployeeKey_SelectedIndexChanged(sender As Object, e As System.EventArgs) Handles ddlEmployeeKey.SelectedIndexChanged
        BindInvoices()
    End Sub
End Class
