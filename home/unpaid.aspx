﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="unpaid.aspx.vb" Inherits="home_unpaid" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head id="Head1" runat="server">
    <title></title>
    <link href="styles/main.css" rel="stylesheet" type="text/css" />
    <link href="styles/slider.css" rel="stylesheet" type="text/css" />
    <link href="styles/user_search.css" rel="stylesheet" type="text/css" />
    <link href="styles/searchDemo.css" rel="stylesheet" type="text/css" />
    <link href="styles/stylish-select.css" rel="stylesheet" type="text/css" />
     <style type="text/css">
a {
	color: Black;
}

a:hover {
	position: relative;
}

a span {
	display: none;
}

a:hover span {
	display: block;
   	position: absolute; top: 10px; left: 0;
	/* formatting only styles */
   	padding: 5px; margin: 10px; z-index: 100;
   	background: #f0f0f0; border: 1px dotted #c0c0c0;
	opacity: 0.9;
}
</style>
</head>
<body>
    <form id="form1" runat="server">
    <div id="wrapper">

	<div id="header">
        <div id="loginLogoutArea">
            Welcome, Richard Coker &nbsp; | &nbsp; <a href="/user/logout">Logout</a>
            <!--<a class="button floatright" href="/user/login" rel="nofollow"><span>Login</span></a>-->
        </div>
        <div id="logo"><a href="/" title="CAR">Counselman Automotive Recycling</a></div>
        <div id="logoForPrint"><img src="/logo2.jpg" width="224" height="50" alt="logo" /></div>
	</div>
	<div id="main" class="clearfix">
		<div id="nav">
			<div id="mainMenu">
                <ul>
				    <li><a href="/">Commissions</a></li>
				    <li class="active"><a href="/search" class="active">Reports</a></li>
				    <li><a href="/">Vehicles to Junk</a></li>
				    <li><a href="/">Delivery Tickets</a></li>
				    <li><a href="/">Accounts</a></li>
				    <li><a href="/">Parts</a></li>
				    <li><a href="/">Maintenance</a></li>
				</ul>
			</div>
		</div>
		 
		<div id="content">
<style type="text/css">
	.question strong { font-weight:normal; }
</style>
<div id="fullwidth">
	<div class="content-header">&nbsp;</div>
	<div id="questions">
		<div class="question">
			<h1>Commission Details</h1><br />
            <table width="100%">
                <tr>
                    <td width="50%">
                        <table>
                            <tr>
                                <td>Employee</td>
                                <td>:</td>
                                <td><asp:DropDownList ID="ddlEmployeeKey" runat="server" AutoPostBack="true">
                                <asp:ListItem Value="0">[Select Employee]</asp:ListItem>
                                </asp:DropDownList></td>
                            </tr>
                            <tr>
                                <td>Pay Period</td>
                                <td>:</td>
                                <td><asp:DropDownList ID="ddlPayPeriod" runat="server" AutoPostBack="true"></asp:DropDownList></td>
                            </tr>
                            <tr>
                                <td>Pay Period Sales</td>
                                <td>:</td>
                                <td align="right"><asp:Label ID="lblPayPeriodSales" runat="server" Text=""></asp:Label></td>
                            </tr>
                            <tr>
                                <td>Pay Period Commission</td>
                                <td>:</td>
                                <td align="right"><asp:Label ID="lblPayPeriodCommission" runat="server" Text=""></asp:Label></td>
                            </tr>
                        </table>
                    </td>
                    <td width="50%" valign="top">
                        <table>
                            <tr>
                                <td>52 Week Sales</td>
                                <td>:</td>
                                <td align="right"><asp:Label ID="lbl52WeekSales" runat="server" Text=""></asp:Label></td>
                            </tr>
                            <tr>
                                <td>52 Week Commission</td>
                                <td>:</td>
                                <td align="right"><asp:Label ID="lbl52WeekCommission" runat="server" Text=""></asp:Label></td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
		</div>
	</div>
	<div class="content-footer">&nbsp;</div><br />
    <asp:Table ID="tUnpaid" CssClass="exampletable" runat="server" Width="899">
        <asp:TableHeaderRow>
            <asp:TableHeaderCell CssClass="border-left">Invoice #</asp:TableHeaderCell>
            <asp:TableHeaderCell>Date</asp:TableHeaderCell>
            <asp:TableHeaderCell>Customer</asp:TableHeaderCell>
            <asp:TableHeaderCell>Customer #</asp:TableHeaderCell>
            <asp:TableHeaderCell>Amount</asp:TableHeaderCell>
            <asp:TableHeaderCell>Paid To Date</asp:TableHeaderCell>
            <asp:TableHeaderCell HorizontalAlign="Right" CssClass="border-right">Balance</asp:TableHeaderCell>
        </asp:TableHeaderRow>
    </asp:Table>
    </div>
<div class="clearfix"></div>
		</div> <!-- content -->
	</div> <!-- main -->
</div> <!-- wrapper -->
	
<div id="footer">
	<ul>
		<li><a href="/about">About</a></li>
		<li><a href="/contact">Contact</a></li>
		<li><a href="/press">Press</a></li>
		<li><a href="/affiliates">Affiliates</a></li>
		<li><a href="/privacy">Privacy</a></li>
		<li><a href="/terms">Terms</a></li>
		<li><a href="/sitemap">Site Map</a></li>
		<li>Follow us on <a href="http://www.twitter.com/">Twitter</a> or <a href="http://www.facebook.com/">Facebook</a>.</li>
	</ul>
	&copy; 2009-2011 , LLC. All rights reserved.
</div>
    </form>
</body>
</html>
