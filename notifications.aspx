﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="notifications.aspx.vb" Inherits="notifications" %>

<%@ Register src="includes/header.ascx" tagname="header" tagprefix="uc1" %>
<%@ Register src="includes/footer.ascx" tagname="footer" tagprefix="uc2" %>
<%@ Register src="includes/tasks.ascx" tagname="tasks" tagprefix="uc3" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link href="style.css" rel="stylesheet" type="text/css" />
    <link href="table_style.css" rel="stylesheet" type="text/css" />
</head>
<body>
    <form id="form1" runat="server">
    <uc1:header ID="header1" runat="server" />
    <br />
    <div class="top"><a href="###"></a> > <a href="###"></a>&nbsp;&nbsp;&nbsp;</div>

    <div id="wrapper">
        <div class="left_col">
            <uc3:tasks ID="tasks1" runat="server" />
        </div>
        <div class="right_main">
    		<h1></h1>
			<br /><br />
			<div class="spacer">
			    <asp:Label ID="lblErrors" runat="server" Text=""></asp:Label>
                <asp:Table ID="tNotifications" runat="server" CellPadding="2" CellSpacing="0">
                    <asp:TableRow>
                        <asp:TableCell CssClass="tdhead">Alert</asp:TableCell>
                        <asp:TableCell CssClass="tdhead">Employee</asp:TableCell>
                        <asp:TableCell CssClass="tdhead" Width="250">Description</asp:TableCell>
                        <asp:TableCell CssClass="tdhead">Date</asp:TableCell>
                    </asp:TableRow>
                    <asp:TableRow>
                        <asp:TableCell>
                            <asp:DropDownList CssClass="smalltext" ID="ddlTriggerTypeKey" AutoPostBack="true" runat="server">
                                <asp:ListItem Value="0">[All]</asp:ListItem>
                            </asp:DropDownList>
                        </asp:TableCell>
                        <asp:TableCell>
                            <asp:DropDownList CssClass="smalltext" ID="ddlEmployeeKey" AutoPostBack="true" runat="server">
                                <asp:ListItem Value="0">[All]</asp:ListItem>
                            </asp:DropDownList>
                        </asp:TableCell>
                        <asp:TableCell>&nbsp;</asp:TableCell>
                        <asp:TableCell></asp:TableCell>
                    </asp:TableRow>
                </asp:Table>
            </div>
            <br /><br />
        </div>
    </div>
    <uc2:footer ID="footer1" runat="server" />
    </form>
</body>
</html>
