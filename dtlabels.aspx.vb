﻿
Partial Class dtlabels
    Inherits System.Web.UI.Page

   Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
      Response.Redirect("http://intranet.counselmanauto.com/deliverytickets")
      Me.txtDeliveryTicketNumber.Focus()
      Me.txtDeliveryTicketNumber.Attributes("onFocus") = "this.select()"
   End Sub

    Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSearch.Click
        lblMessage.Text = ""
        If Me.txtDeliveryTicketNumber.Text.Trim.Length = 0 Or IsNumeric(Me.txtDeliveryTicketNumber.Text) = False Then
            lblMessage.Text = "Enter a valid Delivery Ticket #."
        Else
            Dim strDeliveryTicketIds As String = ""
         Using objDeliveryTickets As New carpldb.Database.DeliveryTickets("IsLastRevision = 1 AND DeliveryTicketNumber = " & Me.txtDeliveryTicketNumber.Text)
            For Each objDeliveryTicket As carpldb.Database.DeliveryTicket In objDeliveryTickets
               If strDeliveryTicketIds.Length > 0 Then strDeliveryTicketIds &= ","
               strDeliveryTicketIds &= objDeliveryTickets(0).DeliveryTicketID.ToString
            Next
            For Each objErr As DevNet.Tools.Common.ValidationError In objDeliveryTickets.ValidationErrors
               lblMessage.Text = objErr.Message & "<br />"
            Next
         End Using

         'If strDeliveryTicketIds.Length > 0 Then
         Me.iframeDeliveryTicketLineItems.Attributes("src") = "dtlabels_lineitems.aspx?deliveryticketid=" & Server.UrlEncode(strDeliveryTicketIds)
         'End If
        End If
    End Sub

End Class
