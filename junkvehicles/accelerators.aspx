﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="accelerators.aspx.vb" Inherits="setup_accelerators" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Accelerators</title>
    <link href="../style.css" rel="stylesheet" type="text/css" />
    <link href="../table_style.css" rel="stylesheet" type="text/css" />
</head>
<body>
    <form id="form1" runat="server">
    		<h1>Vehicles to Junk Accelerators</h1>
            <asp:Label ID="lblErrors" runat="server" Text=""></asp:Label><br />
            Accelerator Type:
            <asp:DropDownList ID="ddlAcceleratorTypeKey" runat="server" AutoPostBack="True">
                <asp:ListItem Value="0" Selected="True">[Select a Type]</asp:ListItem>
                <asp:ListItem Value="1">Quantity On Hand</asp:ListItem>
                <asp:ListItem Value="2">Sales</asp:ListItem>
                <asp:ListItem Value="3">Activity</asp:ListItem>
                <asp:ListItem Value="4">Condition Code</asp:ListItem>
            </asp:DropDownList><br /><br />
            <asp:Table ID="tblAccelerators" runat="server" CellPadding="2" CellSpacing="0" 
                Width="467px">
                <asp:TableHeaderRow>
                    <asp:TableHeaderCell CssClass="tdhead">Value</asp:TableHeaderCell>
                    <asp:TableHeaderCell CssClass="tdhead">Low</asp:TableHeaderCell>
                    <asp:TableHeaderCell CssClass="tdhead">High</asp:TableHeaderCell>
                    <asp:TableHeaderCell CssClass="tdhead">&nbsp;</asp:TableHeaderCell>
                </asp:TableHeaderRow>
                <asp:TableRow>
                    <asp:TableCell ColumnSpan="4"><div class="spacer"></div></asp:TableCell>
                </asp:TableRow>
                <asp:TableFooterRow>
                    <asp:TableCell><asp:TextBox ID="txtAcceleratorKey" Visible="false" runat="server"></asp:TextBox><asp:TextBox ID="txtValue" Visible="false" runat="server"></asp:TextBox></asp:TableCell>
                    <asp:TableCell><asp:TextBox ID="txtLow" Visible="false" runat="server"></asp:TextBox></asp:TableCell>
                    <asp:TableCell><asp:TextBox ID="txtHigh" Visible="false" runat="server"></asp:TextBox></asp:TableCell>
                    <asp:TableCell><asp:Button ID="btnSubmit" Visible="false" runat="server" Text="Submit" /></asp:TableCell>
                </asp:TableFooterRow>
            </asp:Table>
    </form>
</body>
</html>
