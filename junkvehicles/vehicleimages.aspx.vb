﻿
Partial Class junkvehicles_vehicleimages
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(sender As Object, e As System.EventArgs) Handles Me.Load
        Dim td As TableCell

        If Not Request.QueryString("ARAVehicleId") Is Nothing Then
            If IsNumeric(Request.QueryString("ARAVehicleId")) Then
                Using objImages As New carpldb.Database.Images("(PLImageID IN (SELECT PLImageID FROM Powerlink.dbo.PL_IMAGE_VEHICLE WHERE (ARAVehicleID = " & Request.QueryString("ARAVehicleId") & ")))")
                    For Each objImage As carpldb.Database.Image In objImages
                        td = New TableCell
                        Me.tVehicleImages.Rows(0).Cells.Add(td)
                        td.Text &= "<a target=""_new"" border=""0"" href=""http://services.counselmanauto.com/images/plimages/vehicle/" & objImage.Filename & """><img src=""" & "http://services.counselmanauto.com/images/plimages/vehicle/" & objImage.Filename & """ border=""0"" height=""72"" width=""96"" /></a>"
                    Next
                End Using
            End If
        End If
    End Sub
End Class
