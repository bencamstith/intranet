﻿
Partial Class junk
    Inherits System.Web.UI.Page
    Private pintCategorizingStoreNumber As Integer = 1
    Private pintCount As Integer = 0
    Private pbolNavigating As Boolean = False
    Private pintCalled As Integer = 0

   Private Enum eSort
      ByWeight = 1
      ByAge = 2
      ByProfit = 3
      ByROI = 4
   End Enum

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Not IsPostBack Then
            tableVehicleHeader.Visible = False
         BindStockTicketNumbers(eSort.ByWeight)

            'If Not Request.QueryString("invid") Is Nothing Then
            '    If IsNumeric(Request.QueryString("invid")) And Request.QueryString("stn").Length > 0 Then
            '        Me.ddlStockTicketNumbers.SelectedValue = Request.QueryString("stn")
            '        BindVehicle()
            '    End If
            'End If
        End If

        If Session("StockTicketNumber") IsNot Nothing Then
            BindVehicle()
            Session("StockTicketNumber") = Nothing
        End If

        If Me.ddlStockTicketNumbers.SelectedIndex = 1 Then
            Me.btnNext.Enabled = True
            Me.btnPrevious.Enabled = False
            Me.btnNext2.Enabled = True
            Me.btnPrevious2.Enabled = False
        Else
            If Me.ddlStockTicketNumbers.SelectedIndex = Me.ddlStockTicketNumbers.Items.Count - 1 Then
                Me.btnNext.Enabled = False
                Me.btnPrevious.Enabled = True
                Me.btnNext2.Enabled = False
                Me.btnPrevious2.Enabled = True
            Else
                Me.btnNext.Enabled = True
                Me.btnPrevious.Enabled = True
                Me.btnNext2.Enabled = True
                Me.btnPrevious2.Enabled = True
            End If
        End If
    End Sub

   Private Sub BindStockTicketNumbers(ByVal intSort As eSort)
      Me.ddlStockTicketNumbers.Items.Clear()
      Me.ddlStockTicketNumbers.Items.Add(New ListItem("[Select Stock Number]", 0))
      Dim objListItem As ListItem
      Dim strItemText As String = ""
      Dim strSortBy As String = ""

      Select Case intSort
         Case Is = eSort.ByWeight
            strSortBy = "TotalWeighted, WholesaleTotal"
         Case Is = eSort.ByAge
            strSortBy = "Age DESC, WholesaleTotal"
         Case Is = eSort.ByProfit
            strSortBy = "Profit DESC, WholesaleTotal"
         Case Is = eSort.ByROI
            strSortBy = "ROI DESC, WholesaleTotal"
      End Select

      Using objVTJStockTicketNumbers As New cardb.Database.VTJStockTicketNumbers("CategorizingStoreNumber = " & pintCategorizingStoreNumber, strSortBy)
         With New DevNet.Tools.Settings.Connections("CARDB")
            Me.lblErrors.Text = .ErrorMsg()
         End With
         For Each objErr As DevNet.Tools.Common.ValidationError In objVTJStockTicketNumbers.ValidationErrors
            Me.lblErrors.Text = Me.lblErrors.Text & "<br />" & objErr.Message
         Next
         For Each objVTJStockTicketNumber As cardb.Database.VTJStockTicketNumber In objVTJStockTicketNumbers
            Select Case intSort
               Case Is = eSort.ByWeight
                  strItemText = objVTJStockTicketNumber.StockTicketNumber & " - " & FormatCurrency(objVTJStockTicketNumber.TotalWeighted, 2)
               Case Is = eSort.ByAge
                  strItemText = objVTJStockTicketNumber.StockTicketNumber & " - " & objVTJStockTicketNumber.Age
               Case Is = eSort.ByProfit
                  strItemText = objVTJStockTicketNumber.StockTicketNumber & " - " & FormatCurrency(objVTJStockTicketNumber.Profit, 2)
               Case Is = eSort.ByROI
                  strItemText = objVTJStockTicketNumber.StockTicketNumber & " - " & objVTJStockTicketNumber.ROI
            End Select

            objListItem = New ListItem(strItemText, objVTJStockTicketNumber.StockTicketNumber.ToString)
            Me.ddlStockTicketNumbers.Items.Add(objListItem)
            If Session("StockTicketNumber") IsNot Nothing AndAlso CType(Session("StockTicketNumber"), String) = objVTJStockTicketNumber.StockTicketNumber Then
               objListItem.Selected = True
            End If
         Next
      End Using
   End Sub

    Private Function NewTD(ByVal strCSSClass As String, ByVal strText As String, Optional ByVal varHorizontalAlign As HorizontalAlign = HorizontalAlign.NotSet) As TableCell
        Dim td As New TableCell
        td.HorizontalAlign = varHorizontalAlign
        td.CssClass = strCSSClass
        td.Text = strText
        pintCount = pintCount + 1
        td.ID = "td" & pintCount
        td.BorderWidth = 1
        Return td
    End Function

    Private Sub BindVehicle()
        If Me.ddlStockTicketNumbers.SelectedValue.ToString.Length > 0 Then
            pintCalled = pintCalled + 1
            tableVehicleHeader.Visible = True
            Dim decWholesaleTotal As Decimal = 0
            Dim decWWTotal As Decimal = 0

            Using objJunkItems As New cardb.Database.JunkItems("StockTicketNumber = '" & Me.ddlStockTicketNumbers.SelectedValue.ToString & "'")
                If objJunkItems.Count > 0 Then
                    Me.chkJunkVehicle.Checked = True
                Else
                    Me.chkJunkVehicle.Checked = False
                End If
            End Using

            Dim intCounter As Integer = 0

            Using objVehiclesToJunk As New cardb.Database.VehiclesToJunk("CategorizingStoreNumber = " & pintCategorizingStoreNumber & " AND StockTicketNumber = '" & Me.ddlStockTicketNumbers.SelectedValue & "'","InventoryNumber")
                txtMessage.Text = objVehiclesToJunk.Count.ToString & " - CategorizingStoreNumber = " & pintCategorizingStoreNumber & " AND StockTicketNumber = '" & Me.ddlStockTicketNumbers.SelectedValue & "'"
                If objVehiclesToJunk.Count > 0 Then
                    With objVehiclesToJunk(0)

                        If IsNumeric(.ARAVehicleId) Then
                            Me.iVehicleImages.Attributes("src") = "http://intranet.counselmanauto.com/junkvehicles/vehicleimages.aspx?aravehicleid=" & .ARAVehicleId
                        Else
                            Me.iVehicleImages.Attributes("src") = ""
                        End If
                        Me.lblStockTicketNumber.Text = .StockTicketNumber
                        Me.lblModel.Text = .ModelName
                        Me.lblYear.Text = .ModelYear
                        Me.lblLocation.Text = .LocationCode
                        Me.lblVehicleCost.Text = FormatCurrency(.VehicleCost, 2)
                        Me.lblVehicleAge.Text = .VehicleAge
                        Me.lblVehicleIncome.Text = FormatCurrency(.VehicleIncome, 2)
                        Me.lblVehicleProfit.Text = FormatCurrency(.VehicleIncome - .VehicleCost, 2)
                        If .VehicleCost > 0 Then
                            Me.lblROI.Text = "ROI:&nbsp;" & CType(FormatCurrency(.VehicleIncome / .VehicleCost, 2), String).Replace("$", "").Replace(",", "").ToString
                        Else
                            Me.lblROI.Text = "ROI:&nbsp;1.00"
                        End If
                    End With
                    Dim tr As TableRow
                    Dim td As TableCell
                    Dim tdChild As TableCell
                    Dim trChild As TableRow
                    Dim chkBox As CheckBox
                    Dim objBustPulls As cardb.Database.BustPulls

                    If objVehiclesToJunk.Count < 21 Then
                        Me.btnPrevious2.Visible = False
                        Me.btnNext2.Visible = False
                    Else
                        Me.btnPrevious2.Visible = True
                        Me.btnNext2.Visible = True
                    End If

                    decWWTotal = 0
                    decWholesaleTotal = 0

                    'If intCounter = 0 Then
                    '    Me.tableVehicleToJunk = New Table
                    '    Me.tableVehicleToJunk.CellPadding = 2
                    '    Me.tableVehicleToJunk.CellSpacing = 0
                    '    Me.tableVehicleToJunk.BorderWidth = New Unit(1, UnitType.Pixel)
                    '    Me.tableVehicleToJunk.Width = New Unit(700, UnitType.Pixel)
                    'End If
                    For Each objTR As TableRow In Me.tableVehicleToJunk.Rows
                        For Each objTD As TableCell In objTR.Cells
                            objTD.Controls.Clear()
                        Next
                        objTR.Cells.Clear()
                    Next
                    Me.tableVehicleToJunk.Rows.Clear()
                    BindTableHeader()
                    For Each objVehicleToJunk As cardb.Database.VehicleToJunk In objVehiclesToJunk
                        intCounter = intCounter + 1
                        objBustPulls = New cardb.Database.BustPulls("InventoryId = " & objVehicleToJunk.InventoryID)

                        'With objVehicleToJunk
                        '    decWWTotal = decWWTotal + CType(.WW, Decimal)
                        '    decWholesaleTotal = decWholesaleTotal + CType(.WholesalePrice, Decimal)
                        '    tr = New TableRow
                        '    Me.tableVehicleToJunk.Rows.Add(tr)

                        '    Select Case .Rating
                        '        Case 4
                        '            tr.CssClass = "trRatingFour"
                        '            tr.Attributes("onmouseout") = "this.className='trRatingFour'"
                        '            tr.Attributes("onmouseover") = "this.className='trdataOver'"
                        '        Case 3
                        '            tr.CssClass = "trRatingThree"
                        '            tr.Attributes("onmouseout") = "this.className='trRatingThree'"
                        '            tr.Attributes("onmouseover") = "this.className='trdataOver'"
                        '        Case 2
                        '            tr.CssClass = "trRatingTwo"
                        '            tr.Attributes("onmouseout") = "this.className='trRatingTwo'"
                        '            tr.Attributes("onmouseover") = "this.className='trdataOver'"
                        '        Case Else
                        '            tr.CssClass = ""
                        '            tr.Attributes("onmouseout") = "this.className='trdataOut'"
                        '            tr.Attributes("onmouseover") = "this.className='trdataOver'"
                        '    End Select

                        '    tr.CssClass = ""
                        '    tr.Attributes("onmouseout") = "this.className='trdataOut'"
                        '    tr.Attributes("onmouseover") = "this.className='trdataOver'"

                        '    'tr.Attributes("onclick") = "window.location.href=""junk.aspx?invid=" & .InventoryID & "&stn=" & Me.ddlStockTicketNumbers.SelectedValue & """;"

                        '    tr.BorderStyle = BorderStyle.Solid
                        '    tr.BorderWidth = New Unit(1, UnitType.Pixel)

                        '    tr.Cells.Add(NewTD("tdsmalldata", intCounter))
                        '    tr.Cells.Add(NewTD("tdsmalldata", "&nbsp;"))
                        '    tr.Cells.Add(NewTD("tdsmalldata", "&nbsp;"))

                        '    'If .CanBustUp Then
                        '    '    td = New TableCell
                        '    '    tr.Cells.Add(td)
                        '    '    td.CssClass = "tdsmalldata"
                        '    '    td.HorizontalAlign = HorizontalAlign.Center
                        '    '    chkBox = New CheckBox
                        '    '    chkBox.ID = "chkBustUp_" & .InventoryID
                        '    '    If objBustPulls.Count > 0 Then If objBustPulls(0).Bust = True Then chkBox.Checked = True
                        '    '    td.Controls.Add(chkBox)
                        '    'Else
                        '    '    tr.Cells.Add(NewTD("tdsmalldata", "&nbsp;"))
                        '    'End If

                        '    'If .CanPull Then
                        '    '    td = New TableCell
                        '    '    tr.Cells.Add(td)
                        '    '    td.CssClass = "tdsmalldata"
                        '    '    td.HorizontalAlign = HorizontalAlign.Center
                        '    '    chkBox = New CheckBox
                        '    '    chkBox.ID = "chkPull_" & .InventoryID
                        '    '    If objBustPulls.Count > 0 Then If objBustPulls(0).Pull = True Then chkBox.Checked = True
                        '    '    td.Controls.Add(chkBox)
                        '    'Else
                        '    '    tr.Cells.Add(NewTD("tdsmalldata", "&nbsp;"))
                        '    'End If

                        '    tr.Cells.Add(NewTD("tdsmalldata", .InventoryNumber))
                        '    tr.Cells.Add(NewTD("tdsmalldata", .QOH, HorizontalAlign.Center))
                        '    tr.Cells.Add(NewTD("tdsmalldata", .Activity, HorizontalAlign.Center))
                        '    tr.Cells.Add(NewTD("tdsmalldata", .Sales, HorizontalAlign.Center))
                        '    tr.Cells.Add(NewTD("tdsmalldata", .Age, HorizontalAlign.Center))
                        '    tr.Cells.Add(NewTD("tdsmall", .ConditionsAndOptions.Replace(",", ", ")))
                        '    tr.Cells.Add(NewTD("tdsmalldata", .ConditionCode, HorizontalAlign.Center))
                        '    tr.Cells.Add(NewTD("tdsmalldata", FormatCurrency(CType(.RetailPrice, Decimal), 2), HorizontalAlign.Right))
                        '    tr.Cells.Add(NewTD("tdsmalldata", FormatCurrency(CType(.WholesalePrice, Decimal), 2), HorizontalAlign.Right))
                        '    tr.Cells.Add(NewTD("tdsmalldata", FormatCurrency(CType(.WW, Decimal), 2), HorizontalAlign.Right))
                        '    tr.Cells.Add(NewTD("tdsmalldata", FormatCurrency(CType(.CostPrice, Decimal), 2), HorizontalAlign.Right))


                        '    If .QOH > 1 Then
                        '        'tr = New TableRow
                        '        'td = New TableCell
                        '        'tr.Cells.Add(td)
                        '        'td.ColumnSpan = 13
                        '        tr.Attributes("onclick") = "displayRow('child" & .InventoryID & "')"
                        '        BindQOHChildren(tableVehicleToJunk, .InventoryNumber, .InventoryID)
                        '    End If
                        'End With

                        With objVehicleToJunk
                            decWWTotal = decWWTotal + CType(.WW, Decimal)
                            decWholesaleTotal = decWholesaleTotal + CType(.WholesalePrice, Decimal)
                            tr = New TableRow
                            Me.tableVehicleToJunk.Rows.Add(tr)

                            Select Case .Rating
                                Case 4
                                    tr.CssClass = "trRatingFour"
                                    tr.Attributes("onmouseout") = "this.className='trRatingFour'"
                                    tr.Attributes("onmouseover") = "this.className='trdataOver'"
                                Case 3
                                    tr.CssClass = "trRatingThree"
                                    tr.Attributes("onmouseout") = "this.className='trRatingThree'"
                                    tr.Attributes("onmouseover") = "this.className='trdataOver'"
                                Case 2
                                    tr.CssClass = "trRatingTwo"
                                    tr.Attributes("onmouseout") = "this.className='trRatingTwo'"
                                    tr.Attributes("onmouseover") = "this.className='trdataOver'"
                                Case Else
                                    tr.CssClass = ""
                                    tr.Attributes("onmouseout") = "this.className='trdataOut'"
                                    tr.Attributes("onmouseover") = "this.className='trdataOver'"
                            End Select

                            tr.BorderStyle = BorderStyle.Solid
                            tr.BorderWidth = New Unit(1, UnitType.Pixel)
                            'If intCounter = 11 Then
                            '    intCounter = 11
                            'End If
                            td = NewTD("tdsmalldata", intCounter)
                            tr.Cells.Add(td)
                            'tr.Cells.Add(NewTD("tdsmalldata", "&nbsp;"))
                            'tr.Cells.Add(NewTD("tdsmalldata", "&nbsp;"))

                            If .CanBustUp Then
                                td = New TableCell
                                tr.Cells.Add(td)
                                'td.ColumnSpan = 1
                                td.CssClass = "tdsmalldata"
                                td.HorizontalAlign = HorizontalAlign.Center
                                chkBox = New CheckBox
                                chkBox.ID = "chkBustUp_" & .InventoryID
                                If objBustPulls.Count > 0 Then If objBustPulls(0).Bust = True Then chkBox.Checked = True
                                td.Controls.Add(chkBox)
                            Else
                                tr.Cells.Add(NewTD("tdsmalldata", "&nbsp;"))
                            End If

                            If .CanPull Then
                                td = New TableCell
                                tr.Cells.Add(td)
                                'td.ColumnSpan = 1
                                td.CssClass = "tdsmalldata"
                                td.HorizontalAlign = HorizontalAlign.Center
                                chkBox = New CheckBox
                                chkBox.ID = "chkPull_" & .InventoryID
                                If objBustPulls.Count > 0 Then If objBustPulls(0).Pull = True Then chkBox.Checked = True
                                td.Controls.Add(chkBox)
                            Else
                                tr.Cells.Add(NewTD("tdsmalldata", "&nbsp;"))
                            End If

                            tr.Cells.Add(NewTD("tdsmalldata", .InventoryNumber))
                            tr.Cells.Add(NewTD("tdsmalldata", .QOH, HorizontalAlign.Center))
                            tr.Cells.Add(NewTD("tdsmalldata", .Activity, HorizontalAlign.Center))
                            tr.Cells.Add(NewTD("tdsmalldata", .Sales, HorizontalAlign.Center))
                            tr.Cells.Add(NewTD("tdsmalldata", .Age, HorizontalAlign.Center))
                            tr.Cells.Add(NewTD("tdsmall", .ConditionsAndOptions.Replace(",", ", ")))
                            tr.Cells.Add(NewTD("tdsmalldata", .ConditionCode, HorizontalAlign.Center))
                            tr.Cells.Add(NewTD("tdsmalldata", FormatCurrency(CType(.RetailPrice, Decimal), 2), HorizontalAlign.Right))
                            tr.Cells.Add(NewTD("tdsmalldata", FormatCurrency(CType(.WholesalePrice, Decimal), 2), HorizontalAlign.Right))
                            tr.Cells.Add(NewTD("tdsmalldata", FormatCurrency(CType(.WW, Decimal), 2), HorizontalAlign.Right))
                            tr.Cells.Add(NewTD("tdsmalldata", FormatCurrency(CType(.CostPrice, Decimal), 2), HorizontalAlign.Right))

                            If .QOH > 1 Then
                                tr.Attributes("onclick") = "newWindow('related_parts.aspx?inventorynum=" & .InventoryNumber & "&inventoryid=" & .InventoryID & "','','750','450','resizable,scrollbars,status,toolbar')"
                                'onClick="newWindow('http://www.java-scripts.net','','750','450','resizable,scrollbars,status,toolbar')"
                                'trChild = New TableRow
                                'Me.tableVehicleToJunk.Rows.Add(trChild)
                                ''trChild.Attributes("style") = "display:none;"
                                'trChild.ID = "child" & .InventoryID

                                'tdChild = New TableCell
                                'trChild.Cells.Add(tdChild)
                                'tdChild.ColumnSpan = 100
                                'BindQOHChildren(tdChild, .InventoryNumber, .InventoryID)
                            End If
                        End With
                    Next
                    Me.txtMessage.Text = pintCalled.ToString & " (" & intCounter.ToString & ")" & Me.txtMessage.Text
                    objBustPulls = Nothing
                    Me.lblWeightedWholesaleTotal.Text = FormatCurrency(decWWTotal, 2)
                    Me.lblWholesaleTotal.Text = FormatCurrency(decWholesaleTotal, 2)
                End If
            End Using

            Me.lblLocationLabel.Text = CType(Me.ddlStockTicketNumbers.SelectedIndex, String) & " of " & CType(Me.ddlStockTicketNumbers.Items.Count - 1, String)
        End If
    End Sub

    Protected Sub ddlStockTicketNumbers_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlStockTicketNumbers.SelectedIndexChanged
        If Me.IsPostBack And Not pbolNavigating Then
            BindVehicle()
        End If
    End Sub

    Protected Sub btnPrevious_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnPrevious.Click, btnPrevious2.Click
        pbolNavigating = True
        SetJunk()
        SetBustPulls()
        Me.ddlStockTicketNumbers.SelectedIndex = Me.ddlStockTicketNumbers.SelectedIndex - 1
        If Me.ddlStockTicketNumbers.SelectedIndex = 1 Then
            Me.btnNext.Enabled = True
            Me.btnPrevious.Enabled = False
            Me.btnNext2.Enabled = True
            Me.btnPrevious2.Enabled = False
        Else
            Me.btnNext.Enabled = True
            Me.btnPrevious.Enabled = True
            Me.btnNext2.Enabled = True
            Me.btnPrevious2.Enabled = True
        End If
        BindVehicle()
    End Sub

    Protected Sub btnNext_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnNext.Click, btnNext2.Click
        pbolNavigating = True
        SetJunk()
        SetBustPulls()
        Me.ddlStockTicketNumbers.SelectedIndex = Me.ddlStockTicketNumbers.SelectedIndex + 1
        If Me.ddlStockTicketNumbers.SelectedIndex = Me.ddlStockTicketNumbers.Items.Count - 1 Then
            Me.btnNext.Enabled = False
            Me.btnNext2.Enabled = False
            Me.btnPrevious.Enabled = True
            Me.btnPrevious2.Enabled = True
        Else
            Me.btnNext.Enabled = True
            Me.btnNext2.Enabled = True
            Me.btnPrevious.Enabled = True
            Me.btnPrevious2.Enabled = True
        End If
        BindVehicle()
    End Sub

    Private Sub SetJunk()
        Using objJunkItems As New cardb.Database.JunkItems("StockTicketNumber = '" & Me.ddlStockTicketNumbers.SelectedValue.ToString & "'")
            If objJunkItems.Count > 0 Then
                If Me.chkJunkVehicle.Checked = False Then
                    objJunkItems.Item(0).Delete()
                    objJunkItems.Update()
                End If
            Else
                If Me.chkJunkVehicle.Checked Then
                    Dim objJunkItem As cardb.Database.JunkItem
                    objJunkItem = objJunkItems.NewJunkItem
                    objJunkItems.AddJunkItem(objJunkItem)
                    objJunkItem.StockTicketNumber = Me.ddlStockTicketNumbers.SelectedValue.ToString
                    If Not objJunkItems.Update() Then
                        Me.lblErrors.Text = ""
                        For Each objErr As DevNet.Tools.Common.ValidationError In objJunkItems.ValidationErrors
                            Me.lblErrors.Text = Me.lblErrors.Text & objErr.Message & "<br />"
                        Next
                    End If
                End If
            End If
        End Using
    End Sub

    Private Sub SetBustPulls()
        Dim bolBust As Boolean = False
        Dim bolPull As Boolean = False
        Dim objBustPull As cardb.Database.BustPull
        Using objVehiclesToJunk As New cardb.Database.VehiclesToJunk("(StockTicketNumber = '" & Me.ddlStockTicketNumbers.SelectedValue & "')")
            For Each objVehicleToJunk As cardb.Database.VehicleToJunk In objVehiclesToJunk
                bolBust = False
                bolPull = False

                If objVehicleToJunk.CanBustUp Then
                    If Request.Form("chkBustUp_" & objVehicleToJunk.InventoryID) = "on" Then
                        bolBust = True
                    End If
                End If

                If objVehicleToJunk.CanPull Then
                    If Request.Form("chkPull_" & objVehicleToJunk.InventoryID) = "on" Then
                        bolPull = True
                    End If
                End If

                Using objBustPulls As New cardb.Database.BustPulls("InventoryId = " & objVehicleToJunk.InventoryID)
                    If objBustPulls.Count > 0 Then
                        If bolBust = False And bolPull = False Then
                            objBustPulls(0).Delete()
                        Else
                            objBustPulls(0).Bust = bolBust
                            objBustPulls(0).Pull = bolPull
                        End If
                    Else
                        If bolBust = True Or bolPull = True Then
                            objBustPull = objBustPulls.NewBustPull
                            objBustPulls.AddBustPull(objBustPull)
                            objBustPull.InventoryId = objVehicleToJunk.InventoryID
                            objBustPull.Pull = bolPull
                            objBustPull.Bust = bolBust
                        End If
                    End If
                    objBustPulls.Update()
                End Using
            Next
        End Using
    End Sub


    Private Sub BindQOHChildren(ByRef containerTD As TableCell, ByVal strInventoryNumber As String, ByVal intInventoryID As Integer)
        Dim tbl As New Table
        Dim tr As TableRow
        Dim td As TableCell
        Dim strShowHideJS As String = ""

        tbl.Width = 680
        containerTD.Controls.Add(tbl)

        tr = New TableRow
        tbl.Rows.Add(tr)

        tr.BorderStyle = BorderStyle.Solid
        tr.BorderWidth = New Unit(1, UnitType.Pixel)
        tr.BackColor = Drawing.Color.LightGray
        td = NewTD("tdsmallhead", "Bust", HorizontalAlign.Center)
        td.Width = 30
        tr.Cells.Add(td)
        td = NewTD("tdsmallhead", "Pull", HorizontalAlign.Center)
        td.Width = 30
        tr.Cells.Add(td)
        td = NewTD("tdsmallhead", "Store", HorizontalAlign.Center)
        td.Width = 50
        tr.Cells.Add(td)
        td = NewTD("tdsmallhead", "Stock&nbsp;#")
        td.Width = 60
        tr.Cells.Add(td)
        'td = NewTD("tdsmallhead", "Inv.&nbsp;#")
        'tr.Cells.Add(td)
        'td = NewTD("tdsmallhead", "QOH", HorizontalAlign.Center)
        'tr.Cells.Add(td)
        'td = NewTD("tdsmallhead", "Activity", HorizontalAlign.Center)
        'tr.Cells.Add(td)
        'td = NewTD("tdsmallhead", "Sales", HorizontalAlign.Center)
        'tr.Cells.Add(td)
        td = NewTD("tdsmallhead", "Age", HorizontalAlign.Center)
        tr.Cells.Add(td)
        td = NewTD("tdsmallhead", "Conditions/Options")
        tr.Cells.Add(td)
        td = NewTD("tdsmallhead", "Dmg Code", HorizontalAlign.Center)
        tr.Cells.Add(td)
        td = NewTD("tdsmallhead", "Retail", HorizontalAlign.Center)
        tr.Cells.Add(td)
        td = NewTD("tdsmallhead", "Wholesale", HorizontalAlign.Center)
        tr.Cells.Add(td)
        td = NewTD("tdsmallhead", "Weighted", HorizontalAlign.Center)
        tr.Cells.Add(td)
        td = NewTD("tdsmallhead", "Cost", HorizontalAlign.Center)
        tr.Cells.Add(td)

        Dim objBustPulls As cardb.Database.BustPulls

        Using objVehiclesToJunk As New cardb.Database.VehiclesToJunk("InventoryNumber = '" & strInventoryNumber & "' AND InventoryID <> " & intInventoryID)
            If objVehiclesToJunk.Count > 0 Then
                For Each objVehicleToJunk As cardb.Database.VehicleToJunk In objVehiclesToJunk
                    objBustPulls = New cardb.Database.BustPulls("InventoryId = " & objVehicleToJunk.InventoryID)

                    With objVehicleToJunk
                        tr = New TableRow
                        tbl.Rows.Add(tr)

                        'tr.ID = "child" & .VehicleToJunkKey
                        'If strShowHideJS.Length = 0 Then strShowHideJS = strShowHideJS & "document.getElementById('child" & .VehicleToJunkKey & "').style.display='none';"

                        tr.BorderStyle = BorderStyle.Solid
                        tr.BorderWidth = New Unit(1, UnitType.Pixel)
                        tr.BackColor = Drawing.Color.LightGray

                        If objBustPulls.Count > 0 AndAlso objBustPulls(0).Bust = True Then
                            td = NewTD("tdsmalldata", "*", HorizontalAlign.Center)
                        Else
                            td = NewTD("tdsmalldata", "&nbsp;")
                        End If
                        td.BorderWidth = 1
                        tr.Cells.Add(td)

                        If objBustPulls.Count > 0 AndAlso objBustPulls(0).Pull = True Then
                            td = NewTD("tdsmalldata", "*", HorizontalAlign.Center)
                        Else
                            td = NewTD("tdsmalldata", "&nbsp;")
                        End If
                        td.BorderWidth = 1
                        tr.Cells.Add(td)

                        td = NewTD("tdsmalldata", .CategorizingStoreNumber, HorizontalAlign.Center)
                        tr.Cells.Add(td)
                        td = NewTD("tdsmalldata", .StockTicketNumber, HorizontalAlign.Center)
                        tr.Cells.Add(td)
                        'td = NewTD("tdsmalldata", .InventoryNumber)
                        'tr.Cells.Add(td)
                        'td = NewTD("tdsmalldata", .QOH, HorizontalAlign.Center)
                        'tr.Cells.Add(td)
                        'td = NewTD("tdsmalldata", .Activity, HorizontalAlign.Center)
                        'tr.Cells.Add(td)
                        'td = NewTD("tdsmalldata", .Sales, HorizontalAlign.Center)
                        'tr.Cells.Add(td)
                        td = NewTD("tdsmalldata", .Age, HorizontalAlign.Center)
                        tr.Cells.Add(td)
                        td = NewTD("tdsmall", .ConditionsAndOptions.Replace(",", ", "))
                        tr.Cells.Add(td)
                        td = NewTD("tdsmalldata", .ConditionCode, HorizontalAlign.Center)
                        tr.Cells.Add(td)
                        td = NewTD("tdsmalldata", FormatCurrency(.RetailPrice, 2), HorizontalAlign.Right)
                        tr.Cells.Add(td)
                        td = NewTD("tdsmalldata", FormatCurrency(.WholesalePrice, 2), HorizontalAlign.Right)
                        tr.Cells.Add(td)
                        td = NewTD("tdsmalldata", FormatCurrency(.WW, 2), HorizontalAlign.Right)
                        tr.Cells.Add(td)
                        td = NewTD("tdsmalldata", FormatCurrency(.CostPrice, 2), HorizontalAlign.Right)
                        tr.Cells.Add(td)
                    End With
                Next
                objBustPulls = Nothing
            End If
        End Using
    End Sub

    Private Sub BindTableHeader()
        Dim tr As TableRow
        Dim td As TableCell

        tr = New TableRow
        tr.BorderStyle = BorderStyle.Solid
        tr.BorderWidth = New Unit(1, UnitType.Pixel)
        Me.tableVehicleToJunk.Rows.Add(tr)

        td = New TableCell
        td.CssClass = "tdsmallhead"
        td.Width = New Unit(15, UnitType.Pixel)
        td.Text = "#"
        tr.Cells.Add(td)

        td = New TableCell
        td.CssClass = "tdsmallhead"
        td.Width = New Unit(50, UnitType.Pixel)
        td.Text = "Bust"
        tr.Cells.Add(td)

        td = New TableCell
        td.CssClass = "tdsmallhead"
        td.Width = New Unit(50, UnitType.Pixel)
        td.Text = "Pull"
        tr.Cells.Add(td)

        td = New TableCell
        td.CssClass = "tdsmallhead"
        td.Width = New Unit(50, UnitType.Pixel)
        td.Text = "Inv."
        tr.Cells.Add(td)

        td = New TableCell
        td.CssClass = "tdsmallhead"
        td.HorizontalAlign = HorizontalAlign.Center
        td.Width = New Unit(25, UnitType.Pixel)
        td.Text = "QOH"
        tr.Cells.Add(td)

        td = New TableCell
        td.CssClass = "tdsmallhead"
        td.HorizontalAlign = HorizontalAlign.Center
        td.Width = New Unit(25, UnitType.Pixel)
        td.Text = "Activity"
        tr.Cells.Add(td)

        td = New TableCell
        td.CssClass = "tdsmallhead"
        td.HorizontalAlign = HorizontalAlign.Center
        td.Width = New Unit(25, UnitType.Pixel)
        td.Text = "Sales"
        tr.Cells.Add(td)

        td = New TableCell
        td.CssClass = "tdsmallhead"
        td.HorizontalAlign = HorizontalAlign.Center
        td.Width = New Unit(25, UnitType.Pixel)
        td.Text = "Age"
        tr.Cells.Add(td)

        td = New TableCell
        td.CssClass = "tdsmallhead"
        td.Width = New Unit(50, UnitType.Pixel)
        td.Text = "Conditions/Options"
        tr.Cells.Add(td)

        td = New TableCell
        td.CssClass = "tdsmallhead"
        td.HorizontalAlign = HorizontalAlign.Center
        td.Width = New Unit(25, UnitType.Pixel)
        td.Text = "Dmg Code"
        tr.Cells.Add(td)

        td = New TableCell
        td.CssClass = "tdsmallhead"
        td.HorizontalAlign = HorizontalAlign.Center
        td.Width = New Unit(75, UnitType.Pixel)
        td.Text = "Retail"
        tr.Cells.Add(td)

        td = New TableCell
        td.CssClass = "tdsmallhead"
        td.HorizontalAlign = HorizontalAlign.Center
        td.Width = New Unit(75, UnitType.Pixel)
        td.Text = "Wholesale"
        tr.Cells.Add(td)

        td = New TableCell
        td.CssClass = "tdsmallhead"
        td.HorizontalAlign = HorizontalAlign.Center
        td.Width = New Unit(75, UnitType.Pixel)
        td.Text = "Weighted"
        tr.Cells.Add(td)

        td = New TableCell
        td.CssClass = "tdsmallhead"
        td.HorizontalAlign = HorizontalAlign.Center
        td.Width = New Unit(75, UnitType.Pixel)
        td.Text = "Cost"
        tr.Cells.Add(td)
    End Sub

   Protected Sub radSort_Weighted_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles radSort_Weighted.CheckedChanged
      If radSort_Weighted.Checked Then
         BindStockTicketNumbers(eSort.ByWeight)
      End If
   End Sub

   Protected Sub radSort_Age_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles radSort_Age.CheckedChanged
      If radSort_Age.Checked Then
         BindStockTicketNumbers(eSort.ByAge)
      End If
   End Sub

   Protected Sub radSort_Profit_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles radSort_Profit.CheckedChanged
      If radSort_Profit.Checked Then
         BindStockTicketNumbers(eSort.ByProfit)
      End If
   End Sub

   Protected Sub radSort_RIO_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles radSort_RIO.CheckedChanged
      If radSort_RIO.Checked Then
         BindStockTicketNumbers(eSort.ByROI)
      End If
   End Sub
End Class