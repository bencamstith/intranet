﻿
Partial Class junk_report2
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Me.lblReportDate.Text = "&nbsp;-&nbsp;" & Date.Now.ToString("MM/dd/yyyy")
        If IsPostBack Then
            If Request.Form("__EVENTTARGET") IsNot Nothing Then
                If Request.Form("__EVENTTARGET").ToString.StartsWith("tr_") Then
                    Session("StockTicketNumber") = Request.Form("__EVENTTARGET").ToString.Split("_")(1).ToString
                    Response.Redirect("junk3.aspx")
                End If
            End If
        Else
            BindReport()
        End If
    End Sub

    Private Sub BindReport()
        Using objJunkLists As New cardb.Database.StockTicketNumbers("CategorizingStoreNumber = 3 AND (StockTicketNumber IN (SELECT StockTicketNumber FROM dbo.tblJunkItems))", "LocationCode")
            Dim tr As TableRow

            For Each objVehicle As cardb.Database.StockTicketNumber In objJunkLists
                tr = New TableRow
                With objVehicle
                    Me.tableVehicles.Rows.Add(tr)
                    tr.ID = "tr_" & .StockTicketNumber
                    tr.Attributes("onmouseout") = "this.className='trdataOut'"
                    tr.Attributes("onmouseover") = "this.className='trdataOver'"
                    tr.Attributes("onclick") = "javascript:setTimeout('__doPostBack(\'tr_" & .StockTicketNumber & "\',\'\')', 0)"

                    tr.Cells.Add(NewTD("tdsmalldata", .LocationCode))
                    tr.Cells.Add(NewTD("tdsmalldata", .ModelYear))
                    tr.Cells.Add(NewTD("tdsmalldata", .ModelName))
                    tr.Cells.Add(NewTD("tdsmalldata", .StockTicketNumber))
                    tr.Cells.Add(NewTD("tdsmalldata", Notes(.StockTicketNumber)))
                    tr.Cells.Add(NewTD("tdsmalldata", "&nbsp;"))
                    tr.Cells.Add(NewTD("tdsmalldata", "&nbsp;"))
                    tr.Cells.Add(NewTD("tdsmalldata", "&nbsp;"))
                    tr.Cells.Add(NewTD("tdsmalldata", "&nbsp;"))
                End With
            Next
        End Using
    End Sub

    Private Function Notes(ByVal strStockTicketNumber As String) As String
        Dim strNotes As String = ""
        Using objVehicles As New cardb.Database.VehiclesToJunk("StockTicketNumber = '" & strStockTicketNumber & "'", "InventoryNumber")
            For Each objVehicle As cardb.Database.VehicleToJunk In objVehicles
                Using objBustPulls As New cardb.Database.BustPulls("InventoryId = " & objVehicle.InventoryID)
                    If objBustPulls.Count > 0 Then
                        With objBustPulls
                            If .Item(0).Pull Then
                                strNotes = strNotes & "Pull " & objVehicle.InventoryNumber & "<br />"
                            End If
                        End With
                    End If
                End Using
            Next
        End Using

        Return strNotes
    End Function

    Private Function NewTD(ByVal strCSSClass As String, ByVal strText As String, Optional ByVal varHorizontalAlign As HorizontalAlign = HorizontalAlign.NotSet) As TableCell
        Dim td As New TableCell
        td.HorizontalAlign = varHorizontalAlign
        td.CssClass = strCSSClass
        td.BorderWidth = New Unit(1, UnitType.Pixel)
        td.Text = strText
        Return td
    End Function
End Class
