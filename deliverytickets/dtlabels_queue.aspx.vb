﻿Partial Class dtlabels_queue
   Inherits System.Web.UI.Page

   Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
      If Not Request.QueryString("lineitemid") Is Nothing Then
         Dim intStore As Integer = 0
         ' Set intStore to url value if it exists.
         If Not Request.QueryString("store") Is Nothing AndAlso IsNumeric(Request.QueryString("store")) Then intStore = Request.QueryString("store")

         Using objDeliveryTicketLineItems As New carpldb.Database.DeliveryTicketLineItems("LineItemId = " & Request.QueryString("lineitemid").ToString)
            If objDeliveryTicketLineItems.Count > 0 Then
               objDeliveryTicketLineItems(0).BL.PrintLabel(intStore)
            End If
         End Using
      End If
      Response.Redirect("dtlabels_lineitems.aspx?deliveryticketid=" & Request.QueryString("deliveryticketid").ToString)
   End Sub
End Class
