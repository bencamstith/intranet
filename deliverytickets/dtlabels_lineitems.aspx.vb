﻿
Partial Class dtlabels_lineitems
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack AndAlso Not Request.QueryString("deliveryticketid") Is Nothing Then
            BindDeliveryTicketLineItems()
        End If
    End Sub

   Private Sub BindDeliveryTicketLineItems()
        Me.tDeliveryTicketLineItems.Rows.Clear()
        Dim tr As TableRow
        Dim td As TableCell
        Dim lnk As LinkButton

        Dim strWhere As String = ""
        Dim str As String = ""

        If Not Request.QueryString("deliveryticketid") Is Nothing Then
            For x As Integer = 0 To Request.QueryString("deliveryticketid").ToString.Split(",").Length - 1
                str = Request.QueryString("deliveryticketid").Split(",")(x).ToString
                If strWhere.Length > 0 Then strWhere &= " OR "
                strWhere &= "(Revision = " & str.Split("-")(0).ToString & " AND DeliveryTicketID = " & str.Split("-")(1).ToString & ")"
            Next
        End If

        Using objDeliveryTicketLineItems As New carpldb.Database.DeliveryTicketLineItems(strWhere, "Revision, LineItemId")
            For Each objDeliveryTicketLineItem As carpldb.Database.DeliveryTicketLineItem In objDeliveryTicketLineItems
                tr = New TableRow
                Me.tDeliveryTicketLineItems.Rows.Add(tr)

                tr.ID = "lineitem_" & objDeliveryTicketLineItem.LineItemID
                tr.Attributes("onmouseout") = "this.className='trdataOut'"
                tr.Attributes("onmouseover") = "this.className='trdataOver'"

                td = New TableCell
                tr.Cells.Add(td)
                td.VerticalAlign = VerticalAlign.Top
                td.Attributes("style") = "width: 50px;"
                td.Text = objDeliveryTicketLineItem.Revision

                td = New TableCell
                tr.Cells.Add(td)
                td.VerticalAlign = VerticalAlign.Top
                td.Attributes("style") = "width: 35px;text-align: center;"
                td.Text = objDeliveryTicketLineItem.Quantity

                td = New TableCell
                tr.Cells.Add(td)
                td.VerticalAlign = VerticalAlign.Top
                td.Attributes("style") = "width: 90px;"
                td.Text = objDeliveryTicketLineItem.InventoryNumber

                td = New TableCell
                tr.Cells.Add(td)
                td.VerticalAlign = VerticalAlign.Top
                td.Attributes("style") = "width: 190px;"
                td.Text = objDeliveryTicketLineItem.LineItemDescription

                td = New TableCell
                tr.Cells.Add(td)
                td.VerticalAlign = VerticalAlign.Top
                td.Attributes("style") = "width: 60px;text-align: center;"

                If objDeliveryTicketLineItem.BL.Inventory Is Nothing Then
                    td.Text = "Not In Inv."
                Else
                    td.Text = objDeliveryTicketLineItem.BL.Inventory.CategorizingStoreNumber
                End If

                td = New TableCell
                tr.Cells.Add(td)
                td.VerticalAlign = VerticalAlign.Top
                td.Attributes("style") = "width: 75px;"
                td.Text = objDeliveryTicketLineItem.BL.PrintLabelStatus.Replace(" ", "&nbsp;")

                td = New TableCell
                tr.Cells.Add(td)
                td.VerticalAlign = VerticalAlign.Top
                td.Attributes("style") = "width: 80px;"
                If objDeliveryTicketLineItem.BL.PrintLabelCanPrint Then
                    lnk = New LinkButton
                    lnk.Text = "Print 1"
                    lnk.PostBackUrl = "dtlabels_queue.aspx?store=1&deliveryticketid=" & Request.QueryString("deliveryticketid").ToString & "&lineitemid=" & objDeliveryTicketLineItem.LineItemID
                    td.Controls.Add(lnk)
                    lnk = Nothing
                Else
                    td.Text = "Wait"
                End If
                td.Controls.Add(NewLiteral("<br />"))
                'td = New TableCell
                'tr.Cells.Add(td)
                'td.Attributes("style") = "width: 80px;"
                If objDeliveryTicketLineItem.BL.PrintLabelCanPrint Then
                    lnk = New LinkButton
                    lnk.Text = "Print 3"
                    lnk.PostBackUrl = "dtlabels_queue.aspx?store=3&deliveryticketid=" & Request.QueryString("deliveryticketid").ToString & "&lineitemid=" & objDeliveryTicketLineItem.LineItemID
                    td.Controls.Add(lnk)
                    lnk = Nothing
                Else
                    td.Text = "Wait"
                End If
            Next
        End Using
   End Sub

   Private Function NewLiteral(ByVal strHTML As String) As Literal
      Dim objLiteral As New Literal
      objLiteral.Text = strHTML
      Return objLiteral
   End Function

End Class