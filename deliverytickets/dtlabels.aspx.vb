﻿
Partial Class dtlabels
    Inherits System.Web.UI.Page

   Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
      Me.txtDeliveryTicketNumber.Focus()
      Me.txtDeliveryTicketNumber.Attributes("onFocus") = "this.select()"
   End Sub

   Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSearch.Click
      lblMessage.Text = ""

      If Me.txtDeliveryTicketNumber.Text = Me.txtDeliveryTicketNumber_Previous.Text And Me.txtDeliveryTicketNumber_Previous.Text <> Me.txtDeliveryTicketNumber_Printed.Text Then
         If Me.txtCount.Text = "1" Then
            Using objDeliveryTicketLineItems As New carpldb.Database.DeliveryTicketLineItems("(vwPL_DeliveryTickets.IsLastRevision = 1) AND (vwPL_DeliveryTickets.DeliveryTicketNumber = " & Me.txtDeliveryTicketNumber_Previous.Text & ")", "Revision, LineItemId", "INNER JOIN vwPL_DeliveryTickets ON vwPL_DeliveryTicketLineItems.DeliveryTicketID = vwPL_DeliveryTickets.DeliveryTicketID")
               If objDeliveryTicketLineItems.Count = 1 Then
                  Me.iframeDeliveryTicketLineItems.Attributes("src") = "dtlabels_queue.aspx?deliveryticketid=" & objDeliveryTicketLineItems(0).DeliveryTicketID & "&lineitemid=" & objDeliveryTicketLineItems(0).LineItemID
                  Me.txtDeliveryTicketNumber_Printed.Text = Me.txtDeliveryTicketNumber.Text
               Else
                  lblMessage.Text = "More than 1 item to print, click a store to print."
               End If
            End Using
         Else
            lblMessage.Text = "Click a store to print."
         End If
         Exit Sub
      End If

      Me.txtDeliveryTicketNumber_Previous.Text = ""
      Me.txtDeliveryTicketNumber_Printed.Text = ""

        If Me.txtDeliveryTicketNumber.Text.Trim.Length = 0 Or IsNumeric(Me.txtDeliveryTicketNumber.Text) = False Then
            lblMessage.Text = "Enter a valid Delivery Ticket #."
        Else
            Me.txtDeliveryTicketNumber_Previous.Text = ""
            Dim strDeliveryTicketIds As String = ""
            Using objDeliveryTickets As New carpldb.Database.DeliveryTickets("IsLastRevision = 1 AND DeliveryTicketNumber = " & Me.txtDeliveryTicketNumber.Text)
                txtCount.Text = objDeliveryTickets.Count
                Me.txtDeliveryTicketNumber_Previous.Text = Me.txtDeliveryTicketNumber.Text
                For Each objDeliveryTicket As carpldb.Database.DeliveryTicket In objDeliveryTickets
                    If strDeliveryTicketIds.Length > 0 Then strDeliveryTicketIds &= ","
                    strDeliveryTicketIds &= objDeliveryTicket.Revision & "-" & objDeliveryTicket.DeliveryTicketID.ToString
                Next
                For Each objErr As DevNet.Tools.Common.ValidationError In objDeliveryTickets.ValidationErrors
                    lblMessage.Text = objErr.Message & "<br />"
                Next
            End Using

            Me.iframeDeliveryTicketLineItems.Attributes("src") = "dtlabels_lineitems.aspx?deliveryticketid=" & Server.UrlEncode(strDeliveryTicketIds)
        End If
   End Sub

End Class
