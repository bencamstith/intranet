﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="dtlabels_lineitems.aspx.vb" Inherits="dtlabels_lineitems" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Delivery Ticket LineItems</title>
    <link href="../style.css" rel="stylesheet" type="text/css" />
    <link href="../table_style.css" rel="stylesheet" type="text/css" />
</head>
<body>
    <form id="form1" runat="server">
        <asp:Table ID="tDeliveryTicketLineItems" CellPadding="2" CellSpacing="0" Width="580" Border="0" runat="server">
        </asp:Table>
    </form>
</body>
</html>