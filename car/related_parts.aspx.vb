﻿
Partial Class related_parts
    Inherits System.Web.UI.Page

    Private pintCount As Integer = 0

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        BindQOHChildren(Request.QueryString("inventorynum").ToString, Request.QueryString("inventoryid").ToString)
        Me.lblInventoryNumber.Text = Request.QueryString("inventorynum").ToString
    End Sub

    Private Sub BindQOHChildren(ByVal strInventoryNumber As String, ByVal intInventoryID As Integer)
        Dim tr As TableRow
        Dim td As TableCell
        Dim strShowHideJS As String = ""

        tbl.Width = 680

        tr = New TableRow
        tbl.Rows.Add(tr)

        tr.BorderStyle = BorderStyle.Solid
        tr.BorderWidth = New Unit(1, UnitType.Pixel)
        tr.BackColor = Drawing.Color.LightGray
        td = NewTD("tdsmallhead", "Bust", HorizontalAlign.Center)
        td.Width = 30
        tr.Cells.Add(td)
        td = NewTD("tdsmallhead", "Pull", HorizontalAlign.Center)
        td.Width = 30
        tr.Cells.Add(td)
        td = NewTD("tdsmallhead", "Store", HorizontalAlign.Center)
        td.Width = 50
        tr.Cells.Add(td)
        td = NewTD("tdsmallhead", "Stock&nbsp;#")
        td.Width = 60
        tr.Cells.Add(td)
        'td = NewTD("tdsmallhead", "Inv.&nbsp;#")
        'tr.Cells.Add(td)
        'td = NewTD("tdsmallhead", "QOH", HorizontalAlign.Center)
        'tr.Cells.Add(td)
        'td = NewTD("tdsmallhead", "Activity", HorizontalAlign.Center)
        'tr.Cells.Add(td)
        'td = NewTD("tdsmallhead", "Sales", HorizontalAlign.Center)
        'tr.Cells.Add(td)
        td = NewTD("tdsmallhead", "Age", HorizontalAlign.Center)
        tr.Cells.Add(td)
        td = NewTD("tdsmallhead", "Conditions/Options")
        tr.Cells.Add(td)
        td = NewTD("tdsmallhead", "Dmg Code", HorizontalAlign.Center)
        tr.Cells.Add(td)
        td = NewTD("tdsmallhead", "Retail", HorizontalAlign.Center)
        tr.Cells.Add(td)
        td = NewTD("tdsmallhead", "Wholesale", HorizontalAlign.Center)
        tr.Cells.Add(td)
        td = NewTD("tdsmallhead", "Weighted", HorizontalAlign.Center)
        tr.Cells.Add(td)
        td = NewTD("tdsmallhead", "Cost", HorizontalAlign.Center)
        tr.Cells.Add(td)

        Dim objBustPulls As cardb.Database.BustPulls

        Using objVehiclesToJunk As New cardb.Database.VehiclesToJunk("InventoryNumber = '" & strInventoryNumber & "' AND InventoryID <> " & intInventoryID)
            If objVehiclesToJunk.Count > 0 Then
                For Each objVehicleToJunk As cardb.Database.VehicleToJunk In objVehiclesToJunk
                    objBustPulls = New cardb.Database.BustPulls("InventoryId = " & objVehicleToJunk.InventoryID)

                    With objVehicleToJunk
                        tr = New TableRow
                        tbl.Rows.Add(tr)

                        'tr.ID = "child" & .VehicleToJunkKey
                        'If strShowHideJS.Length = 0 Then strShowHideJS = strShowHideJS & "document.getElementById('child" & .VehicleToJunkKey & "').style.display='none';"

                        tr.BorderStyle = BorderStyle.Solid
                        tr.BorderWidth = New Unit(1, UnitType.Pixel)
                        tr.BackColor = Drawing.Color.LightGray

                        If objBustPulls.Count > 0 AndAlso objBustPulls(0).Bust = True Then
                            td = NewTD("tdsmalldata", "*", HorizontalAlign.Center)
                        Else
                            td = NewTD("tdsmalldata", "&nbsp;")
                        End If
                        td.BorderWidth = 1
                        tr.Cells.Add(td)

                        If objBustPulls.Count > 0 AndAlso objBustPulls(0).Pull = True Then
                            td = NewTD("tdsmalldata", "*", HorizontalAlign.Center)
                        Else
                            td = NewTD("tdsmalldata", "&nbsp;")
                        End If
                        td.BorderWidth = 1
                        tr.Cells.Add(td)

                        td = NewTD("tdsmalldata", .CategorizingStoreNumber, HorizontalAlign.Center)
                        tr.Cells.Add(td)
                        td = NewTD("tdsmalldata", .StockTicketNumber, HorizontalAlign.Center)
                        tr.Cells.Add(td)
                        'td = NewTD("tdsmalldata", .InventoryNumber)
                        'tr.Cells.Add(td)
                        'td = NewTD("tdsmalldata", .QOH, HorizontalAlign.Center)
                        'tr.Cells.Add(td)
                        'td = NewTD("tdsmalldata", .Activity, HorizontalAlign.Center)
                        'tr.Cells.Add(td)
                        'td = NewTD("tdsmalldata", .Sales, HorizontalAlign.Center)
                        'tr.Cells.Add(td)
                        td = NewTD("tdsmalldata", .Age, HorizontalAlign.Center)
                        tr.Cells.Add(td)
                        td = NewTD("tdsmall", .ConditionsAndOptions.Replace(",", ", "))
                        tr.Cells.Add(td)
                        td = NewTD("tdsmalldata", .ConditionCode, HorizontalAlign.Center)
                        tr.Cells.Add(td)
                        td = NewTD("tdsmalldata", FormatCurrency(.RetailPrice, 2), HorizontalAlign.Right)
                        tr.Cells.Add(td)
                        td = NewTD("tdsmalldata", FormatCurrency(.WholesalePrice, 2), HorizontalAlign.Right)
                        tr.Cells.Add(td)
                        td = NewTD("tdsmalldata", FormatCurrency(.WW, 2), HorizontalAlign.Right)
                        tr.Cells.Add(td)
                        td = NewTD("tdsmalldata", FormatCurrency(.CostPrice, 2), HorizontalAlign.Right)
                        tr.Cells.Add(td)
                    End With
                Next
                objBustPulls = Nothing
            End If
        End Using
    End Sub

    Private Function NewTD(ByVal strCSSClass As String, ByVal strText As String, Optional ByVal varHorizontalAlign As HorizontalAlign = HorizontalAlign.NotSet) As TableCell
        Dim td As New TableCell
        td.HorizontalAlign = varHorizontalAlign
        td.CssClass = strCSSClass
        td.Text = strText
        pintCount = pintCount + 1
        td.ID = "td" & pintCount
        td.BorderWidth = 1
        Return td
    End Function

End Class
