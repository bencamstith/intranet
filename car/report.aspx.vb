﻿Imports Persits.PDF

Partial Class report
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        ' Create instance of the PDF manager.
        Dim objPDF As PdfManager = New PdfManager()

        ' Create new document.
        Dim objDoc As PdfDocument = objPDF.CreateDocument()

        ' Specify title and creator for the document.
        objDoc.Title = "Vehicles to Junk Report"
        objDoc.Creator = ""
        Dim strTempFile As String = Server.MapPath("junklist3.htm")
        With New System.IO.StreamWriter(strTempFile, False)
            .Write(ReportHTML)
            .Close()
        End With
        'objDoc.ImportFromUrl("http://office/car/junklist3.htm", "landscape=true;LeftMargin=20;RightMargin=20;TopMargin=20;BottomMargin=20")
        'objDoc.ImportFromUrl("http://office/car/pullreport.htm", "landscape=false;LeftMargin=20;RightMargin=20;TopMargin=20;BottomMargin=20")
        objDoc.ImportFromUrl("http://office/car/bustreport.htm", "landscape=false;LeftMargin=20;RightMargin=20;TopMargin=20;BottomMargin=20")

        '' Add a page to document.
        'Dim objPage As PdfPage = objDoc.Pages.Add()

        '' Obtain font.
        'Dim objFont As PdfFont = objDoc.Fonts("Helvetica")

        '' Draw text using this parameter string:
        'Dim strParams As String = "x=0; y=650; width=612; alignment=center; size=50"
        'objPage.Canvas.DrawText("Hello World!", strParams, objFont)

        ' Save, generate unique file name to avoid overwriting existing file.
        Dim strFilename As String = objDoc.Save(Server.MapPath("hello.pdf"), False)
        txtResult.Text = "Success! Download your PDF file <A TARGET=_new HREF=" + strFilename + ">here</A>"

    End Sub

    Private Function ReportHTML() As String
        Dim strHTML As String = HTMLBody()
        Dim strDetail As String = ""

        Using objJunkLists As New cardb.Database.StockTicketNumbers("CategorizingStoreNumber = 1 AND (StockTicketNumber IN (SELECT StockTicketNumber FROM dbo.tblJunkItems))", "LocationCode")
            For Each objVehicle As cardb.Database.StockTicketNumber In objJunkLists
                With objVehicle
                    strDetail = strDetail & HTMLDetail()
                    strDetail = strDetail.Replace("{Location}", .LocationCode)
                    strDetail = strDetail.Replace("{Year}", .ModelYear)
                    strDetail = strDetail.Replace("{Model}", .ModelName)
                    strDetail = strDetail.Replace("{Stk#}", .StockTicketNumber)
                    strDetail = strDetail.Replace("{Notes}", Notes(.StockTicketNumber))
                End With
            Next
        End Using
        strHTML = strHTML.Replace("{detail}", strDetail)
        Return strHTML
    End Function

    Private Function Notes(ByVal strStockTicketNumber As String) As String
        Dim strNotes As String = ""
        Using objVehicles As New cardb.Database.VehiclesToJunk("StockTicketNumber = '" & strStockTicketNumber & "'", "InventoryNumber")
            For Each objVehicle As cardb.Database.VehicleToJunk In objVehicles
                Using objBustPulls As New cardb.Database.BustPulls("InventoryId = " & objVehicle.InventoryID)
                    If objBustPulls.Count > 0 Then
                        With objBustPulls
                            If .Item(0).Pull Then
                                strNotes = strNotes & "Pull " & objVehicle.InventoryNumber & "<br />"
                            End If
                        End With
                    End If
                End Using
            Next
        End Using

        Return strNotes
    End Function

    Private Function HTMLBody() As String
        Dim strHTML As String
        strHTML = "<html>" & _
                  "<head>" & _
                  "    <title>Vehicles to Junk</title>" & _
                  "    <link href=""reportstyles.css"" rel=""stylesheet"" type=""text/css"" />" & _
                  "</head>" & _
                  "<body>" & _
                  "<center><b><u><font face=""arial"" size=""2"">Vehicles to Junk Store 1 - " & Date.Now.ToString("MM/dd/yyyy") & "</font></u></b><br /><br />" & _
                  "<table cellpadding=""1"" cellspacing=""0"" width=""1000"" style=""border: thin solid #000000"">" & _
                  "    <tr>" & _
                  "        <td class=""tdhead"" style=""width: 60px;""><font face=""arial"" size=""2"">Location</font></td>" & _
                  "        <td class=""tdhead"" style=""width: 60px;""><font face=""arial"" size=""2"">Year</font></td>" & _
                  "        <td class=""tdhead"" style=""width: 60px;""><font face=""arial"" size=""2"">Model</font></td>" & _
                  "        <td class=""tdhead"" style=""width: 60px;""><font face=""arial"" size=""2"">Stk #</font></td>" & _
                  "        <td class=""tdhead"" style=""width: 130px;""><font face=""arial"" size=""2"">Notes</font></td>" & _
                  "        <td class=""tdhead"" style=""width: 150px;""><font face=""arial"" size=""2"">Deleted?</font></td>" & _
                  "        <td class=""tdhead"" style=""width: 180px;""><font face=""arial"" size=""2"">Replace With?</font></td>" & _
                  "        <td class=""tdhead"" style=""width: 100px;""><font face=""arial"" size=""2"">Stk #</font></td>" & _
                  "        <td class=""tdhead"" style=""width: 100px;border-style: none none solid none;""><font face=""arial"" size=""2"">Location Changed?</font></td>" & _
                  "    </tr>" & _
                  "{detail}" & _
                  "</table></center>" & _
                  "</body>" & _
                  "</html>"

        Return strHTML
    End Function

    Private Function HTMLDetail() As String
        Dim strHTML As String
        strHTML = "    <tr>" & _
                  "        <td><font face=""arial"" size=""2"">{Location}</font></td>" & _
                  "        <td><font face=""arial"" size=""2"">{Year}</font></td>" & _
                  "        <td><font face=""arial"" size=""2"">{Model}</font></td>" & _
                  "        <td><font face=""arial"" size=""2"">{Stk#}</font></td>" & _
                  "        <td><font face=""arial"" size=""2"">{Notes}</font></td>" & _
                  "        <td>&nbsp;</td>" & _
                  "        <td>&nbsp;</td>" & _
                  "        <td>&nbsp;</td>" & _
                  "        <td class=""tdlast"">&nbsp;</td>" & _
                  "    </tr>"

        Return strHTML
    End Function

End Class
