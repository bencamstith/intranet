﻿Imports Persits.PDF
Imports cardb

Partial Class bust
    Inherits System.Web.UI.Page
    Private pintStoreNumber As String = 1

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Request.QueryString("store") IsNot Nothing Then
            pintStoreNumber = Request.QueryString("store").ToString
        End If
        ' Create instance of the PDF manager.
        Dim objPDF As PdfManager = New PdfManager()

        ' Create new document.
        Dim objDoc As PdfDocument = objPDF.CreateDocument()

        ' Specify title and creator for the document.
        objDoc.Title = "Pull Parts Report " & Date.Now.ToString("MM/dd/yyyy")
        objDoc.Creator = ""

        Dim strReportSource As String

        With New System.IO.StreamReader(Server.MapPath("bustreport.htm"))
            strReportSource = .ReadToEnd
            .Close()
        End With

        strReportSource = BuildReport(strReportSource)

        objDoc.ImportFromUrl(strReportSource, "landscape=false;LeftMargin=20;RightMargin=20;TopMargin=20;BottomMargin=20")

        With New System.IO.FileInfo(Server.MapPath("bustreport.pdf"))
            If .Exists Then .Delete()
        End With

        ' Save, generate unique file name to avoid overwriting existing file.
        Dim strFilename As String = objDoc.Save(Server.MapPath("bustreport.pdf"), True)

        objDoc.Close()
        objDoc = Nothing
        objPDF = Nothing

        Response.Redirect(strFilename)

    End Sub

    Private Function BuildReport(ByVal strReportSource As String) As String
        Dim strFullReportHTML As String = ""
        Dim strReportHTML As String = ""
        Dim str100PartsHTML As String = ""
        Dim str150PartsHTML As String = ""
        Dim bolFirstRow As Boolean = True

        ' Remove Header and Footer
        strReportSource = strReportSource.Replace(ReportHeader, "")
        strReportSource = strReportSource.Replace(ReportFooter, "")

        strFullReportHTML = ReportHeader()

        If New System.IO.FileInfo(Server.MapPath("bust100.htm")).Exists Then
            With New System.IO.StreamReader(Server.MapPath("bust100.htm"))
                str100PartsHTML = .ReadToEnd()
            End With
        End If

        If New System.IO.FileInfo(Server.MapPath("bust150.htm")).Exists Then
            With New System.IO.StreamReader(Server.MapPath("bust150.htm"))
                str150PartsHTML = .ReadToEnd()
            End With
        End If

        Using objParts As New Database.VehiclesToJunk("(InventoryID IN (SELECT InventoryId FROM dbo.tblBustPulls WHERE (Bust = 1))) AND (CategorizingStoreNumber = " & pintStoreNumber & ")")
            For Each objPart As Database.VehicleToJunk In objParts
                strReportHTML = strReportSource

                ' Make Page Breaks after first Table
                If Not bolFirstRow Then
                    strReportHTML = strReportHTML.Replace("{style}", "style=""page-break-before: always""")
                Else
                    strReportHTML = strReportHTML.Replace("{style}", "")
                    ' Populate Header Fields on First Row
                    strFullReportHTML.Replace("{StoreNumber}", objPart.CategorizingStoreNumber)
                End If
                If bolFirstRow = True Then bolFirstRow = False

                With objPart
                    strReportHTML = strReportHTML.Replace("{Date}", Date.Now.ToString("MM/dd/yyyy"))
                    strReportHTML = strReportHTML.Replace("{StoreNumber}", .CategorizingStoreNumber)
                    strReportHTML = strReportHTML.Replace("{Location}", .LocationCode)
                    strReportHTML = strReportHTML.Replace("{Year}", .ModelYear)
                    strReportHTML = strReportHTML.Replace("{Model}", .ModelName)
                    strReportHTML = strReportHTML.Replace("{Stock #}", .StockTicketNumber)
                    strReportHTML = strReportHTML.Replace("{InventoryNumber}", .InventoryNumber)
                    strReportHTML = strReportHTML.Replace("{Conditions/Options}", .ConditionsAndOptions)
                    strReportHTML = strReportHTML.Replace("{ConditionCode}", .ConditionCode)
                    strReportHTML = strReportHTML.Replace("{Retail}", FormatCurrency(.RetailPrice, 2))
                    strReportHTML = strReportHTML.Replace("{Wholesale}", FormatCurrency(.WholesalePrice, 2))

                    If .InventoryNumber.StartsWith("100") Then
                        strReportHTML = strReportHTML.Replace("{partnumbers}", str100PartsHTML)
                    Else
                        If .InventoryNumber.StartsWith("150") Then
                            strReportHTML = strReportHTML.Replace("{partnumbers}", str150PartsHTML)
                        End If
                    End If
                End With

                strFullReportHTML = strFullReportHTML & strReportHTML
            Next
        End Using

        strFullReportHTML = strFullReportHTML & ReportFooter()

        Dim strTempFile As String = Server.MapPath("bustreport_source.htm")
        With New System.IO.StreamWriter(strTempFile, False)
            .Write(strFullReportHTML)
            .Close()
        End With

        Return strTempFile
    End Function

    Private Function ReportHeader() As String
        Return "<html><head><title>Bust Report Store {StoreNumber}</title><link href=""reportstyles.css"" rel=""stylesheet"" type=""text/css"" /></head><body>"
    End Function

    Private Function ReportFooter() As String
        Return "</body></html>"
    End Function
End Class
