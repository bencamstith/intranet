﻿
Partial Class setup_accelerators
    Inherits System.Web.UI.Page
    Private pintID As Integer = 0

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            If IsNumeric(Request.QueryString("id")) Then
                pintID = CType(Request.QueryString("id"), Integer)
                Me.txtAcceleratorKey.Text = ""
                Me.txtValue.Text = ""
                Me.txtLow.Text = ""
                Me.txtHigh.Text = ""
            End If
            If IsNumeric(Request.QueryString("ddlAcceleratorTypeKey")) Then
                Me.ddlAcceleratorTypeKey.SelectedValue = Request.QueryString("ddlAcceleratorTypeKey")

                BindTable()
            End If
        End If
    End Sub

    Protected Sub ddlAcceleratorTypeKey_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlAcceleratorTypeKey.SelectedIndexChanged
        pintID = 0
        BindTable()
    End Sub

    Private Sub BindTable()
        Dim tr As TableRow
        Dim td As TableCell
        Dim objHyperLink As HyperLink

        Me.lblErrors.Text = ""
        Using objAccelerators As New cardb.Database.Accelerators("AcceleratorTypeKey = " & Me.ddlAcceleratorTypeKey.SelectedValue)
            If objAccelerators.ValidationErrors.Count > 0 Then
                Me.lblErrors.Text = (New DevNet.Tools.Settings.Connections("CARDB")).ErrorMsg
                For Each objError As DevNet.Tools.Common.ValidationError In objAccelerators.ValidationErrors
                    Me.lblErrors.Text = Me.lblErrors.Text & objError.Message & "<br />"
                Next
            End If
            For Each objAccelerator As cardb.Database.Accelerator In objAccelerators
                tr = New TableRow
                Me.tblAccelerators.Rows.Add(tr)

                td = New TableCell
                tr.Cells.Add(td)
                td.Text = objAccelerator.Value

                td = New TableCell
                tr.Cells.Add(td)
                td.Text = objAccelerator.Low

                td = New TableCell
                tr.Cells.Add(td)
                td.Text = objAccelerator.High

                td = New TableCell
                objHyperLink = New HyperLink
                objHyperLink.ID = "link" & objAccelerator.AcceleratorKey
                objHyperLink.NavigateUrl = "accelerators.aspx?ddlAcceleratorTypeKey=" & Me.ddlAcceleratorTypeKey.SelectedValue & "&id=" & objAccelerator.AcceleratorKey
                objHyperLink.Text = "Edit"
                td.Controls.Add(objHyperLink)
                tr.Cells.Add(td)

                If pintID = objAccelerator.AcceleratorKey Then
                    Me.txtAcceleratorKey.Text = objAccelerator.AcceleratorKey
                    Me.txtValue.Text = objAccelerator.Value
                    Me.txtLow.Text = objAccelerator.Low
                    Me.txtHigh.Text = objAccelerator.High
                End If
            Next
        End Using
        If pintID = 0 Then
            Me.txtAcceleratorKey.Text = ""
            Me.txtValue.Text = ""
            Me.txtLow.Text = ""
            Me.txtHigh.Text = ""
            Me.txtAcceleratorKey.Visible = False
            Me.txtValue.Visible = False
            Me.txtLow.Visible = False
            Me.txtHigh.Visible = False
            Me.btnSubmit.Visible = False
        Else
            Me.txtValue.Visible = True
            Me.txtLow.Visible = True
            Me.txtHigh.Visible = True
            Me.btnSubmit.Visible = True
        End If
    End Sub

    Protected Sub btnSubmit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSubmit.Click
        Me.lblErrors.Text = ""
        Using objAccelerators As New cardb.Database.Accelerators("AcceleratorKey = " & Me.txtAcceleratorKey.Text)
            If objAccelerators.Count > 0 Then
                objAccelerators(0).Value = Me.txtValue.Text
                objAccelerators(0).Low = Me.txtLow.Text
                objAccelerators(0).High = Me.txtHigh.Text

                If objAccelerators.Update Then
                    pintID = 0
                    BindTable()
                Else
                    For Each objError As DevNet.Tools.Common.ValidationError In objAccelerators.ValidationErrors
                        Me.lblErrors.Text = Me.lblErrors.Text & objError.Message & "<br />"
                    Next
                End If
            End If
        End Using
    End Sub
End Class
