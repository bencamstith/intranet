﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="junk3.aspx.vb" Inherits="junk3" %>

<%@ Register src="includes/header.ascx" tagname="header" tagprefix="uc1" %>
<%@ Register src="includes/footer.ascx" tagname="footer" tagprefix="uc2" %>

<%@ Register src="includes/tasks.ascx" tagname="tasks" tagprefix="uc3" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Vehicles to Junk</title>
    <link href="style.css" rel="stylesheet" type="text/css" />
    <link href="table_style.css" rel="stylesheet" type="text/css" />
</head>
<script type="text/javascript">
function displayRow(sid){
	var row = document.getElementById(sid);
	if (row.style.display == '')  row.style.display = 'none';
	else row.style.display = '';
}
</script>
<script type="text/javascript" language="JavaScript">
var win = null;
function newWindow(mypage,myname,w,h,features) {
  var winl = (screen.width-w)/2;
  var wint = (screen.height-h)/2;
  if (winl < 0) winl = 0;
  if (wint < 0) wint = 0;
  var settings = 'height=' + h + ',';
  settings += 'width=' + w + ',';
  settings += 'top=' + wint + ',';
  settings += 'left=' + winl + ',';
  settings += features;
  win = window.open(mypage,myname,settings);
  win.window.focus();
}
</script>
<body>
    <form id="form1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <uc1:header ID="header1" runat="server" />
    <br />
    <div class="top"><a href="###"></a><a href="###"></a>&nbsp;&nbsp;&nbsp;</div>

    <div id="wrapper">
        <div class="left_col">
            <uc3:tasks ID="tasks1" runat="server" />
        </div>
        <div class="right_main">
    		<asp:UpdatePanel ID="UpdatePanel1" runat="server">
    		<ContentTemplate>
            <asp:TextBox ID="txtMessage" runat="server" Visible="false"></asp:TextBox>
    		<h1>Vehicles to Junk Store 3</h1>
			<asp:Label ID="lblErrors" runat="server" Text=""></asp:Label><br />
			<table cellpadding="2" cellspacing="0">
			    <tr>
			        <td>Stock Number</td>
			        <td>:</td>
			        <td><asp:DropDownList ID="ddlStockTicketNumbers" runat="server" AutoPostBack="True">
                        <asp:ListItem Value="0">[Select Stock Number]</asp:ListItem>
                        </asp:DropDownList>
                    </td>
			    </tr>
			</table><br />
			<div class="spacer"></div>
            <asp:Table ID="tableVehicleHeader" runat="server" CellPadding="2" CellSpacing="0" BorderWidth="0">
                <asp:TableRow>
                    <asp:TableCell CssClass="tdsmallhead" Width="100">Stock Number</asp:TableCell>
                    <asp:TableCell CssClass="tdsmallhead" Width="10">:</asp:TableCell>
                    <asp:TableCell CssClass="tdsmallhead" Width="100"><asp:Label ID="lblStockTicketNumber" runat="server" Text=""></asp:Label></asp:TableCell>
                    <asp:TableCell RowSpan="7">&nbsp;</asp:TableCell>
                    <asp:TableCell HorizontalAlign="Center" Width="450" VerticalAlign="Middle" RowSpan="11">
                        <br />
                        <table cellpadding="2" cellspacing="0" border="1">
                            <tr class="trRatingTwo">
                                <td>Only One In Stock</td>
                            </tr>
                            <tr class="trRatingThree">
                                <td>Only One In Stock & Perfect</td>
                            </tr>
                            <tr class="trRatingFour">
                                <td>Nicest One in Stock</td>
                            </tr>
                        </table>
                        <br /><br />
                        <asp:Label ID="lblLocationLabel" runat="server" Text=""></asp:Label><br /><br />
                        <asp:Button ID="btnPrevious" runat="server" Text="Previous" Enabled="false" Width="100" />&nbsp;&nbsp;&nbsp;&nbsp;<asp:Button ID="btnNext" runat="server" Text="Next"  Width="100"/><br />
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell CssClass="tdsmallhead">Year</asp:TableCell>
                    <asp:TableCell CssClass="tdsmallhead">:</asp:TableCell>
                    <asp:TableCell CssClass="tdsmallhead"><asp:Label ID="lblYear" runat="server" Text="" CssClass="tdsmalldata"></asp:Label></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell CssClass="tdsmallhead">Model</asp:TableCell>
                    <asp:TableCell CssClass="tdsmallhead">:</asp:TableCell>
                    <asp:TableCell CssClass="tdsmallhead"><asp:Label ID="lblModel" runat="server" Text="" CssClass="tdsmalldata"></asp:Label></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell CssClass="tdsmallhead">Location</asp:TableCell>
                    <asp:TableCell CssClass="tdsmallhead">:</asp:TableCell>
                    <asp:TableCell CssClass="tdsmallhead"><asp:Label ID="lblLocation" runat="server" Text="" CssClass="tdsmalldata"></asp:Label></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell CssClass="tdsmallhead">Wholesale&nbsp;Total</asp:TableCell>
                    <asp:TableCell CssClass="tdsmallhead">:</asp:TableCell>
                    <asp:TableCell CssClass="tdsmallhead"><asp:Label ID="lblWholesaleTotal" runat="server" Text="" CssClass="tdsmalldata"></asp:Label></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell CssClass="tdsmallhead">Weighted&nbsp;Total</asp:TableCell>
                    <asp:TableCell CssClass="tdsmallhead">:</asp:TableCell>
                    <asp:TableCell CssClass="tdsmallhead"><asp:Label ID="lblWeightedWholesaleTotal" runat="server" Text="" CssClass="tdsmalldata"></asp:Label></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell CssClass="tdsmallhead">Vehicle&nbsp;Cost</asp:TableCell>
                    <asp:TableCell CssClass="tdsmallhead">:</asp:TableCell>
                    <asp:TableCell CssClass="tdsmallhead"><asp:Label ID="lblVehicleCost" runat="server" Text="" CssClass="tdsmalldata"></asp:Label></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell CssClass="tdsmallhead">Vehicle&nbsp;Age</asp:TableCell>
                    <asp:TableCell CssClass="tdsmallhead">:</asp:TableCell>
                    <asp:TableCell CssClass="tdsmallhead"><asp:Label ID="lblVehicleAge" runat="server" Text="" CssClass="tdsmalldata"></asp:Label></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell CssClass="tdsmallhead">Income</asp:TableCell>
                    <asp:TableCell CssClass="tdsmallhead">:</asp:TableCell>
                    <asp:TableCell CssClass="tdsmallhead"><asp:Label ID="lblVehicleIncome" runat="server" Text="" CssClass="tdsmalldata"></asp:Label></asp:TableCell>
                    <asp:TableCell CssClass="tdsmallhead" RowSpan="2"><asp:Label ID="lblROI" runat="server" Text=""></asp:Label></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell CssClass="tdsmallhead">Profit</asp:TableCell>
                    <asp:TableCell CssClass="tdsmallhead">:</asp:TableCell>
                    <asp:TableCell CssClass="tdsmallhead"><asp:Label ID="lblVehicleProfit" runat="server" Text="" CssClass="tdsmalldata"></asp:Label></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell CssClass="tdsmallhead">Junk Vehicle</asp:TableCell>
                    <asp:TableCell CssClass="tdsmallhead">:</asp:TableCell>
                    <asp:TableCell CssClass="tdsmallhead"><asp:CheckBox ID="chkJunkVehicle" runat="server" /></asp:TableCell>
                    <asp:TableCell CssClass="tdsmallhead">&nbsp;</asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell ColumnSpan="5" VerticalAlign="Top">
                        <asp:Table ID="tableVehicleToJunk" runat="server" CellPadding="2" CellSpacing="0" BorderWidth="1" Width="700">
                            <asp:TableRow BorderStyle="Solid" BorderWidth="1">
                                <asp:TableCell CssClass="tdsmallhead" Width="15px">#</asp:TableCell>
                                <asp:TableCell CssClass="tdsmallhead" Width="50px">Bust</asp:TableCell>
                                <asp:TableCell CssClass="tdsmallhead" Width="50px">Pull</asp:TableCell>
                                <asp:TableCell CssClass="tdsmallhead" Width="50px">Inv. #</asp:TableCell>
                                <asp:TableCell CssClass="tdsmallhead" HorizontalAlign="Center" Width="25px">QOH</asp:TableCell>
                                <asp:TableCell CssClass="tdsmallhead" HorizontalAlign="Center" Width="25px">Activity</asp:TableCell>
                                <asp:TableCell CssClass="tdsmallhead" HorizontalAlign="Center" Width="25px">Sales</asp:TableCell>
                                <asp:TableCell CssClass="tdsmallhead" HorizontalAlign="Center" Width="25px">Age</asp:TableCell>
                                <asp:TableCell CssClass="tdsmallhead" Width="50px">Conditions/Options</asp:TableCell>
                                <asp:TableCell CssClass="tdsmallhead" HorizontalAlign="Center" Width="25px">Dmg Code</asp:TableCell>
                                <asp:TableCell CssClass="tdsmallhead" HorizontalAlign="Center" Width="75px">Retail</asp:TableCell>
                                <asp:TableCell CssClass="tdsmallhead" HorizontalAlign="Center" Width="75px">Wholesale</asp:TableCell>
                                <asp:TableCell CssClass="tdsmallhead" HorizontalAlign="Center" Width="75px">Weighted</asp:TableCell>
                                <asp:TableCell CssClass="tdsmallhead" HorizontalAlign="Center" Width="75px">Cost</asp:TableCell>
                            </asp:TableRow>
                        </asp:Table>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell CssClass="tdsmallhead">&nbsp;</asp:TableCell>
                    <asp:TableCell CssClass="tdsmallhead">&nbsp;</asp:TableCell>
                    <asp:TableCell CssClass="tdsmallhead">&nbsp;</asp:TableCell>
                    <asp:TableCell CssClass="tdsmallhead">&nbsp;</asp:TableCell>
                    <asp:TableCell HorizontalAlign="center">
                        <asp:Button ID="btnPrevious2" runat="server" Text="Previous" Enabled="false" Width="100" />&nbsp;&nbsp;&nbsp;&nbsp;<asp:Button ID="btnNext2" runat="server" Text="Next"  Width="100"/><br />                    
                    </asp:TableCell>
                </asp:TableRow>
            </asp:Table>
            </ContentTemplate>
            </asp:UpdatePanel>
        </div>
    </div>
    </form>
</body>
</html>
