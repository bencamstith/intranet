﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="junk_report.aspx.vb" Inherits="junk_report" %>

<%@ Register src="includes/header.ascx" tagname="header" tagprefix="uc1" %>
<%@ Register src="includes/footer.ascx" tagname="footer" tagprefix="uc2" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Junk List</title>
    <link href="style.css" rel="stylesheet" type="text/css" />
    <link href="table_style.css" rel="stylesheet" type="text/css" />
</head>
<body>
    <form id="form1" runat="server">
    <uc1:header ID="header1" runat="server" />
 <br />
    <div class="top"><a href="###"></a><a href="###"></a>&nbsp;&nbsp;&nbsp;</div>
    <asp:DropDownList ID="DropDownList1" runat="server" CssClass="noshow" AutoPostBack="true">
    </asp:DropDownList>
    <div id="wrapper">
        <div class="right_main">
    		<h1>Vehicles to Junk Store 1<asp:Label ID="lblReportDate" runat="server" Text="Label"></asp:Label></h1>
			<asp:Label ID="lblErrors" runat="server" Text=""></asp:Label><br />
            <asp:Table ID="tableVehicles" runat="server" Width="904px" BorderWidth="1" CellPadding="2" CellSpacing="0">
                <asp:TableRow BorderWidth="1">
                    <asp:TableCell CssClass="tdsmallhead" BorderWidth="1" Width="60">Location</asp:TableCell>
                    <asp:TableCell CssClass="tdsmallhead" BorderWidth="1" Width="40">Year</asp:TableCell>
                    <asp:TableCell CssClass="tdsmallhead" BorderWidth="1" Width="75">Model</asp:TableCell>
                    <asp:TableCell CssClass="tdsmallhead" BorderWidth="1">Stk #</asp:TableCell>
                    <asp:TableCell CssClass="tdsmallhead" BorderWidth="1">Notes</asp:TableCell>
                    <asp:TableCell CssClass="tdsmallhead" BorderWidth="1">Deleted?</asp:TableCell>
                    <asp:TableCell CssClass="tdsmallhead" BorderWidth="1" Width="150">Replaced With?</asp:TableCell>
                    <asp:TableCell CssClass="tdsmallhead" BorderWidth="1" Width="150">Stk #</asp:TableCell>
                    <asp:TableCell CssClass="tdsmallhead" BorderWidth="1">Location&nbsp;Changed?</asp:TableCell>
                </asp:TableRow>
            </asp:Table>
        </div>
    </div>    
    </form>
</body>
</html>
