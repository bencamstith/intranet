﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="vehiclestojunk.aspx.vb" Inherits="vehiclestojunk" %>
<%@ Register src="includes/header.ascx" tagname="header" tagprefix="uc1" %>
<%@ Register src="includes/footer.ascx" tagname="footer" tagprefix="uc2" %>
<%@ Register src="includes/tasks.ascx" tagname="tasks" tagprefix="uc3" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Vehicles to Junk</title>
    <link href="style.css" rel="stylesheet" type="text/css" />
    <link href="table_style.css" rel="stylesheet" type="text/css" />
</head>
<body>
    <form id="form1" runat="server">
    <uc1:header ID="header1" runat="server" />
    <br />
    <div class="top"><a href="###"></a> > <a href="###"></a>&nbsp;&nbsp;&nbsp;</div>
    
    <div id="reportwrapper">
        <div class="right_main">
        <asp:TextBox ID="txtDisplayCount" runat="server" Visible="false"></asp:TextBox>
        <asp:Label ID="txtNotes" runat="server" Text="" Visible="false"></asp:Label>
    		<h1>Vehicles to Junk Store 1</h1>
            <asp:Label ID="lblErrors" runat="server" Text="Label"></asp:Label><br />
			<asp:Table ID="tblJunk" runat="server" CellPadding="1" CellSpacing="0" 
                BorderWidth="1" Width="900px">
                <asp:TableHeaderRow>
                    <asp:TableCell CssClass="reportHeader" HorizontalAlign="Center">Bust&nbsp;Up</asp:TableCell>
                    <asp:TableCell CssClass="reportHeader" HorizontalAlign="Center">Pull</asp:TableCell>
                    <asp:TableCell CssClass="reportHeader" Width="85px">Inventory #</asp:TableCell>
                    <asp:TableCell CssClass="reportHeader" Width="40px">Year</asp:TableCell>
                    <asp:TableCell CssClass="reportHeader" Width="50px">Model</asp:TableCell>
                    <asp:TableCell CssClass="reportHeader" Width="50px">Stk#</asp:TableCell>
                    <asp:TableCell CssClass="reportHeader" Width="50px">Location</asp:TableCell>
                    <asp:TableCell CssClass="reportHeader" Width="50px">QOH</asp:TableCell>
                    <asp:TableCell CssClass="reportHeader" Width="50px">Activity</asp:TableCell>
                    <asp:TableCell CssClass="reportHeader" Width="50px">Sales</asp:TableCell>
                    <asp:TableCell CssClass="reportHeader" Width="50">Age</asp:TableCell>
                    <asp:TableCell CssClass="reportHeader" Width="50px">Conditions/Options</asp:TableCell>
                    <asp:TableCell CssClass="reportHeader" Width="25px">Dmg Code</asp:TableCell>
                    <asp:TableCell CssClass="reportHeader">Retail</asp:TableCell>
                    <asp:TableCell CssClass="reportHeader">Wholesale</asp:TableCell>
                    <asp:TableCell CssClass="reportHeader">WW</asp:TableCell>
                </asp:TableHeaderRow>
            </asp:Table>
            <asp:Button ID="btnNext" runat="server" Text="Next" />
					<br /><br />
        </div>
    </div>
    </form>
</body>
</html>
