﻿Imports Persits.PDF

Partial Class junklist
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim objPDF As PdfManager = New PdfManager()

        ' Create new document.
        Dim objDoc As PdfDocument = objPDF.CreateDocument()

        ' Specify title and creator for the document.
        objDoc.Title = "Vehicles to Junk Report - " & Date.Now.ToString("MM/dd/yyyy")
        objDoc.Creator = "John Smith"
        objDoc.ImportFromUrl("http://localhost/samp.htm")

        '' Add a page to document.
        'Dim objPage As PdfPage = objDoc.Pages.Add()

        '' Obtain font.
        'Dim objFont As PdfFont = objDoc.Fonts("Helvetica")

        '' Draw text using this parameter string:
        'Dim strParams As String = "x=0; y=650; width=612; alignment=center; size=50"
        'objPage.Canvas.DrawText("Hello World!", strParams, objFont)

        ' Save, generate unique file name to avoid overwriting existing file.
        Dim strFilename As String = objDoc.Save(Server.MapPath("hello.pdf"), False)
        'txtResult.Text = "Success! Download your PDF file <A TARGET=_new HREF=" + strFilename + ">here</A>"
    End Sub


End Class
