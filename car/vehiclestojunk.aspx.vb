﻿
Partial Class vehiclestojunk
    Inherits System.Web.UI.Page

    Private pintPage As Integer = 1
    Private pintPageSize As Integer = 500
    Private pintDisplayedTotal As Integer

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            BindJunk()
            pintDisplayedTotal = 0
            'Me.txtDisplayCount.Text = 0
        End If
    End Sub

    Private Sub BindJunk()
        Dim tr As TableRow
        Dim td As TableCell
        Dim chkBox As CheckBox
        Dim intCount As Integer = 0
        Dim intPreviousDisplayTotal As Integer

        Me.lblErrors.Text = ""
        Dim strStockTicketNumber As String = ""
        Dim objPreviousJunk As pldb.Database.VehiclesToJunk
        Dim pbolStockTicketNumberChanged As Boolean = False

        If IsNumeric(Me.txtDisplayCount.Text) Then pintDisplayedTotal = CType(Me.txtDisplayCount.Text, Integer)
        pintDisplayedTotal = 56
        intPreviousDisplayTotal = pintDisplayedTotal

        Dim strWhere As String
        If pintDisplayedTotal > 0 Then
            strWhere = "CategorizingStoreNumber = 1 AND NOT  InventoryId In (Select TOP " & pintDisplayedTotal & " InventoryId from Inventory INV Where CategorizingStoreNumber = 1 Order By StockTicketNumber, InventoryNumber)"
            '                                           (NOT (InventoryId IN (SELECT TOP 5 EmployeeKey FROM tblEmployees)))
        Else
            strWhere = "CategorizingStoreNumber = 1"
        End If
        Me.txtNotes.Text = strWhere
        Using objJunks As New pldb.Database.VehiclesToJunks(strWhere, "StockTicketNumber, InventoryNumber", "", pintPageSize)
            If objJunks.ValidationErrors.Count > 0 Then
                Me.lblErrors.Text = (New DevNet.Tools.Settings.Connections("PLDB")).ErrorMsg
                For Each objError As DevNet.Tools.Common.ValidationError In objJunks.ValidationErrors
                    Me.lblErrors.Text = Me.lblErrors.Text & objError.Message & "<br />"
                Next
            End If
            If objJunks.Count > 0 Then
                strStockTicketNumber = objJunks(0).StockTicketNumber
                objPreviousJunk = objJunks(0)
            End If

            For Each objJunk As pldb.Database.VehiclesToJunk In objJunks
                If strStockTicketNumber <> objJunk.StockTicketNumber Then
                    pbolStockTicketNumberChanged = True
                    tr = New TableRow
                    Me.tblJunk.Rows.Add(tr)
                    td = New TableCell
                    tr.Cells.Add(td)
                    td.ColumnSpan = 16
                    td.Text = "<div class=""spacer""></div>"

                    tr = New TableRow
                    Me.tblJunk.Rows.Add(tr)

                    td = New TableCell
                    tr.Cells.Add(td)
                    td.ColumnSpan = 2
                    td.HorizontalAlign = HorizontalAlign.Center
                    chkBox = New CheckBox
                    chkBox.ID = "chkJunk_" & objJunk.StockTicketNumber
                    td.Controls.Add(chkBox)

                    td = New TableCell
                    tr.Cells.Add(td)
                    td.ColumnSpan = 3
                    td.CssClass = "reportData"
                    td.Text = "Junk"

                    td = New TableCell
                    tr.Cells.Add(td)
                    td.CssClass = "reportData"
                    td.Text = objPreviousJunk.StockTicketNumber

                    td = New TableCell
                    tr.Cells.Add(td)
                    td.ColumnSpan = 8
                    td.Text = "&nbsp;"

                    td = New TableCell
                    tr.Cells.Add(td)
                    td.CssClass = "reportData"
                    td.Text = FormatCurrency(objPreviousJunk.WholesaleTotal, 2)

                    td = New TableCell
                    tr.Cells.Add(td)
                    td.ColumnSpan = 3
                    td.Text = "&nbsp;"

                    tr = New TableRow
                    Me.tblJunk.Rows.Add(tr)
                    td = New TableCell
                    tr.Cells.Add(td)
                    td.ColumnSpan = 16
                    td.Text = "<div class=""spacer""></div>"
                End If

                If pbolStockTicketNumberChanged Then
                    If objJunk.ItemCount > (pintPageSize - intCount) Then
                        pintDisplayedTotal = pintDisplayedTotal + intCount
                        Exit For
                    End If
                    pbolStockTicketNumberChanged = False
                End If
                

                tr = New TableRow
                Me.tblJunk.Rows.Add(tr)
                tr.Attributes("onmouseout") = "this.className='trdataOut'"
                tr.Attributes("onmouseover") = "this.className='trdataOver'"

                If objJunk.CanBustUp Then
                    td = New TableCell
                    tr.Cells.Add(td)
                    chkBox = New CheckBox
                    chkBox.ID = "chkBustUp_" & objJunk.InventoryID
                    td.Controls.Add(chkBox)
                Else
                    AddTD(tr, "&nbsp;")
                End If

                If objJunk.CanPull Then
                    td = New TableCell
                    tr.Cells.Add(td)
                    chkBox = New CheckBox
                    chkBox.ID = "chkCanPull_" & objJunk.InventoryID
                    td.Controls.Add(chkBox)
                Else
                    AddTD(tr, "&nbsp;")
                End If

                AddTD(tr, objJunk.InventoryNumber)
                AddTD(tr, objJunk.ModelYear)
                AddTD(tr, objJunk.ModelName)
                AddTD(tr, objJunk.StockTicketNumber)
                AddTD(tr, objJunk.LocationCode).HorizontalAlign = HorizontalAlign.Center
                AddTD(tr, objJunk.QOH).HorizontalAlign = HorizontalAlign.Center
                AddTD(tr, objJunk.Activity).HorizontalAlign = HorizontalAlign.Center
                AddTD(tr, objJunk.Sales).HorizontalAlign = HorizontalAlign.Center
                AddTD(tr, "&nbsp;")
                AddTD(tr, objJunk.ConditionsAndOptions)
                AddTD(tr, objJunk.ConditionCode).HorizontalAlign = HorizontalAlign.Center
                AddTD(tr, FormatCurrency(objJunk.RetailPrice, 2)).HorizontalAlign = HorizontalAlign.Right
                AddTD(tr, FormatCurrency(objJunk.WholesalePrice, 2)).HorizontalAlign = HorizontalAlign.Right
                AddTD(tr, FormatCurrency(objJunk.WW, 2)).HorizontalAlign = HorizontalAlign.Right

                strStockTicketNumber = objJunk.StockTicketNumber
                objPreviousJunk = objJunk
                intCount = intCount + 1
            Next
            Me.txtNotes.Text = Me.txtNotes.Text & "<br />" & pintDisplayedTotal
            Me.txtDisplayCount.Text = pintDisplayedTotal
        End Using
    End Sub

    Private Function AddTD(ByVal tr As TableRow, ByVal strText As String) As TableCell
        Dim td As New TableCell
        tr.Cells.Add(td)
        td.CssClass = "reportData"
        td.Text = strText
        Return td
    End Function

    Protected Sub btnNext_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnNext.Click
        BindJunk()
    End Sub
End Class
