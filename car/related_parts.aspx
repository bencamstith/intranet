﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="related_parts.aspx.vb" Inherits="related_parts" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Other Inventory Parts</title>
    <link href="table_style.css" rel="stylesheet" type="text/css" />
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <table cellpadding="2" cellspacing="0" border="0">
            <tr>
                <td class="tdsmallhead">Inventory #</td>
                <td class="tdsmallhead">:</td>
                <td><asp:Label ID="lblInventoryNumber" runat="server" Text="Label" CssClass="tdsmallhead"></asp:Label></td>
            </tr>
        </table>
        <asp:Table ID="tbl" runat="server" CellPadding="2" CellSpacing="0">
        </asp:Table>
    </div>
    </form>
</body>
</html>
