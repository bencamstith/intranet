﻿Imports System.Web
Imports System.Web.Services
Imports System.Web.Services.Protocols

' To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line.
' <System.Web.Script.Services.ScriptService()> _
<WebService(Namespace:="http://tempuri.org/")> _
<WebServiceBinding(ConformsTo:=WsiProfiles.BasicProfile1_1)> _
<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Public Class mmdcparttypes
    Inherits System.Web.Services.WebService

    Public Structure sMMDCPartType
        Public MMDCPartTypeKey As Integer
        Public PartType As String
        Public DateCreated As String
        Public _Success As Boolean
        Public ActiveUserKey As Integer
        Public _Message As String
        Public _Message2 As String
    End Structure

    <WebMethod()> _
    Public Function MMDCPartTypes(ByVal strWhere As String, ByVal strOrderBy As String, ByVal strJoin As String, ByVal intTop As Integer) As System.Data.DataTable
        Dim objMMDCPartTypes As cardb.Database.MMDCPartTypes
        objMMDCPartTypes = New cardb.Database.MMDCPartTypes(strWhere, strOrderBy, strJoin, intTop)
        Return objMMDCPartTypes
    End Function

    <WebMethod()> _
    Public Function Query(ByVal objMMDCPartType As sMMDCPartType) As sMMDCPartType
        objMMDCPartType._Success = False

        Using objMMDCPartTypes As New cardb.Database.MMDCPartTypes("MMDCPartTypeKey = " & objMMDCPartType.MMDCPartTypeKey)
            If objMMDCPartTypes.Count > 0 Then
                With objMMDCPartTypes(0)
                    objMMDCPartType.MMDCPartTypeKey = IIf(IsDBNull(.MMDCPartTypeKey), 0, .MMDCPartTypeKey)
                    objMMDCPartType.PartType = IIf(IsDBNull(.PartType), "", .PartType)
                    objMMDCPartType.DateCreated = IIf(IsDBNull(.DateCreated), "", .DateCreated)

                    objMMDCPartType._Success = True
                End With
            Else
                objMMDCPartType._Message = "MMDCPartType doesn't exist."
            End If

            objMMDCPartType._Message = ""
            objMMDCPartType._Message2 = ""
            For Each objErr As DevNet.Tools.Common.ValidationError In objMMDCPartTypes.ValidationErrors
                If objMMDCPartType._Message.Length > 0 Then objMMDCPartType._Message = objMMDCPartType._Message & "~"
                objMMDCPartType._Message = objMMDCPartType._Message & objErr.Message
            Next
        End Using

        Return objMMDCPartType
    End Function

    <WebMethod()> _
    Public Function Add(ByVal objMMDCPartType As sMMDCPartType) As sMMDCPartType
        objMMDCPartType._Success = False

        Using objMMDCPartTypes As New cardb.Database.MMDCPartTypes("", , , 0)
            With objMMDCPartTypes.AddMMDCPartType(objMMDCPartTypes.NewMMDCPartType)
                .PartType = objMMDCPartType.PartType
                .DateCreated = objMMDCPartType.DateCreated
                .BL.ActiveUserKey = objMMDCPartType.ActiveUserKey

                If objMMDCPartTypes.Update Then
                    objMMDCPartType._Success = True
                    objMMDCPartType.MMDCPartTypeKey = .MMDCPartTypeKey
                End If
            End With

            objMMDCPartType._Message = ""
            objMMDCPartType._Message2 = ""
            For Each objErr As DevNet.Tools.Common.ValidationError In objMMDCPartTypes.ValidationErrors
                If objMMDCPartType._Message.Length > 0 Then objMMDCPartType._Message = objMMDCPartType._Message & "~"
                objMMDCPartType._Message = objMMDCPartType._Message & objErr.Message
            Next
        End Using

        Return objMMDCPartType
    End Function

    <WebMethod()> _
    Public Function Edit(ByVal objMMDCPartType As sMMDCPartType) As sMMDCPartType
        objMMDCPartType._Success = False

        Using objMMDCPartTypes As New cardb.Database.MMDCPartTypes("MMDCPartTypeKey = " & objMMDCPartType.MMDCPartTypeKey)
            If objMMDCPartTypes.Count > 0 Then
                With objMMDCPartTypes(0)
                    .PartType = objMMDCPartType.PartType
                    .DateCreated = objMMDCPartType.DateCreated
                    .BL.ActiveUserKey = objMMDCPartType.ActiveUserKey

                    If objMMDCPartTypes.Update Then
                        objMMDCPartType._Success = True
                        objMMDCPartType.MMDCPartTypeKey = .MMDCPartTypeKey
                    End If
                End With
            Else
                objMMDCPartType._Message = "MMDCPartType doesn't exist."
            End If

            objMMDCPartType._Message = ""
            objMMDCPartType._Message2 = ""
            For Each objErr As DevNet.Tools.Common.ValidationError In objMMDCPartTypes.ValidationErrors
                If objMMDCPartType._Message.Length > 0 Then objMMDCPartType._Message = objMMDCPartType._Message & "~"
                objMMDCPartType._Message = objMMDCPartType._Message & objErr.Message
            Next
        End Using

        Return objMMDCPartType
    End Function

    <WebMethod()> _
    Public Function Delete(ByVal objMMDCPartType As sMMDCPartType) As sMMDCPartType
        objMMDCPartType._Success = False

        Using objMMDCPartTypes As New cardb.Database.MMDCPartTypes("MMDCPartTypeKey = " & objMMDCPartType.MMDCPartTypeKey)
            If objMMDCPartTypes.Count > 0 Then
                objMMDCPartTypes(0).BL.ActiveUserKey = objMMDCPartType.ActiveUserKey
                objMMDCPartTypes(0).Delete()

                If objMMDCPartTypes.Update Then
                    objMMDCPartType._Success = True
                End If
            Else
                objMMDCPartType._Message = "MMDCPartType doesn't exist."
            End If

            objMMDCPartType._Message = ""
            objMMDCPartType._Message2 = ""
            For Each objErr As DevNet.Tools.Common.ValidationError In objMMDCPartTypes.ValidationErrors
                If objMMDCPartType._Message.Length > 0 Then objMMDCPartType._Message = objMMDCPartType._Message & "~"
                objMMDCPartType._Message = objMMDCPartType._Message & objErr.Message
            Next
        End Using

        Return objMMDCPartType
    End Function
End Class
