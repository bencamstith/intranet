﻿Imports System.Web
Imports System.Web.Services
Imports System.Web.Services.Protocols

' To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line.
' <System.Web.Script.Services.ScriptService()> _
<WebService(Namespace:="http://tempuri.org/")> _
<WebServiceBinding(ConformsTo:=WsiProfiles.BasicProfile1_1)> _
<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Public Class mmdcdgroups
    Inherits System.Web.Services.WebService

    Public Structure sMMDCDGroup
        Public MMDCDGroupKey As Integer
        Public YearRange As String
        Public Model As String
        Public MmdCd As String
        Public MmdCategory As String
        Public CarlineNum As String
        Public AvgCost As Double
        Public AvgIncome As Double
        Public AvgProfit As Double
        Public AvgROI As Double
        Public DateCreated As String
        Public _Success As Boolean
        Public ActiveUserKey As Integer
        Public _Message As String
        Public _Message2 As String
    End Structure

    <WebMethod()> _
    Public Function MMDCDGroups(ByVal strWhere As String, ByVal strOrderBy As String, ByVal strJoin As String, ByVal intTop As Integer) As System.Data.DataTable
        Dim objMMDCDGroups As cardb.Database.MMDCDGroups
        objMMDCDGroups = New cardb.Database.MMDCDGroups(strWhere, strOrderBy, strJoin, intTop)
        Return objMMDCDGroups
    End Function

    <WebMethod()> _
    Public Function Query(ByVal objMMDCDGroup As sMMDCDGroup) As sMMDCDGroup
        objMMDCDGroup._Success = False

        Using objMMDCDGroups As New cardb.Database.MMDCDGroups("MMDCDGroupKey = " & objMMDCDGroup.MMDCDGroupKey)
            If objMMDCDGroups.Count > 0 Then
                With objMMDCDGroups(0)
                    objMMDCDGroup.MMDCDGroupKey = IIf(IsDBNull(.MMDCDGroupKey), 0, .MMDCDGroupKey)
                    objMMDCDGroup.YearRange = IIf(IsDBNull(.YearRange), "", .YearRange)
                    objMMDCDGroup.Model = IIf(IsDBNull(.Model), "", .Model)
                    objMMDCDGroup.MmdCd = IIf(IsDBNull(.MmdCd), "", .MmdCd)
                    objMMDCDGroup.MmdCategory = IIf(IsDBNull(.MmdCategory), "", .MmdCategory)
                    objMMDCDGroup.CarlineNum = IIf(IsDBNull(.CarlineNum), "", .CarlineNum)
                    objMMDCDGroup.AvgCost = IIf(IsDBNull(.AvgCost), 0, .AvgCost)
                    objMMDCDGroup.AvgIncome = IIf(IsDBNull(.AvgIncome), 0, .AvgIncome)
                    objMMDCDGroup.AvgProfit = IIf(IsDBNull(.AvgProfit), 0, .AvgProfit)
                    objMMDCDGroup.AvgROI = IIf(IsDBNull(.AvgROI), 0, .AvgROI)
                    objMMDCDGroup.DateCreated = IIf(IsDBNull(.DateCreated), "", .DateCreated)

                    objMMDCDGroup._Success = True
                End With
            Else
                objMMDCDGroup._Message = "MMDCDGroup doesn't exist."
            End If

            objMMDCDGroup._Message = ""
            objMMDCDGroup._Message2 = ""
            For Each objErr As DevNet.Tools.Common.ValidationError In objMMDCDGroups.ValidationErrors
                If objMMDCDGroup._Message.Length > 0 Then objMMDCDGroup._Message = objMMDCDGroup._Message & "~"
                objMMDCDGroup._Message = objMMDCDGroup._Message & objErr.Message
            Next
        End Using

        Return objMMDCDGroup
    End Function

    <WebMethod()> _
    Public Function Add(ByVal objMMDCDGroup As sMMDCDGroup) As sMMDCDGroup
        objMMDCDGroup._Success = False

        Using objMMDCDGroups As New cardb.Database.MMDCDGroups("", , , 0)
            With objMMDCDGroups.AddMMDCDGroup(objMMDCDGroups.NewMMDCDGroup)
                .YearRange = objMMDCDGroup.YearRange
                .Model = objMMDCDGroup.Model
                .MmdCd = objMMDCDGroup.MmdCd
                .MmdCategory = objMMDCDGroup.MmdCategory
                .CarlineNum = objMMDCDGroup.CarlineNum
                .AvgCost = objMMDCDGroup.AvgCost
                .AvgIncome = objMMDCDGroup.AvgIncome
                .AvgProfit = objMMDCDGroup.AvgProfit
                .AvgROI = objMMDCDGroup.AvgROI
                .DateCreated = objMMDCDGroup.DateCreated
                .BL.ActiveUserKey = objMMDCDGroup.ActiveUserKey

                If objMMDCDGroups.Update Then
                    objMMDCDGroup._Success = True
                    objMMDCDGroup.MMDCDGroupKey = .MMDCDGroupKey
                End If
            End With

            objMMDCDGroup._Message = ""
            objMMDCDGroup._Message2 = ""
            For Each objErr As DevNet.Tools.Common.ValidationError In objMMDCDGroups.ValidationErrors
                If objMMDCDGroup._Message.Length > 0 Then objMMDCDGroup._Message = objMMDCDGroup._Message & "~"
                objMMDCDGroup._Message = objMMDCDGroup._Message & objErr.Message
            Next
        End Using

        Return objMMDCDGroup
    End Function

    <WebMethod()> _
    Public Function Edit(ByVal objMMDCDGroup As sMMDCDGroup) As sMMDCDGroup
        objMMDCDGroup._Success = False

        Using objMMDCDGroups As New cardb.Database.MMDCDGroups("MMDCDGroupKey = " & objMMDCDGroup.MMDCDGroupKey)
            If objMMDCDGroups.Count > 0 Then
                With objMMDCDGroups(0)
                    .YearRange = objMMDCDGroup.YearRange
                    .Model = objMMDCDGroup.Model
                    .MmdCd = objMMDCDGroup.MmdCd
                    .MmdCategory = objMMDCDGroup.MmdCategory
                    .CarlineNum = objMMDCDGroup.CarlineNum
                    .AvgCost = objMMDCDGroup.AvgCost
                    .AvgIncome = objMMDCDGroup.AvgIncome
                    .AvgProfit = objMMDCDGroup.AvgProfit
                    .AvgROI = objMMDCDGroup.AvgROI
                    .DateCreated = objMMDCDGroup.DateCreated
                    .BL.ActiveUserKey = objMMDCDGroup.ActiveUserKey

                    If objMMDCDGroups.Update Then
                        objMMDCDGroup._Success = True
                        objMMDCDGroup.MMDCDGroupKey = .MMDCDGroupKey
                    End If
                End With
            Else
                objMMDCDGroup._Message = "MMDCDGroup doesn't exist."
            End If

            objMMDCDGroup._Message = ""
            objMMDCDGroup._Message2 = ""
            For Each objErr As DevNet.Tools.Common.ValidationError In objMMDCDGroups.ValidationErrors
                If objMMDCDGroup._Message.Length > 0 Then objMMDCDGroup._Message = objMMDCDGroup._Message & "~"
                objMMDCDGroup._Message = objMMDCDGroup._Message & objErr.Message
            Next
        End Using

        Return objMMDCDGroup
    End Function

    <WebMethod()> _
    Public Function Delete(ByVal objMMDCDGroup As sMMDCDGroup) As sMMDCDGroup
        objMMDCDGroup._Success = False

        Using objMMDCDGroups As New cardb.Database.MMDCDGroups("MMDCDGroupKey = " & objMMDCDGroup.MMDCDGroupKey)
            If objMMDCDGroups.Count > 0 Then
                objMMDCDGroups(0).BL.ActiveUserKey = objMMDCDGroup.ActiveUserKey
                objMMDCDGroups(0).Delete()

                If objMMDCDGroups.Update Then
                    objMMDCDGroup._Success = True
                End If
            Else
                objMMDCDGroup._Message = "MMDCDGroup doesn't exist."
            End If

            objMMDCDGroup._Message = ""
            objMMDCDGroup._Message2 = ""
            For Each objErr As DevNet.Tools.Common.ValidationError In objMMDCDGroups.ValidationErrors
                If objMMDCDGroup._Message.Length > 0 Then objMMDCDGroup._Message = objMMDCDGroup._Message & "~"
                objMMDCDGroup._Message = objMMDCDGroup._Message & objErr.Message
            Next
        End Using

        Return objMMDCDGroup
    End Function
End Class
