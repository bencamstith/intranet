﻿Imports System.Web
Imports System.Web.Services
Imports System.Web.Services.Protocols

' To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line.
' <System.Web.Script.Services.ScriptService()> _
<WebService(Namespace:="http://tempuri.org/")> _
<WebServiceBinding(ConformsTo:=WsiProfiles.BasicProfile1_1)> _
<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Public Class webaccounts
    Inherits System.Web.Services.WebService

    Public Structure sWebAccount
        Public WebAccountKey As Integer
        Public Email As String
        Public Password As String
        Public EmailInvoices As Boolean
        Public EmailStatements As Boolean
        Public DateCreated As String
        Public ExpiredPassword As Boolean
        Public AccountStatus As Integer
        Public WebFolder As String
        Public _Success As Boolean
        Public ActiveUserKey As Integer
        Public _Message As String
        Public _Message2 As String
    End Structure

    <WebMethod()> _
    Public Function WebAccounts(ByVal strWhere As String, ByVal strOrderBy As String, ByVal strJoin As String, ByVal intTop As Integer) As System.Data.DataTable
        Dim objWebAccounts As cardb.Database.WebAccounts
        objWebAccounts = New cardb.Database.WebAccounts(strWhere, strOrderBy, strJoin, intTop)
        Return objWebAccounts
    End Function

    <WebMethod()> _
    Public Function Query(ByVal objWebAccount As sWebAccount) As sWebAccount
        objWebAccount._Success = False

        Using objWebAccounts As New cardb.Database.WebAccounts("WebAccountKey = " & objWebAccount.WebAccountKey)
            If objWebAccounts.Count > 0 Then
                With objWebAccounts(0)
                    objWebAccount.WebAccountKey = IIf(IsDBNull(.WebAccountKey), 0, .WebAccountKey)
                    objWebAccount.Email = IIf(IsDBNull(.Email), "", .Email)
                    objWebAccount.Password = IIf(IsDBNull(.Password), "", .Password)
                    '  objWebAccount.EmailInvoices = IIf(IsDBNull(.EmailInvoices), False, .EmailInvoices)
                    objWebAccount.EmailStatements = IIf(IsDBNull(.EmailStatements), False, .EmailStatements)
                    objWebAccount.DateCreated = IIf(IsDBNull(.DateCreated), "", .DateCreated)
                    objWebAccount.ExpiredPassword = IIf(IsDBNull(.ExpiredPassword), False, .ExpiredPassword)
                    objWebAccount.AccountStatus = IIf(IsDBNull(.AccountStatus), 0, .AccountStatus)
                    objWebAccount.WebFolder = IIf(IsDBNull(.WebFolder), "", .WebFolder)

                    objWebAccount._Success = True
                End With
            Else
                objWebAccount._Message = "WebAccount doesn't exist."
            End If

            objWebAccount._Message = ""
            objWebAccount._Message2 = ""
            For Each objErr As DevNet.Tools.Common.ValidationError In objWebAccounts.ValidationErrors
                If objWebAccount._Message.Length > 0 Then objWebAccount._Message = objWebAccount._Message & "~"
                objWebAccount._Message = objWebAccount._Message & objErr.Message
            Next
        End Using

        Return objWebAccount
    End Function

    <WebMethod()> _
    Public Function Add(ByVal objWebAccount As sWebAccount) As sWebAccount
        objWebAccount._Success = False

        Using objWebAccounts As New cardb.Database.WebAccounts("", , , 0)
            With objWebAccounts.AddWebAccount(objWebAccounts.NewWebAccount)
                .Email = objWebAccount.Email
                .Password = objWebAccount.Password
                ' .EmailInvoices = objWebAccount.EmailInvoices
                .EmailStatements = objWebAccount.EmailStatements
                .DateCreated = objWebAccount.DateCreated
                .ExpiredPassword = objWebAccount.ExpiredPassword
                .AccountStatus = objWebAccount.AccountStatus
                .WebFolder = objWebAccount.WebFolder
                .BL.ActiveUserKey = objWebAccount.ActiveUserKey

                If objWebAccounts.Update Then
                    objWebAccount._Success = True
                    objWebAccount.WebAccountKey = .WebAccountKey
                End If
            End With

            objWebAccount._Message = ""
            objWebAccount._Message2 = ""
            For Each objErr As DevNet.Tools.Common.ValidationError In objWebAccounts.ValidationErrors
                If objWebAccount._Message.Length > 0 Then objWebAccount._Message = objWebAccount._Message & "~"
                objWebAccount._Message = objWebAccount._Message & objErr.Message
            Next
        End Using

        Return objWebAccount
    End Function

    <WebMethod()> _
    Public Function Edit(ByVal objWebAccount As sWebAccount) As sWebAccount
        objWebAccount._Success = False

        Using objWebAccounts As New cardb.Database.WebAccounts("WebAccountKey = " & objWebAccount.WebAccountKey)
            If objWebAccounts.Count > 0 Then
                With objWebAccounts(0)
                    .Email = objWebAccount.Email
                    .Password = objWebAccount.Password
                    '    .EmailInvoices = objWebAccount.EmailInvoices
                    .EmailStatements = objWebAccount.EmailStatements
                    .DateCreated = objWebAccount.DateCreated
                    .ExpiredPassword = objWebAccount.ExpiredPassword
                    .AccountStatus = objWebAccount.AccountStatus
                    .WebFolder = objWebAccount.WebFolder
                    .BL.ActiveUserKey = objWebAccount.ActiveUserKey

                    If objWebAccounts.Update Then
                        objWebAccount._Success = True
                        objWebAccount.WebAccountKey = .WebAccountKey
                    End If
                End With
            Else
                objWebAccount._Message = "WebAccount doesn't exist."
            End If

            objWebAccount._Message = ""
            objWebAccount._Message2 = ""
            For Each objErr As DevNet.Tools.Common.ValidationError In objWebAccounts.ValidationErrors
                If objWebAccount._Message.Length > 0 Then objWebAccount._Message = objWebAccount._Message & "~"
                objWebAccount._Message = objWebAccount._Message & objErr.Message
            Next
        End Using

        Return objWebAccount
    End Function

    <WebMethod()> _
    Public Function Delete(ByVal objWebAccount As sWebAccount) As sWebAccount
        objWebAccount._Success = False

        Using objWebAccounts As New cardb.Database.WebAccounts("WebAccountKey = " & objWebAccount.WebAccountKey)
            If objWebAccounts.Count > 0 Then
                objWebAccounts(0).BL.ActiveUserKey = objWebAccount.ActiveUserKey
                objWebAccounts(0).Delete()

                If objWebAccounts.Update Then
                    objWebAccount._Success = True
                End If
            Else
                objWebAccount._Message = "WebAccount doesn't exist."
            End If

            objWebAccount._Message = ""
            objWebAccount._Message2 = ""
            For Each objErr As DevNet.Tools.Common.ValidationError In objWebAccounts.ValidationErrors
                If objWebAccount._Message.Length > 0 Then objWebAccount._Message = objWebAccount._Message & "~"
                objWebAccount._Message = objWebAccount._Message & objErr.Message
            Next
        End Using

        Return objWebAccount
    End Function
End Class
