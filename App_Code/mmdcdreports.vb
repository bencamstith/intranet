﻿Imports System.Web
Imports System.Web.Services
Imports System.Web.Services.Protocols

' To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line.
' <System.Web.Script.Services.ScriptService()> _
<WebService(Namespace:="http://tempuri.org/")> _
<WebServiceBinding(ConformsTo:=WsiProfiles.BasicProfile1_1)> _
<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Public Class mmdcdreports
    Inherits System.Web.Services.WebService

    Public Structure sMMDCDReport
        Public MMDCDReportKey As Integer
        Public MMDCDGroupKey As Integer
        Public MmdCd As String
        Public MmdCategory As String
        Public CarlineYear As Integer
        Public CarlineNm As String
        Public PartType As String
        Public OutOfStockActivity As Double
        Public TotalActivity As Double
        Public Sales As Double
        Public QOH As Double
        Public Need As Double
        Public AvgPrice As Double
        Public AvgExchangePrice As Double
        Public DateCreated As String
        Public _Success As Boolean
        Public ActiveUserKey As Integer
        Public _Message As String
        Public _Message2 As String
    End Structure

    <WebMethod()> _
    Public Function MMDCDReports(ByVal strWhere As String, ByVal strOrderBy As String, ByVal strJoin As String, ByVal intTop As Integer) As System.Data.DataTable
        Dim objMMDCDReports As cardb.Database.MMDCDReports
        objMMDCDReports = New cardb.Database.MMDCDReports(strWhere, strOrderBy, strJoin, intTop)
        Return objMMDCDReports
    End Function

    <WebMethod()> _
    Public Function Query(ByVal objMMDCDReport As sMMDCDReport) As sMMDCDReport
        objMMDCDReport._Success = False

        Using objMMDCDReports As New cardb.Database.MMDCDReports("MMDCDReportKey = " & objMMDCDReport.MMDCDReportKey)
            If objMMDCDReports.Count > 0 Then
                With objMMDCDReports(0)
                    objMMDCDReport.MMDCDReportKey = IIf(IsDBNull(.MMDCDReportKey), 0, .MMDCDReportKey)
                    objMMDCDReport.MMDCDGroupKey = IIf(IsDBNull(.MMDCDGroupKey), 0, .MMDCDGroupKey)
                    objMMDCDReport.MmdCd = IIf(IsDBNull(.MmdCd), "", .MmdCd)
                    objMMDCDReport.MmdCategory = IIf(IsDBNull(.MmdCategory), "", .MmdCategory)
                    objMMDCDReport.CarlineYear = IIf(IsDBNull(.CarlineYear), 0, .CarlineYear)
                    objMMDCDReport.CarlineNm = IIf(IsDBNull(.CarlineNm), "", .CarlineNm)
                    objMMDCDReport.PartType = IIf(IsDBNull(.PartType), "", .PartType)
                    objMMDCDReport.OutOfStockActivity = IIf(IsDBNull(.OutOfStockActivity), 0, .OutOfStockActivity)
                    objMMDCDReport.TotalActivity = IIf(IsDBNull(.TotalActivity), 0, .TotalActivity)
                    objMMDCDReport.Sales = IIf(IsDBNull(.Sales), 0, .Sales)
                    objMMDCDReport.QOH = IIf(IsDBNull(.QOH), 0, .QOH)
                    objMMDCDReport.Need = IIf(IsDBNull(.Need), 0, .Need)
                    objMMDCDReport.AvgPrice = IIf(IsDBNull(.AvgPrice), 0, .AvgPrice)
                    objMMDCDReport.AvgExchangePrice = IIf(IsDBNull(.AvgExchangePrice), 0, .AvgExchangePrice)
                    objMMDCDReport.DateCreated = IIf(IsDBNull(.DateCreated), "", .DateCreated)

                    objMMDCDReport._Success = True
                End With
            Else
                objMMDCDReport._Message = "MMDCDReport doesn't exist."
            End If

            objMMDCDReport._Message = ""
            objMMDCDReport._Message2 = ""
            For Each objErr As DevNet.Tools.Common.ValidationError In objMMDCDReports.ValidationErrors
                If objMMDCDReport._Message.Length > 0 Then objMMDCDReport._Message = objMMDCDReport._Message & "~"
                objMMDCDReport._Message = objMMDCDReport._Message & objErr.Message
            Next
        End Using

        Return objMMDCDReport
    End Function

    <WebMethod()> _
    Public Function Add(ByVal objMMDCDReport As sMMDCDReport) As sMMDCDReport
        objMMDCDReport._Success = False

        Using objMMDCDReports As New cardb.Database.MMDCDReports("", , , 0)
            With objMMDCDReports.AddMMDCDReport(objMMDCDReports.NewMMDCDReport)
                .MMDCDGroupKey = objMMDCDReport.MMDCDGroupKey
                .MmdCd = objMMDCDReport.MmdCd
                .MmdCategory = objMMDCDReport.MmdCategory
                .CarlineYear = objMMDCDReport.CarlineYear
                .CarlineNm = objMMDCDReport.CarlineNm
                .PartType = objMMDCDReport.PartType
                .OutOfStockActivity = objMMDCDReport.OutOfStockActivity
                .TotalActivity = objMMDCDReport.TotalActivity
                .Sales = objMMDCDReport.Sales
                .QOH = objMMDCDReport.QOH
                .Need = objMMDCDReport.Need
                .AvgPrice = objMMDCDReport.AvgPrice
                .AvgExchangePrice = objMMDCDReport.AvgExchangePrice
                .DateCreated = objMMDCDReport.DateCreated
                .BL.ActiveUserKey = objMMDCDReport.ActiveUserKey

                If objMMDCDReports.Update Then
                    objMMDCDReport._Success = True
                    objMMDCDReport.MMDCDReportKey = .MMDCDReportKey
                End If
            End With

            objMMDCDReport._Message = ""
            objMMDCDReport._Message2 = ""
            For Each objErr As DevNet.Tools.Common.ValidationError In objMMDCDReports.ValidationErrors
                If objMMDCDReport._Message.Length > 0 Then objMMDCDReport._Message = objMMDCDReport._Message & "~"
                objMMDCDReport._Message = objMMDCDReport._Message & objErr.Message
            Next
        End Using

        Return objMMDCDReport
    End Function

    <WebMethod()> _
    Public Function Edit(ByVal objMMDCDReport As sMMDCDReport) As sMMDCDReport
        objMMDCDReport._Success = False

        Using objMMDCDReports As New cardb.Database.MMDCDReports("MMDCDReportKey = " & objMMDCDReport.MMDCDReportKey)
            If objMMDCDReports.Count > 0 Then
                With objMMDCDReports(0)
                    .MMDCDGroupKey = objMMDCDReport.MMDCDGroupKey
                    .MmdCd = objMMDCDReport.MmdCd
                    .MmdCategory = objMMDCDReport.MmdCategory
                    .CarlineYear = objMMDCDReport.CarlineYear
                    .CarlineNm = objMMDCDReport.CarlineNm
                    .PartType = objMMDCDReport.PartType
                    .OutOfStockActivity = objMMDCDReport.OutOfStockActivity
                    .TotalActivity = objMMDCDReport.TotalActivity
                    .Sales = objMMDCDReport.Sales
                    .QOH = objMMDCDReport.QOH
                    .Need = objMMDCDReport.Need
                    .AvgPrice = objMMDCDReport.AvgPrice
                    .AvgExchangePrice = objMMDCDReport.AvgExchangePrice
                    .DateCreated = objMMDCDReport.DateCreated
                    .BL.ActiveUserKey = objMMDCDReport.ActiveUserKey

                    If objMMDCDReports.Update Then
                        objMMDCDReport._Success = True
                        objMMDCDReport.MMDCDReportKey = .MMDCDReportKey
                    End If
                End With
            Else
                objMMDCDReport._Message = "MMDCDReport doesn't exist."
            End If

            objMMDCDReport._Message = ""
            objMMDCDReport._Message2 = ""
            For Each objErr As DevNet.Tools.Common.ValidationError In objMMDCDReports.ValidationErrors
                If objMMDCDReport._Message.Length > 0 Then objMMDCDReport._Message = objMMDCDReport._Message & "~"
                objMMDCDReport._Message = objMMDCDReport._Message & objErr.Message
            Next
        End Using

        Return objMMDCDReport
    End Function

    <WebMethod()> _
    Public Function Delete(ByVal objMMDCDReport As sMMDCDReport) As sMMDCDReport
        objMMDCDReport._Success = False

        Using objMMDCDReports As New cardb.Database.MMDCDReports("MMDCDReportKey = " & objMMDCDReport.MMDCDReportKey)
            If objMMDCDReports.Count > 0 Then
                objMMDCDReports(0).BL.ActiveUserKey = objMMDCDReport.ActiveUserKey
                objMMDCDReports(0).Delete()

                If objMMDCDReports.Update Then
                    objMMDCDReport._Success = True
                End If
            Else
                objMMDCDReport._Message = "MMDCDReport doesn't exist."
            End If

            objMMDCDReport._Message = ""
            objMMDCDReport._Message2 = ""
            For Each objErr As DevNet.Tools.Common.ValidationError In objMMDCDReports.ValidationErrors
                If objMMDCDReport._Message.Length > 0 Then objMMDCDReport._Message = objMMDCDReport._Message & "~"
                objMMDCDReport._Message = objMMDCDReport._Message & objErr.Message
            Next
        End Using

        Return objMMDCDReport
    End Function
End Class
