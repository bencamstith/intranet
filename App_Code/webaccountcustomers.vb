﻿Imports System.Web
Imports System.Web.Services
Imports System.Web.Services.Protocols

' To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line.
' <System.Web.Script.Services.ScriptService()> _
<WebService(Namespace:="http://tempuri.org/")> _
<WebServiceBinding(ConformsTo:=WsiProfiles.BasicProfile1_1)> _
<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Public Class webaccountcustomers
    Inherits System.Web.Services.WebService

    Public Structure sWebAccountCustomer
        Public WebAccountCustomerKey As Integer
        Public CustomerNumber As String
        Public DateCreated As String
        Public WebAccountKey As Integer
        Public EmailQuotes As Boolean
        Public QuoteEmailAddress As String
        Public _Success As Boolean
        Public ActiveUserKey As Integer
        Public _Message As String
        Public _Message2 As String
    End Structure

    <WebMethod()> _
    Public Function WebAccountCustomers(ByVal strWhere As String, ByVal strOrderBy As String, ByVal strJoin As String, ByVal intTop As Integer) As System.Data.DataTable
        Dim objWebAccountCustomers As cardb.Database.WebAccountCustomers
        objWebAccountCustomers = New cardb.Database.WebAccountCustomers(strWhere, strOrderBy, strJoin, intTop)
        Return objWebAccountCustomers
    End Function

    <WebMethod()> _
    Public Function Query(ByVal objWebAccountCustomer As sWebAccountCustomer) As sWebAccountCustomer
        objWebAccountCustomer._Success = False

        Using objWebAccountCustomers As New cardb.Database.WebAccountCustomers("WebAccountCustomerKey = " & objWebAccountCustomer.WebAccountCustomerKey)
            If objWebAccountCustomers.Count > 0 Then
                With objWebAccountCustomers(0)
                    objWebAccountCustomer.WebAccountCustomerKey = IIf(IsDBNull(.WebAccountCustomerKey), 0, .WebAccountCustomerKey)
                    objWebAccountCustomer.CustomerNumber = IIf(IsDBNull(.CustomerNumber), "", .CustomerNumber)
                    objWebAccountCustomer.DateCreated = IIf(IsDBNull(.DateCreated), "", .DateCreated)
                    objWebAccountCustomer.WebAccountKey = IIf(IsDBNull(.WebAccountKey), 0, .WebAccountKey)
                    objWebAccountCustomer.EmailQuotes = IIf(IsDBNull(.EmailQuotes), False, .EmailQuotes)
                    objWebAccountCustomer.QuoteEmailAddress = IIf(IsDBNull(.QuoteEmailAddress), "", .QuoteEmailAddress)

                    objWebAccountCustomer._Success = True
                End With
            Else
                objWebAccountCustomer._Message = "WebAccountCustomer doesn't exist."
            End If

            objWebAccountCustomer._Message = ""
            objWebAccountCustomer._Message2 = ""
            For Each objErr As DevNet.Tools.Common.ValidationError In objWebAccountCustomers.ValidationErrors
                If objWebAccountCustomer._Message.Length > 0 Then objWebAccountCustomer._Message = objWebAccountCustomer._Message & "~"
                objWebAccountCustomer._Message = objWebAccountCustomer._Message & objErr.Message
            Next
        End Using

        Return objWebAccountCustomer
    End Function

    <WebMethod()> _
    Public Function Add(ByVal objWebAccountCustomer As sWebAccountCustomer) As sWebAccountCustomer
        objWebAccountCustomer._Success = False

        Using objWebAccountCustomers As New cardb.Database.WebAccountCustomers("", , , 0)
            With objWebAccountCustomers.AddWebAccountCustomer(objWebAccountCustomers.NewWebAccountCustomer)
                .CustomerNumber = objWebAccountCustomer.CustomerNumber
                .DateCreated = objWebAccountCustomer.DateCreated
                .WebAccountKey = objWebAccountCustomer.WebAccountKey
                .EmailQuotes = objWebAccountCustomer.EmailQuotes
                .QuoteEmailAddress = objWebAccountCustomer.QuoteEmailAddress
                .BL.ActiveUserKey = objWebAccountCustomer.ActiveUserKey

                If objWebAccountCustomers.Update Then
                    objWebAccountCustomer._Success = True
                    objWebAccountCustomer.WebAccountCustomerKey = .WebAccountCustomerKey
                End If
            End With

            objWebAccountCustomer._Message = ""
            objWebAccountCustomer._Message2 = ""
            For Each objErr As DevNet.Tools.Common.ValidationError In objWebAccountCustomers.ValidationErrors
                If objWebAccountCustomer._Message.Length > 0 Then objWebAccountCustomer._Message = objWebAccountCustomer._Message & "~"
                objWebAccountCustomer._Message = objWebAccountCustomer._Message & objErr.Message
            Next
        End Using

        Return objWebAccountCustomer
    End Function

    <WebMethod()> _
    Public Function Edit(ByVal objWebAccountCustomer As sWebAccountCustomer) As sWebAccountCustomer
        objWebAccountCustomer._Success = False

        Using objWebAccountCustomers As New cardb.Database.WebAccountCustomers("WebAccountCustomerKey = " & objWebAccountCustomer.WebAccountCustomerKey)
            If objWebAccountCustomers.Count > 0 Then
                With objWebAccountCustomers(0)
                    .CustomerNumber = objWebAccountCustomer.CustomerNumber
                    .DateCreated = objWebAccountCustomer.DateCreated
                    .WebAccountKey = objWebAccountCustomer.WebAccountKey
                    .EmailQuotes = objWebAccountCustomer.EmailQuotes
                    .QuoteEmailAddress = objWebAccountCustomer.QuoteEmailAddress
                    .BL.ActiveUserKey = objWebAccountCustomer.ActiveUserKey

                    If objWebAccountCustomers.Update Then
                        objWebAccountCustomer._Success = True
                        objWebAccountCustomer.WebAccountCustomerKey = .WebAccountCustomerKey
                    End If
                End With
            Else
                objWebAccountCustomer._Message = "WebAccountCustomer doesn't exist."
            End If

            objWebAccountCustomer._Message = ""
            objWebAccountCustomer._Message2 = ""
            For Each objErr As DevNet.Tools.Common.ValidationError In objWebAccountCustomers.ValidationErrors
                If objWebAccountCustomer._Message.Length > 0 Then objWebAccountCustomer._Message = objWebAccountCustomer._Message & "~"
                objWebAccountCustomer._Message = objWebAccountCustomer._Message & objErr.Message
            Next
        End Using

        Return objWebAccountCustomer
    End Function

    <WebMethod()> _
    Public Function Delete(ByVal objWebAccountCustomer As sWebAccountCustomer) As sWebAccountCustomer
        objWebAccountCustomer._Success = False

        Using objWebAccountCustomers As New cardb.Database.WebAccountCustomers("WebAccountCustomerKey = " & objWebAccountCustomer.WebAccountCustomerKey)
            If objWebAccountCustomers.Count > 0 Then
                objWebAccountCustomers(0).BL.ActiveUserKey = objWebAccountCustomer.ActiveUserKey
                objWebAccountCustomers(0).Delete()

                If objWebAccountCustomers.Update Then
                    objWebAccountCustomer._Success = True
                End If
            Else
                objWebAccountCustomer._Message = "WebAccountCustomer doesn't exist."
            End If

            objWebAccountCustomer._Message = ""
            objWebAccountCustomer._Message2 = ""
            For Each objErr As DevNet.Tools.Common.ValidationError In objWebAccountCustomers.ValidationErrors
                If objWebAccountCustomer._Message.Length > 0 Then objWebAccountCustomer._Message = objWebAccountCustomer._Message & "~"
                objWebAccountCustomer._Message = objWebAccountCustomer._Message & objErr.Message
            Next
        End Using

        Return objWebAccountCustomer
    End Function
End Class
