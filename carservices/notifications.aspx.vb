﻿
Partial Class notifications
    Inherits System.Web.UI.Page
    Private intSearchPage As Integer = 1
    Private intEmployeeKey As Integer = 0

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Me.IsPostBack Then
            If Not Request.QueryString("page") Is Nothing Then
                intSearchPage = CType(Request.QueryString("page"), Integer)
            End If

            BindTriggerTypes()
            BindEmployees()
        End If
        BindAlerts()
    End Sub

    Private Sub BindTriggerTypes()
        Using objTriggerTypes As New cardb.Database.TriggerTypes
            For Each objTriggerType As cardb.Database.TriggerType In objTriggerTypes
                Me.ddlTriggerTypeKey.Items.Add(New ListItem(objTriggerType.TriggerType, objTriggerType.TriggerTypeKey))
            Next
        End Using
    End Sub

    Private Sub BindEmployees()
        Using objEmployees As New cardb.Database.Employees
            For Each objEmployee As cardb.Database.Employee In objEmployees
                Me.ddlEmployeeKey.Items.Add(New ListItem(objEmployee.EmployeeName, objEmployee.EmployeeKey))
            Next
        End Using
    End Sub

    Private Sub BindAlerts()
        Dim intTotalCount As Integer = 0
        Dim intPages As Integer = 0
        Dim tr As TableRow
        Dim td As TableCell
        Dim strWhere As String = ""

        If intEmployeeKey > 0 Then
            strWhere = "(TransactionAlertKey IN (SELECT TransactionAlertKey FROM tblTransactionAlerts WHERE (EmployeeId = (SELECT EmployeeID FROM tblEmployees WHERE (EmployeeKey = " & intEmployeeKey & ")))))"
        End If

        Me.lblErrors.Text = strWhere
        Using objTransactionAlertTriggers As New cardb.Database.TransactionAlertTriggers(strWhere, "DateCreated DESC", "", 25)
            For Each objErr As DevNet.Tools.Common.ValidationError In objTransactionAlertTriggers.ValidationErrors
                Me.lblErrors.Text = Me.lblErrors.Text & objErr.Message & "<br />"
            Next
            If objTransactionAlertTriggers.Count = 0 Then
                tr = New TableRow
                tr.ID = "noresults"
                tNotifications.Rows.Add(tr)

                td = New TableCell
                tr.Cells.Add(td)
                td.ColumnSpan = 4
                td.Text = "No Alerts found."
            End If
            For Each objAlert As cardb.Database.TransactionAlertTrigger In objTransactionAlertTriggers
                tr = New TableRow
                td = New TableCell

                ' tr.ID = "alert_" & objAlert.TransactionAlertTriggerKey
                tr.Attributes("onmouseout") = "this.className='trdataOut'"
                tr.Attributes("onmouseover") = "this.className='trdataOver'"
                'tr.Attributes("onclick") = "parent.iNav.location.href='crumbs.aspx?urlToolTip=" & Server.UrlEncode("Return to search results for """ & Me.txtSearch.Text & """") & "&urlText=" & Server.UrlEncode("""" & Me.txtSearch.Text & """") & "&urlBack=" & strReturnURL & "';window.navigate('employee.aspx?search=" & strReturnURL & "&employeekey=" & .EmployeeKey & "')"
                tr.Attributes("onclick") = "parent.iNav.location.href='alert.aspx?TransactionAlertTriggerKey=" & objAlert.TransactionAlertTriggerKey & "'"
                tNotifications.Rows.Add(tr)

                tr.Cells.Add(td)
                td.ColumnSpan = 0
                td.Text = objAlert.TransactionTriggerKey_Object.TriggerTypeKey_Object.TriggerType

                td = New TableCell
                tr.Cells.Add(td)
                td.Text = ""
                td.Text = Employee(objAlert.TransactionAlertKey_Object.EmployeeId).EmployeeName

                td = New TableCell
                tr.Cells.Add(td)
                td.Text = AlertDescription(objAlert)

                td = New TableCell
                tr.Cells.Add(td)
                td.Text = objAlert.DateCreated
            Next
        End Using
    End Sub

    Private Function AlertDescription(ByRef objAlert As cardb.Database.TransactionAlertTrigger) As String
        Dim strDescription As String = ""

        With objAlert.TransactionAlertKey_Object
            strDescription = "{INITIALS} {TYPE} {INVENTORYNUMBER} #{STOCKTICKETNUMBER} for {UNITPRICE} ({DISCOUNT}% Discount) to {CUSTOMER}"
            strDescription = strDescription.Replace("{INITIALS}", Employee(objAlert.TransactionAlertKey_Object.EmployeeId).EmployeeInitials)
            strDescription = strDescription.Replace("{UNITPRICE}", FormatCurrency(.UnitPrice, 2))
            strDescription = strDescription.Replace("{CUSTOMER}", .Customer)
            Select Case .Type
                Case Is = "I"
                    strDescription = strDescription.Replace("{TYPE}", "Invoiced")
                Case Is = "Q"
                    strDescription = strDescription.Replace("{TYPE}", "Quoted")
                Case Is = "D"
                    strDescription = strDescription.Replace("{TYPE}", "created Delivery Ticket for")
                Case Is = "W"
                    strDescription = strDescription.Replace("{TYPE}", "created Work Order for")
                Case Is = "P"
                    strDescription = strDescription.Replace("{TYPE}", "created P.O. for")
            End Select

            Using objAlertInventories As New cardb.Database.TransactionAlertInventories("TransactionAlertKey = " & .TransactionAlertKey)
                For Each objAlertInventory As cardb.Database.TransactionAlertInventory In objAlertInventories
                    If objAlertInventory.InventoryId = .InventoryId Then
                        strDescription = strDescription.Replace("{DISCOUNT}", Math.Round((objAlertInventory.WholesalePrice - .UnitPrice) / objAlertInventory.WholesalePrice * 100, 2).ToString)
                        strDescription = strDescription.Replace("{INVENTORYNUMBER}", objAlertInventory.InventoryNumber)
                        strDescription = strDescription.Replace("{STOCKTICKETNUMBER}", objAlertInventory.StockTicketNumber)
                        Exit For
                    End If
                Next
            End Using
        End With

        Return strDescription
    End Function

    Private Function Employee(ByVal intEmployeeId As Integer) As cardb.Database.Employee
        Dim objEmployee As cardb.Database.Employee = Nothing
        Using objEmployess As New cardb.Database.Employees("EmployeeId = " & intEmployeeId)
            If objEmployess.Count > 0 Then
                objEmployee = objEmployess(0)
            End If
        End Using
        Return objEmployee
    End Function

    Protected Sub ddlEmployeeKey_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlEmployeeKey.SelectedIndexChanged
        intEmployeeKey = ddlEmployeeKey.SelectedValue
        BindAlerts()
    End Sub
End Class
